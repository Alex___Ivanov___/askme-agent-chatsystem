module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "jest": true
    },
    "extends": "airbnb",
    "globals": {
        "it": "readonly",
        "describe": "readonly",
        "test": "readonly",
        "expect": "readonly",
        "beforeEach": "readonly",
        "jest": "readonly",
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "jest"
    ],
    "parser": "babel-eslint",
    "rules": {
        "max-len": ["error", {"code":120}],
        "no-console": "off",
        "no-plusplus": "off",
        "react/jsx-filename-extension": ["warn", {"extensions": [".js", ".jsx"]}],
        "import/no-extraneous-dependencies": ["error", {"devDependencies": true}],
        "react/button-has-type": "off",
        "linebreak-style": "off",
        "react/prop-types": "warn",
        "jsx-a11y/click-events-have-key-events": "off",
        "jsx-a11y/no-static-element-interactions": "off",
        "camelcase":"warn",
        "indent": ["warn", 2],
        "react/jsx-indent": "warn"
    },
};
