const merge = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { DefinePlugin } = require('webpack');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    // new BundleAnalyzerPlugin(),
    new CleanWebpackPlugin(),
    new DefinePlugin({
      DEBUG_LOG: JSON.stringify(true),
      BACKEND_WEBSOCKET: JSON.stringify('wss://staging.stchat.cf/api'),
      ORIGIN_DOMAIN_WHITELIST: JSON.stringify([
        'https://staging.stchat.cf',
        'http://localhost:3000',
        'https://investarena.waiviodev.com',
      ]),
      IFRAME_DOMAIN: JSON.stringify('https://staging.stchat.cf'),
    }),
  ],
});
