const merge = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { DefinePlugin } = require('webpack');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    // new BundleAnalyzerPlugin(),
    new CleanWebpackPlugin(),
    new DefinePlugin({
      DEBUG_LOG: JSON.stringify(false),
      BACKEND_WEBSOCKET: JSON.stringify('ws://95.217.189.252:8090'),
      ORIGIN_DOMAIN_WHITELIST: JSON.stringify(['http://95.217.189.252']),
      IFRAME_DOMAIN: JSON.stringify('http://95.217.189.252'),
    }),
  ],
});
