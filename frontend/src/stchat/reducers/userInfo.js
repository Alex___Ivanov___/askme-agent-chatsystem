import actions from '../actions';

const userInfoInitialState = {
  username: '',
  isGuest: false,
};

export default (state = userInfoInitialState, action) => {
  switch (action.type) {
  case actions.SET_USERINFO: {
    return { ...state, ...action.payload };
  }
  default:
    return state;
  }
};
