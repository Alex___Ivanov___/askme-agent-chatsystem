import chatMessagesUpdater from '../chatMessagesUpdater';
import actions from '../../actions';

describe('ChatMessagesUpdater CHAT_UPDATED action handling', () => {
  const cases = [
    {
      desc: 'should return state with new roomId and clientId fields',
      initialState: {},
      action: {
        type: actions.CHAT_UPDATED,
        payload: {
          roomId: 'room1',
          clientId: 'user1',
        },
      },
      wantState: {
        roomId: 'room1',
        clientId: 'user1',
      },
    },
    {
      desc: 'should return state with updated roomId and clientId fields',
      initialState: {
        roomId: 'room1',
        clientId: 'user1',
      },
      action: {
        type: actions.CHAT_UPDATED,
        payload: {
          roomId: 'room2',
          clientId: 'user2',
        },
      },
      wantState: {
        roomId: 'room2',
        clientId: 'user2',
      },
    },
  ];

  cases.forEach((c) => {
    it(c.desc, () => {
      expect(chatMessagesUpdater(c.initialState, c.action)).toEqual(c.wantState);
    });
  });
});
