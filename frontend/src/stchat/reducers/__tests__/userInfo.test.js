import actions from '../../actions';
import userInfoReducer from '../userInfo';

describe('USERINFO reducer SET_USERNAME action handling', () => {
  const initialState = {};
  const action = {
    type: actions.SET_USERINFO,
    payload: {
      username: 'userName',
    },
  };
  const wantState = {
    username: 'userName',
  };
  it('should return state with new userName', () => {
    expect(userInfoReducer(initialState, action)).toEqual(wantState);
  });
});
