import actions from '../../actions';
import modals from '../modals';

describe('modals reducer actions', () => {
  // arrange
  const cases = [
    {
      desc: 'should bind msg and resultCb to modals confirm state and result returns true',
      action: {
        type: actions.OPEN_CONFIRM_MODAL,
        payload: {
          msg: 'test message',
          resultCb: (result) => result,
        },
      },
      wasState: {
        confirm: {
          msg: '',
          enabled: false,
          resultCb: () => false,
        },
      },
      wantState: {
        confirm: {
          msg: 'test message',
          enabled: true,
          resultCb: (result) => result,
        },
      },
      result: true,
    },
    {
      desc: 'should reset modals confirm state to initial',
      action: { type: actions.CLOSE_CONFIRM_MODAL },
      wasState: {
        confirm: {
          msg: 'test message',
          enabled: true,
          resultCb: (result) => result,
        },
      },
      wantState: {
        confirm: {
          msg: '',
          enabled: false,
          resultCb: () => false,
        },
      },
      result: false,
    },
  ];
  cases.forEach((c) => {
    it(c.desc, () => {
      // act
      const gotState = modals(c.wasState, c.action);
      // assert
      expect(JSON.stringify(gotState)).toEqual(JSON.stringify(c.wantState));
      expect(gotState.confirm.resultCb(true)).toEqual(c.result);
    });
  });
});
