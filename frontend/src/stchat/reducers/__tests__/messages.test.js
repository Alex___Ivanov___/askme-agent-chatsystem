import actions from '../../actions';
import messagesReducer, { emptyRoom } from '../messages';

describe('MESSAGE reducer GET_MESSAGES_STORE action handling', () => {
  const roomMessages = [{
    sender: 'sender2',
    text: 'text2',
    time: '2002-07-01T16:51:05Z',
    id: '2',
    isModerated: true,
  }];
  const messages = [{
    sender: 'sender1',
    text: 'text1',
    time: '2002-07-01T16:50:05Z',
    id: '1',
    isModerated: true,
  }];
  const roomId = 'roomId1';

  const cases = [
    {
      desc: `should return state with new room when state doesn't
  contain input roomId`,
      initialState: {},
      action: {
        type: actions.GET_MESSAGES_STORE,
        payload: {
          roomId,
          messages,
        },
      },
      wantState: {
        roomId1: {
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          messages,
          historyEndReached: false,
          initialHistoryFetched: false,
          unreadEndReached: false,
        },
      },
    },
    {
      desc: `should return state with new room when state doesn't
  contain messages field in room`,
      initialState: {
        roomId1: {
          historyEndReached: false,
        },
      },
      action: {
        type: actions.GET_MESSAGES_STORE,
        payload: {
          roomId,
          messages,
        },
      },
      wantState: {
        roomId1: {
          messages,
          historyEndReached: false,
        },
      },
    },
    {
      desc: `should return state with new room when state doesn't
  contain messages field in room`,
      initialState: {
        roomId1: {
          messages: roomMessages,
          historyEndReached: false,
        },
      },
      action: {
        type: actions.GET_MESSAGES_STORE,
        payload: {
          roomId,
          messages,
        },
      },
      wantState: {
        roomId1: {
          messages: [...roomMessages, ...messages],
          historyEndReached: false,
        },
      },
    },
  ];

  cases.forEach((c) => {
    it(c.desc, () => {
      expect(messagesReducer(c.initialState, c.action)).toEqual(c.wantState);
    });
  });
});

describe('MESSAGE reducer SEND_MESSAGE_STORE action handling', () => {
  const payload = {
    sender: 'sender1',
    text: 'text1',
    time: '2002-07-01T16:53:05Z',
    id: '1',
    roomId: 'roomId1',
    isModerated: true,
  };
  const {
    sender, text, time, id, isModerated,
  } = payload;
  const roomMessages = [{
    sender: 'sender2',
    text: 'text2',
    time: '2002-07-01T16:51:05Z',
    id: '2',
    isModerated: true,
  }];

  const cases = [
    {
      desc: 'should return state with new room when state doesn\'t contain input roomId',
      initialState: {},
      action: {
        type: actions.SEND_MESSAGE_STORE,
        payload,
      },
      wantState: {
        roomId1: {
          chatScrollPosition: {
            scrollTop: 0,
            top: 1,
          },
          messages: [{
            sender, text, time, id, isModerated,
          }],
          historyEndReached: false,
          initialHistoryFetched: false,
          unreadEndReached: false,
        },
      },
    },
    {
      desc: 'should return state with new room when state doesn\'t contain messages field in room',
      initialState: {
        roomId1: {
          historyEndReached: false,
        },
      },
      action: {
        type: actions.SEND_MESSAGE_STORE,
        payload,
      },
      wantState: {
        roomId1: {
          chatScrollPosition: {
            top: 1,
          },
          messages: [{
            sender, text, time, id, isModerated,
          }],
          historyEndReached: false,
        },
      },
    },
    {
      desc: 'should return state with old room and new message in it',
      initialState: {
        roomId1: {
          messages: roomMessages,
          historyEndReached: false,
        },
      },
      action: {
        type: actions.SEND_MESSAGE_STORE,
        payload,
      },
      wantState: {
        roomId1: {
          chatScrollPosition: {
            top: 1,
          },
          messages: [{
            sender, text, time, id, isModerated,
          }, ...roomMessages],
          historyEndReached: false,
        },
      },
    },
  ];

  cases.forEach((c) => {
    it(c.desc, () => {
      expect(messagesReducer(c.initialState, c.action)).toEqual(c.wantState);
    });
  });
});

describe('MESSAGE reducer MESSAGE_RECEIVED_STORE action handling', () => {
  const payload = {
    sender: 'sender1',
    text: 'text1',
    time: '2002-07-01T16:53:05Z',
    id: '1',
    roomId: 'roomId1',
    isModerated: true,
  };
  const {
    sender, text, time, id, isModerated,
  } = payload;
  const roomMessages = [{
    sender: 'sender2',
    text: 'text2',
    time: '2002-07-01T16:51:05Z',
    id: '2',
    isModerated: true,
  }];

  const cases = [
    {
      desc: 'should return state with new room when state doesn\'t contain input roomId',
      initialState: {},
      action: {
        type: actions.MESSAGE_RECEIVED_STORE,
        payload,
      },
      wantState: {
        roomId1: {
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          messages: [{
            sender, text, time, id, isModerated,
          }],
          historyEndReached: false,
          initialHistoryFetched: false,
          unreadEndReached: false,
        },
      },
    },
    {
      desc: 'should return state with new room when state doesn\'t contain input roomId',
      initialState: {
        roomId1: {
          historyEndReached: false,
        },
      },
      action: {
        type: actions.MESSAGE_RECEIVED_STORE,
        payload,
      },
      wantState: {
        roomId1: {
          messages: [{
            sender, text, time, id, isModerated,
          }],
          historyEndReached: false,
        },
      },
    },
    {
      desc: 'should return state with old room and new message in it',
      initialState: {
        roomId1: {
          messages: roomMessages,
          historyEndReached: false,
        },
      },
      action: {
        type: actions.MESSAGE_RECEIVED_STORE,
        payload,
      },
      wantState: {
        roomId1: {
          messages: [{
            sender, text, time, id, isModerated,
          }, ...roomMessages],
          historyEndReached: false,
        },
      },
    },
  ];

  cases.forEach((c) => {
    it(c.desc, () => {
      expect(messagesReducer(c.initialState, c.action)).toEqual(c.wantState);
    });
  });
});

describe('MESSAGE reducer HISTORY_END_REACHED action handling', () => {
  const cases = [
    {
      desc: 'should return state with new room when state doesn\'t contain input roomId',
      initialState: {},
      action: {
        type: actions.HISTORY_END_REACHED_STORE,
        payload: {
          roomId: 'roomId2',
        },
      },
      wantState: {
        roomId2: {
          messages: [],
          historyEndReached: true,
          initialHistoryFetched: false,
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          unreadEndReached: false,
        },
      },
    },
    {
      desc: 'should return state with the room and new historyEndReached flag in it',
      initialState: {
        roomId2: {
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          messages: [],
          historyEndReached: false,
          unreadEndReached: false,
        },
      },
      action: {
        type: actions.HISTORY_END_REACHED_STORE,
        payload: {
          roomId: 'roomId2',
        },
      },
      wantState: {
        roomId2: {
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          messages: [],
          historyEndReached: true,
          unreadEndReached: false,
        },
      },
    },
  ];

  cases.forEach((c) => {
    it(c.desc, () => {
      expect(messagesReducer(c.initialState, c.action)).toEqual(c.wantState);
    });
  });
});

describe('MESSAGE reducer SET_INITIAL_HISTORY_FETCHED action handling', () => {
  const cases = [
    {
      desc: 'should return state with new room when state doesn\'t contain input roomId',
      initialState: {},
      action: {
        type: actions.SET_INITIAL_HISTORY_FETCHED,
        payload: {
          roomId: 'roomId2',
          initialHistoryFetched: true,
        },
      },
      wantState: {
        roomId2: {
          messages: [],
          historyEndReached: false,
          initialHistoryFetched: true,
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          unreadEndReached: false,
        },
      },
    },
    {
      desc: 'should return state with new room when state doesn\'t contain input roomId',
      initialState: {
        roomId2: {
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          messages: [],
          historyEndReached: false,
          initialHistoryFetched: false,
          unreadEndReached: false,
        },
      },
      action: {
        type: actions.SET_INITIAL_HISTORY_FETCHED,
        payload: {
          roomId: 'roomId2',
          initialHistoryFetched: true,
        },
      },
      wantState: {
        roomId2: {
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          messages: [],
          historyEndReached: false,
          initialHistoryFetched: true,
          unreadEndReached: false,
        },
      },
    },
  ];

  cases.forEach((c) => {
    it(c.desc, () => {
      expect(messagesReducer(c.initialState, c.action)).toEqual(c.wantState);
    });
  });
});

describe('MESSAGE reducer UPDATE_ROOM_SCROLL action handling', () => {
  const cases = [
    {
      desc: 'should return state with new room when state doesn\'t contain input roomId',
      initialState: {},
      action: {
        type: actions.UPDATE_ROOM_SCROLL,
        payload: {
          roomId: 'roomId2',
          chatScrollPosition: {
            scrollTop: 1,
            top: 1,
          },
        },
      },
      wantState: {
        roomId2: {
          messages: [],
          historyEndReached: false,
          initialHistoryFetched: false,
          chatScrollPosition: {
            scrollTop: 1,
            top: 1,
          },
          unreadEndReached: false,
        },
      },
    },
    {
      desc: 'should return state with new room when state doesn\'t contain input roomId',
      initialState: {
        roomId2: {
          chatScrollPosition: {
            scrollTop: 0,
            top: 0,
          },
          messages: [],
          historyEndReached: false,
          initialHistoryFetched: false,
          unreadEndReached: false,
        },
      },
      action: {
        type: actions.UPDATE_ROOM_SCROLL,
        payload: {
          roomId: 'roomId2',
          chatScrollPosition: {
            scrollTop: 1,
            top: 1,
          },
        },
      },
      wantState: {
        roomId2: {
          chatScrollPosition: {
            scrollTop: 1,
            top: 1,
          },
          messages: [],
          historyEndReached: false,
          initialHistoryFetched: false,
          unreadEndReached: false,
        },
      },
    },
  ];

  cases.forEach((c) => {
    it(c.desc, () => {
      expect(messagesReducer(c.initialState, c.action)).toEqual(c.wantState);
    });
  });
});

describe('MESSAGES reducer CLEAR_HISTORY_STORE action handling', () => {
  const initialState = {
    room1: {
      messages: [
        {
          sender: 'monterey',
          text: 'some text room1 4',
          time: '2002-07-01T16:50:05Z',
          id: '1',
          isModerated: true,
        },
      ],
      historyEndReached: false,
      chatScrollPosition: {
        scrollTop: 1,
        top: 1,
      },
      initialHistoryFetched: true,
    },
  };
  const action = {
    type: actions.CLEAR_HISTORY_STORE,
    payload: {
      roomId: 'room1',
    },
  };
  const wantState = {
    room1: {
      ...emptyRoom,
      historyEndReached: true,
      initialHistoryFetched: true,
    },
  };

  it('should return state with new activeRoomID', () => {
    expect(messagesReducer(initialState, action)).toEqual(wantState);
  });
});

describe('MESSAGES reducer DELETE_ROOM_STORE action handling', () => {
  const initialState = {
    room1: {
      messages: [
        {
          sender: 'monterey',
          text: 'some text room1 4',
          time: '2002-07-01T16:50:05Z',
          id: '1',
          isModerated: true,
        },
      ],
      historyEndReached: false,
      chatScrollPosition: {
        scrollTop: 1,
        top: 1,
      },
      initialHistoryFetched: true,
    },
  };
  const action = {
    type: actions.DELETE_ROOM_STORE,
    payload: {
      roomId: 'room1',
    },
  };
  const wantState = {};

  it('should return state with no rooms', () => {
    expect(messagesReducer(initialState, action)).toEqual(wantState);
  });
});

describe('MESSAGES reducer SET_MESSAGES_STORE action handling', () => {
  const initialState = {
    room1: {
      messages: [
        {
          sender: 'monterey',
          text: 'some text room1 4',
          time: '2002-07-01T16:50:05Z',
          id: '1',
          isModerated: true,
        },
      ],
      historyEndReached: false,
      chatScrollPosition: {
        scrollTop: 1,
        top: 1,
      },
      initialHistoryFetched: true,
    },
  };

  const action = {
    type: actions.SET_MESSAGES_STORE,
    payload: {
      room2: {
        messages: [
          {
            sender: 'monterey',
            text: 'some text room1 4',
            time: '2002-07-01T16:50:05Z',
            id: '1',
            isModerated: true,
          },
        ],
        historyEndReached: false,
        chatScrollPosition: {
          scrollTop: 1,
          top: 1,
        },
        initialHistoryFetched: true,
      },
    },
  };
  const wantState = {
    room2: {
      messages: [
        {
          sender: 'monterey',
          text: 'some text room1 4',
          time: '2002-07-01T16:50:05Z',
          id: '1',
          isModerated: true,
        },
      ],
      historyEndReached: false,
      chatScrollPosition: {
        scrollTop: 1,
        top: 1,
      },
      initialHistoryFetched: true,
    },
  };

  it('should return new state from action.payload', () => {
    expect(messagesReducer(initialState, action)).toEqual(wantState);
  });
});
