import roomsReducer from '../rooms';
import actions from '../../actions';

describe('ROOMS reducer SET_ACTIVE_ROOM action handling', () => {
  const initialState = {};
  const action = {
    type: actions.SET_ACTIVE_ROOM,
    payload: 'room1',
  };
  const wantState = {
    activeRoomID: 'room1',
  };

  it('should return state with new activeRoomID', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer GET_USER_ROOMS_STORE action handling', () => {
  const cases = [
    {
      desc: 'should return state with new rooms',
      initialState: {},
      action: {
        type: actions.GET_USER_ROOMS_STORE,
        payload: {
          rooms: {
            room1: {
              name: 'room1',
              creator: 'user1',
              members: ['user1', 'user2'],
              type: 'group',
              readOnly: false,
              latestMsg: {
                id: 1, sender: 'user1', text: 'some text', time: 'time',
              },
              oldestMsgId: 12,
              unreadMsgCount: 10,
            },
          },
        },
      },
      wantState: {
        rooms: {
          room1: {
            name: 'room1',
            creator: 'user1',
            members: ['user1', 'user2'],
            type: 'group',
            readOnly: false,
            latestMsg: {
              id: 1, sender: 'user1', text: 'some text', time: 'time',
            },
            oldestMsgId: 12,
            unreadMsgCount: 10,
          },
        },
      },
    },
    {
      desc: 'should return state with old rooms and append new rooms',
      initialState: {
        rooms: {
          room1: {
            name: 'room1',
            creator: 'user1',
            members: ['user1', 'user2'],
            type: 'group',
            readOnly: false,
            latestMsg: null,
            oldestMsgId: 0,
            unreadMsgCount: 0,
          },
        },
      },
      action: {
        type: actions.GET_USER_ROOMS_STORE,
        payload: {
          rooms: {
            room2: {
              name: 'room2',
              creator: 'user1',
              members: ['user1', 'user2'],
              type: 'group',
              readOnly: false,
              latestMsg: {
                id: 1, sender: 'user1', text: 'some text', time: 'time',
              },
              oldestMsgId: 12,
              unreadMsgCount: 10,
            },
          },
        },
      },
      wantState: {
        rooms: {
          room1: {
            name: 'room1',
            creator: 'user1',
            members: ['user1', 'user2'],
            type: 'group',
            readOnly: false,
            latestMsg: null,
            oldestMsgId: 0,
            unreadMsgCount: 0,
          },
          room2: {
            name: 'room2',
            creator: 'user1',
            members: ['user1', 'user2'],
            type: 'group',
            readOnly: false,
            latestMsg: {
              id: 1, sender: 'user1', text: 'some text', time: 'time',
            },
            oldestMsgId: 12,
            unreadMsgCount: 10,
          },
        },
      },
    },
    {
      desc: 'should return state with replaced room',
      initialState: {
        rooms: {
          room1: {
            name: 'room1',
            creator: 'user2',
            members: ['user1', 'user2'],
            type: 'group',
          },
        },
      },
      action: {
        type: actions.GET_USER_ROOMS_STORE,
        payload: {
          rooms: {
            room1: {
              name: 'room1',
              creator: 'user1',
              members: ['user1', 'user2'],
              type: 'group',
            },
          },
        },
      },
      wantState: {
        rooms: {
          room1: {
            name: 'room1',
            creator: 'user1',
            members: ['user1', 'user2'],
            type: 'group',
          },
        },
      },
    },
  ];

  cases.forEach((c) => {
    it(c.desc, () => {
      expect(roomsReducer(c.initialState, c.action)).toEqual(c.wantState);
    });
  });
});

describe('ROOMS reducer DELETE_USERS_FROM_ROOM_STORE and USERS_DELETED action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user1', 'user2'],
        type: 'group',
      },
    },
  };
  const action = {
    type: actions.DELETE_USERS_FROM_ROOM_STORE,
    payload: {
      roomId: 'room1',
      users: ['user1'],
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
      },
    },
  };
  it('should return room without deteted members', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer USERS_DELETED action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user1', 'user2'],
        type: 'group',
      },
    },
  };
  const action = {
    type: actions.DELETE_USERS_FROM_ROOM_STORE,
    payload: {
      roomId: 'room1',
      users: ['user1'],
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
      },
    },
  };
  it('should return room without deteted members', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer CREATE_ROOM_STORE action handling', () => {
  const initialState = {
    rooms: {},
  };
  const action = {
    type: actions.CREATE_ROOM_STORE,
    payload: {
      id: 'room1',
      name: 'room1',
      creator: 'user2',
      members: ['user2'],
      type: 'group',
      readOnly: false,
      latestMsg: null,
      oldestMsgId: 0,
      unreadMsgCount: 0,
    },
  };
  const wantState = {
    activeRoomID: 'room1',
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  it('should return state with new room', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer UPDATE_ROOM action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
      },
    },
  };
  const action = {
    type: actions.UPDATE_ROOM,
    payload: {
      id: 'room1',
      name: 'room1',
      creator: 'user2',
      members: ['user1', 'user2'],
      type: 'group',
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user1', 'user2'],
        type: 'group',
      },
    },
  };
  it('should return state with updated room', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer USERS_ADDED action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  const action = {
    type: actions.USERS_ADDED,
    payload: {
      id: 'room1',
      name: 'room1',
      creator: 'user2',
      members: ['user1', 'user2'],
      type: 'group',
      readOnly: false,
      latestMsg: null,
      oldestMsgId: 0,
      unreadMsgCount: 0,
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user1', 'user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  it('should return state with updated room', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer INVITED_TO_ROOM_STORE action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  const action = {
    type: actions.INVITED_TO_ROOM_STORE,
    payload: {
      id: 'room1',
      name: 'room1',
      creator: 'user2',
      members: ['user1', 'user2'],
      type: 'group',
      readOnly: false,
      latestMsg: null,
      oldestMsgId: 0,
      unreadMsgCount: 0,
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user1', 'user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  it('should return state with updated room', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer UPDATE_ROOM_SETTINGS_STORE action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
      },
    },
  };
  const action = {
    type: actions.UPDATE_ROOM_SETTINGS_STORE,
    payload: {
      id: 'room1',
      room: {
        name: 'room2',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room2',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  it('should return state with updated room', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer ROOM_SETTINGS_UPDATED action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  const action = {
    type: actions.UPDATE_ROOM_SETTINGS_STORE,
    payload: {
      id: 'room1',
      room: {
        name: 'room2',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 10,
        unreadMsgCount: 10,
      },
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room2',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 10,
        unreadMsgCount: 10,
      },
    },
  };
  it('should return state with updated room', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer ROOM_LATEST_MESSAGE action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        latestMsg: {
          sender: 'sender2',
          text: 'text2',
          time: '2002-07-01T16:51:05Z',
          id: '2',
        },
        unreadMsgCount: 1,
      },
    },
  };
  const action = {
    type: actions.ROOM_LATEST_MESSAGE,
    payload: {
      roomId: 'room1',
      latestMsg: {
        sender: 'sender2',
        text: 'text 3',
        time: '2002-07-01T16:51:06Z',
        id: '3',
      },
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        latestMsg: {
          sender: 'sender2',
          text: 'text 3',
          time: '2002-07-01T16:51:06Z',
          id: '3',
        },
        unreadMsgCount: 1,
      },
    },
  };
  it('should return state with updated room and incremented unreadMsgCount by 1', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer ROOM_SETTINGS_UPDATED action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 0,
        unreadMsgCount: 0,
      },
    },
  };
  const action = {
    type: actions.UPDATE_ROOM_SETTINGS_STORE,
    payload: {
      id: 'room1',
      room: {
        name: 'room2',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 10,
        unreadMsgCount: 10,
      },
    },
  };
  const wantState = {
    rooms: {
      room1: {
        name: 'room2',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        readOnly: false,
        latestMsg: null,
        oldestMsgId: 10,
        unreadMsgCount: 10,
      },
    },
  };
  it('should return state with updated room', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});

describe('ROOMS reducer DELETE_ROOM_STORE action handling', () => {
  const initialState = {
    rooms: {
      room1: {
        name: 'room1',
        creator: 'user2',
        members: ['user2'],
        type: 'group',
        latestMsg: {
          sender: 'sender2',
          text: 'text2',
          time: '2002-07-01T16:51:05Z',
          id: '2',
        },
        unreadMsgCount: 1,
      },
    },
  };
  const action = {
    type: actions.DELETE_ROOM_STORE,
    payload: {
      roomId: 'room1',
    },
  };
  const wantState = {
    rooms: {},
  };
  it('should return state with rooms which doesnt consist room1', () => {
    expect(roomsReducer(initialState, action)).toEqual(wantState);
  });
});
