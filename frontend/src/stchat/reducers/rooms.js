import actions from '../actions';

const roomsInitialState = {
  activeRoomID: '',
  prevActiveRoomID: '',
  rooms: {},
};

// room store structure
// {
//   activeRoomID: 'room-id-string',
//   rooms: {
//     'room-id-string': {
//       name: 'room name',
//       members: ['user1', 'user2'],
//       creator: 'user1',
//       type: 'group'/'private',
//       readOnly: true/false ,
//       latestMsg: {
//         sender: 'user2',
//         text: 'some text',
//         id: 3,
//         time: '2014-01-01T23:28:56.782Z',
//       },
//       oldestMsgId: 1,
//       unreadMsgCount: 1 ,
//     }
//   }
// }

export default (state = roomsInitialState, action) => {
  switch (action.type) {
  case actions.SET_ACTIVE_ROOM: {
    return {
      ...state,
      activeRoomID: action.payload,
    };
  }
  case actions.SET_PREV_ACTIVE_ROOM:
    return {
      ...state,
      prevActiveRoomID: action.payload,
    };
  case actions.GET_USER_ROOMS_STORE: {
    const { rooms: oldRooms } = state;
    const { rooms } = action.payload;
    return {
      ...state,
      rooms: { ...oldRooms, ...rooms },
    };
  }
  case actions.UPDATE_ROOM: {
    const { rooms } = state;
    const {
      id,
    } = action.payload;
    rooms[id] = { ...rooms[id], ...action.payload };
    return {
      ...state,
      rooms: { ...rooms },
    };
  }
  case actions.UPDATE_ROOM_SETTINGS_STORE:
  case actions.ROOM_SETTINGS_UPDATED: {
    const { rooms } = state;
    const {
      id,
      room,
    } = action.payload;
    rooms[id] = {
      ...rooms[id],
      ...room,
    };
    return {
      ...state,
      rooms: { ...rooms },
    };
  }
  case actions.ROOM_LATEST_MESSAGE: {
    const { rooms } = state;
    const { roomId, latestMsg } = action.payload;
    return {
      ...state,
      rooms: {
        ...rooms,
        [roomId]: {
          ...rooms[roomId],
          latestMsg,
        },
      },
    };
  }
  case actions.DELETE_ROOM_STORE: {
    const { rooms } = state;
    const { roomId } = action.payload;
    delete rooms[roomId];

    return {
      ...state,
      rooms: { ...rooms },
    };
  }
  default:
    return state;
  }
};
