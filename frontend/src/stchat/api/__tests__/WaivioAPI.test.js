import { searchUser, getUserDelails } from '../WaivioAPI';

const mockFetch = (data, ok, status) => jest.fn().mockImplementation(() => Promise.resolve({
  ok,
  status,
  json: () => JSON.parse(data),
}));

describe('searchUser', () => {
  it('when fetch resolves should resolve with jsonData',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(searchUser('a', 10))
        .resolves
        .toStrictEqual(JSON.parse(jsonData));
    });

  it('when usernameExcerpt is empty should reject whit error',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(searchUser('', 10))
        .rejects
        .toThrow(Error('usernameExcerpt isn\'t string type or have empty value'));
    });

  it('when usernameExcerpt is undefined should reject whit error',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(searchUser(undefined, 10))
        .rejects
        .toThrow(Error('usernameExcerpt isn\'t string type or have empty value'));
    });

  it('when maxEntries is 0 should reject whit error',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(searchUser('a', 0))
        .rejects
        .toThrow(Error('maxEntries isn\'t number type or heave a negative, zero, bigger than 100 value'));
    });

  it('when maxEntries is negative should reject whit error',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(searchUser('a', -1))
        .rejects
        .toThrow(Error('maxEntries isn\'t number type or heave a negative, zero, bigger than 100 value'));
    });

  it('when maxEntries is bigger than 100 should reject whit error',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(searchUser('a', 101))
        .rejects
        .toThrow(Error('maxEntries isn\'t number type or heave a negative, zero, bigger than 100 value'));
    });

  it('when fetch resolves response.ok false should reject whit error',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = false;
      const status = 400;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(searchUser('a', 10))
        .rejects
        .toThrow(Error(`Request failed: response code ${status}`));
    });
});

describe('getUserDelails', () => {
  it('when userID presented should resolve with jsonData',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(getUserDelails('a'))
        .resolves
        .toStrictEqual(JSON.parse(jsonData));
    });

  it('when userID is undefined should resolve with jsonData',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(getUserDelails(undefined))
        .rejects
        .toThrow(Error('userID isn\'t string type or have empty value'));
    });

  it('when userID value is empty should resolve with jsonData',
    async () => {
      const jsonData = '{"a": 1}';
      const ok = true;
      const status = 200;
      window.fetch = mockFetch(jsonData, ok, status);
      await expect(getUserDelails(''))
        .rejects
        .toThrow(Error('userID isn\'t string type or have empty value'));
    });
});
