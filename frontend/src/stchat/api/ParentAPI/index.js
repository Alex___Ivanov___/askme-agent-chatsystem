import config from '../../config';

const { originWhitelist } = config;
let currentOrigin = '';


const sendMessage = (msg, origin) => {
  window.parent.postMessage(msg, origin);
};

let openChat = () => {};

export const onOpenChat = (openChatCb) => {
  openChat = openChatCb;
};

export const sendConnected = () => {
  originWhitelist.forEach((origin) => {
    sendMessage({
      cmd: 'connected',
    }, origin);
  });
};

export default (eventName) => {
  sendMessage({
    cmd: 'new_event',
    args: {
      name: eventName,
    },
  }, currentOrigin);
};

export const sendCloseChat = () => {
  sendMessage({
    cmd: 'close_chat',
  }, currentOrigin);
};

const routeMsg = (msg) => {
  const { origin } = msg;
  if (!originWhitelist.includes(origin)) {
    console.error('ParentAPI: message has been received from invalid origin');
    return;
  }

  const { cmd } = msg.data;

  if (typeof cmd === 'undefined') {
    console.log('ParentAPI: post message data has no "cmd" property');
    return;
  }

  switch (cmd) {
  case 'open_chat':

    openChat();
    break;
  case 'register_origin':
    currentOrigin = origin;
    break;
  default:
    console.error(`ParentAPI: invalid cmd: ${cmd}`);
  }
};

window.addEventListener('message', routeMsg);
