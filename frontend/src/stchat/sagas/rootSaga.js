import { all, takeEvery } from 'redux-saga/effects';
import actions from '../actions';
import openChat from './openChat';
import getUserRooms from './getUserRooms';
import getHistoryMessages from './getHistoryMessages';
import sendMessage from './sendMessage';
import updateRoomSettings from './updateRoomSettings';
import getRoomUnreadMessages from './getRoomUnreadMessages';
import messageReceived from './receivedMessage';
import nextUnreadMessage from './nextUnreadMessage';
import nextUnreadId from './nextUnreadId';
import changeBotActivity from './changeBotActivity';
import closeRoom from './closeRoom';
import appendToAllPendingRooms from './appendToAllPendingRooms';

export default function* rootSaga() {
  yield all([
    takeEvery(actions.OPEN_CHAT, openChat),
    takeEvery(actions.GET_USER_ROOMS, getUserRooms),
    takeEvery(actions.GET_MESSAGES, getHistoryMessages),
    takeEvery(actions.SEND_MESSAGE, sendMessage),
    takeEvery(actions.UPDATE_ROOM_SETTINGS, updateRoomSettings),
    takeEvery(actions.GET_ROOM_UNREAD_MESSAGES, getRoomUnreadMessages),
    takeEvery(actions.MESSAGE_RECEIVED, messageReceived),
    takeEvery(actions.NEXT_UNREAD_MESSAGE, nextUnreadMessage),
    takeEvery(actions.NEXT_UNREAD_ID, nextUnreadId),
    takeEvery(actions.CHANGE_BOT_ACTIVITY, changeBotActivity),
    takeEvery(actions.CLOSE_ROOM, closeRoom),
    takeEvery(actions.APPEND_TO_ALL_PENDING_ROOMS, appendToAllPendingRooms),
  ]);
}
