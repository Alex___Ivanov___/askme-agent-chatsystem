import { call, put } from 'redux-saga/effects';
import * as chatAPI from '../api/ChatAPI';
import actions from '../actions';

export default function* deleteRoom(a) {
  const { roomId } = a.payload;
  if (typeof roomId !== 'string' || roomId.length === 0) {
    console.log(new Error('invalid roomId param'));
    return;
  }
  try {
    yield call(chatAPI.closeRoom, roomId);
    yield put({
      type: actions.SET_ACTIVE_ROOM,
      payload: '',
    });
    yield put({
      type: actions.DELETE_ROOM_STORE,
      payload: {
        roomId,
      },
    });
  } catch (e) {
    console.log(e);
  }
}
