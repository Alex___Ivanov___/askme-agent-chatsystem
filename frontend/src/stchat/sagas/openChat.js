import { put, select } from 'redux-saga/effects';
import {
  getRooms,
  getPrevActiveRoomId,
} from '../selectors';
import {
  setActiveRoom,
  unsetPrevActiveRoom,
} from '../actionCreators';


export default function* openChat() {
  try {
    const prevActiveRoomID = yield select(getPrevActiveRoomId);
    if (prevActiveRoomID === '') {
      return;
    }
    const rooms = yield select(getRooms);
    if (!rooms[prevActiveRoomID]) {
      yield put(unsetPrevActiveRoom());
      return;
    }
    yield put(setActiveRoom(prevActiveRoomID));
  } catch (e) {
    console.error(e.message);
  }
}
