import { call, put } from 'redux-saga/effects';
import createRoom from '../createRoom';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';


describe('createRoom', () => {
  let generator;
  const members = ['user_1', 'user_2'];
  const name = 'room name';
  const readOnly = false;
  const moderable = false;
  let chatApiResponse = null;
  const action = {
    payload: null,
  };
  let logMessages;

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    chatApiResponse = {
      name: 'room_name',
      members: ['user_1', 'user_2'],
      id: 'id_room',
      readOnly: false,
      moderable,
    };

    action.payload = {
      members,
      name,
      readOnly,
      moderable,
    };
    generator = createRoom(action);
  });

  test('Should call log when members isn`t array', () => {
    // arrange
    action.payload.members = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid members param');
  });

  test('Should call log when members has length is zero', () => {
    // arrange
    action.payload.members = [];

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid members param');
  });

  test('Should call log when name ins`t string', () => {
    // arrange
    action.payload.name = [];

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomName param');
  });

  test('Should call log when name is empty', () => {
    // arrange
    action.payload.name = '';

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomName param');
  });

  test('Should call log when readOnly is invalid', () => {
    // arrange
    action.payload.readOnly = '';

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid readOnly param');
  });

  test('Should call log when moderable is invalid', () => {
    // arrange
    action.payload.moderable = undefined;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid moderable param');
  });

  test('Should call log when chatAPI.createRoom returns Promise.reject', () => {
    // arrange
    const error = new Error('some error');

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.createRoom, action.payload));
    expect(generator.throw(error).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(error);
  });

  test('Should call log when chatAPI.addUsersToRoom returns Promise.resolve with invalid members field', () => {
    // arrange
    const error = new Error('invalid members');
    chatApiResponse.members = null;

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.createRoom, action.payload));
    expect(generator.next(chatApiResponse).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(error);
  });

  test('Should call log when chatAPI.addUsersToRoom returns Promise.resolve with invalid id field', () => {
    // arrange
    const error = new Error('invalid id format');
    chatApiResponse.id = null;

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.createRoom, action.payload));
    expect(generator.next(chatApiResponse).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(error);
  });

  test('Should call log when chatAPI.addUsersToRoom returns Promise.resolve with invalid name field', () => {
    // arrange
    const error = new Error('invalid name');
    chatApiResponse.name = null;

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.createRoom, action.payload));
    expect(generator.next(chatApiResponse).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(error);
  });

  test('Should call log when chatAPI.addUsersToRoom returns Promise.resolve with invalid readOnly field', () => {
    // arrange
    const error = new Error('invalid readOnly');
    chatApiResponse.readOnly = null;

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.createRoom, action.payload));
    expect(generator.next(chatApiResponse).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(error);
  });

  test('Should call log when chatAPI.addUsersToRoom returns Promise.resolve with invalid moderable field', () => {
    // arrange
    const error = new Error('invalid moderable');
    chatApiResponse.moderable = undefined;

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.createRoom, action.payload));
    expect(generator.next(chatApiResponse).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(error);
  });

  test('Should call log when chatAPI.addUsersToRoom returns Promise.resolve with valid room fields', () => {
    // arrange
    const wantAction = {
      type: actions.CREATE_ROOM_STORE,
      payload: chatApiResponse,
    };

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.createRoom, action.payload));
    expect(generator.next(chatApiResponse).value).toEqual(put(wantAction));
    expect(generator.next().done).toBeTruthy();
  });
});
