import { call, put } from 'redux-saga/effects';
import addUsersToRoom from '../addUsersToRoom';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';


describe('addUsersToRoom', () => {
  let logMessages;
  let generator;
  const users = ['user_1', 'user_2'];
  const roomId = 'room_id';
  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      users,
      roomId,
    };
    generator = addUsersToRoom(action);
  });

  test('Should call console.error when users field isn`t array', () => {
    // arrange
    action.payload.roomId = undefined;
    action.payload.users = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid users param');
  });

  test('Should call console.error when users has length is zero', () => {
    // arrange
    action.payload.users = [];

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid users param');
  });

  test('Should call console.error when roomId ins`t string', () => {
    // arrange
    action.payload.roomId = [];

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  test('Should call console.error when roomId is empty', () => {
    // arrange
    action.payload.roomId = '';

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  test('Should call console.error when chatAPI.addUsersToRoom returns Promise.reject', () => {
    // assert
    expect(generator.next().value).toEqual(call(chatAPI.addUsersToRoom, users, roomId));
    expect(generator.throw(new Error('some error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('some error');
  });

  test('Should call console.error when chatAPI.addUsersToRoom returns Promise.resolve with members field isn`t valid',
    () => {
      // arrange
      const chatApiResponse = {
        members: 'null',
      };

      // assert
      expect(generator.next().value).toEqual(call(chatAPI.addUsersToRoom, users, roomId));
      expect(generator.next(chatApiResponse).done).toBeTruthy();
      expect(logMessages.length).toBe(1);
      expect(logMessages[0].message).toEqual('chatAPI.addUsersToRoom: "members" isn`t valid');
    });

  test('Should call console.error when chatAPI.addUsersToRoom returns Promise.resolve and puts to store', () => {
    // arrange
    const wantAction = {
      type: actions.UPDATE_ROOM_SETTINGS_STORE,
      payload: {
        id: roomId,
        room: {
          members: [...users, 'user_3'],
        },
      },
    };
    const chatApiResponse = {
      members: ['user_1', 'user_2', 'user_3'],
    };

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.addUsersToRoom, users, roomId));
    expect(generator.next(chatApiResponse).value).toEqual(put(wantAction));
    expect(generator.next().done).toBeTruthy();
  });
});
