import { call, put, select } from 'redux-saga/effects';
import getUserRooms from '../getUserRooms';
import { getUserInfo } from '../../selectors';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';


describe('getUserRooms', () => {
  let logMessages;
  let generator;
  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };
    generator = getUserRooms();
  });

  test('Should call log when rooms field of args isn`t array', () => {
    // arrange
    const chatApiResponse = {
      rooms: undefined,
    };

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.getUserRooms));
    expect(generator.next(chatApiResponse).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual('invalid rooms format');
  });

  test('Should do nothing when rooms length is zero', () => {
    // arrange
    const chatApiResponse = {
      rooms: [],
    };

    // act
    generator.next();

    // assert
    expect(generator.next(chatApiResponse).value).toEqual(select(getUserInfo));
    expect(generator.next().done).toBeTruthy();
  });

  test('Should put GET_USER_ROOMS action when we have a private room with participant name', () => {
    // arrange
    const room = {
      id: 'room_id',
      name: 'room_name',
      members: ['user_1', 'test_username'],
      creator: 'test_username',
      type: 'private',
      readOnly: false,
      unreadMsgCount: 0,
      oldestMsgId: 0,
      latestMsg: null,
      moderable: false,
    };

    const chatApiResponse = {
      rooms: [room],
    };
    const username = 'test_username';
    const wantAction = {
      type: actions.GET_USER_ROOMS_STORE,
      payload: {
        rooms: {
          room_id: {
            name: 'user_1',
            members: ['user_1', 'test_username'],
            creator: 'test_username',
            type: 'private',
            readOnly: false,
            unreadMsgCount: 0,
            oldestMsgId: 0,
            latestMsg: null,
            moderable: false,
          },
        },
      },
    };

    // act
    generator.next();
    generator.next(chatApiResponse);

    // assert
    expect(generator.next(username).value).toEqual(put(wantAction));
    expect(generator.next().done).toBeTruthy();
  });

  test('Should put GET_USER_ROOMS action when we have a public room without participant name', () => {
    // arrange
    const room = {
      id: 'room_id',
      name: 'room_name',
      members: ['user_1', 'test_username', 'user_2'],
      creator: 'test_username',
      type: 'public',
      readOnly: false,
      unreadMsgCount: 0,
      oldestMsgId: 0,
      latestMsg: null,
      moderable: false,
    };

    const chatApiResponse = {
      rooms: [room],
    };
    const username = 'test_username';
    const wantAction = {
      type: actions.GET_USER_ROOMS_STORE,
      payload: {
        rooms: {
          room_id: {
            name: 'room_name',
            members: ['user_1', 'test_username', 'user_2'],
            creator: 'test_username',
            type: 'public',
            readOnly: false,
            unreadMsgCount: 0,
            oldestMsgId: 0,
            latestMsg: null,
            moderable: false,
          },
        },
      },
    };

    // act
    generator.next();
    generator.next(chatApiResponse);

    // assert
    expect(generator.next(username).value).toEqual(put(wantAction));
    expect(generator.next().done).toBeTruthy();
  });

  test('Should call log when someone throws error', () => {
    // arrange
    const wantErrorMsg = new Error('some error');

    // act
    generator.next(chatAPI.getUserRooms);

    // assert
    expect(generator.throw(new Error('some error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(wantErrorMsg);
  });
});
