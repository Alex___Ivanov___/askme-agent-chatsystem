import { select, put } from 'redux-saga/effects';
import roomDeleted from '../roomDeleted';
import { getActiveRoomId } from '../../selectors';
import actions from '../../actions';

describe('roomDeleted', () => {
  let logMessages;
  let generator;
  const roomId = 'room_id';
  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.log = (...args) => {
      logMessages.push(...args);
    };
    action.payload = {
      roomId,
    };
    generator = roomDeleted(action);
  });

  it('Should call log when roomId isn`t valid', () => {
    // arrange
    action.payload.roomId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  it('Should call log when call returns error', () => {
    // assert
    expect(generator.next().value).toEqual(select(getActiveRoomId));
    expect(generator.throw(new Error('some error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('some error');
  });

  it('Should call put actions.SET_ACTIVE_ROOM and actions.DELETE_ROOM_STORE', () => {
    // assert
    expect(generator.next().value).toEqual(select(getActiveRoomId));
    expect(generator.next('room_id').value).toEqual(put({
      type: actions.SET_ACTIVE_ROOM,
      payload: '',
    }));
    expect(generator.next().value).toEqual(put({
      type: actions.DELETE_ROOM_STORE,
      payload: {
        roomId: 'room_id',
      },
    }));
    expect(logMessages.length).toBe(0);
  });

  it('Should call put actions.DELETE_ROOM_STORE', () => {
    // assert
    expect(generator.next().value).toEqual(select(getActiveRoomId));
    expect(generator.next('room_id_2').value).toEqual(put({
      type: actions.DELETE_ROOM_STORE,
      payload: {
        roomId: 'room_id',
      },
    }));
    expect(logMessages.length).toBe(0);
  });
});
