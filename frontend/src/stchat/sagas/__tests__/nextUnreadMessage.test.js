import { call, put, select } from 'redux-saga/effects';
import nextUnreadMessage from '../nextUnreadMessage';
import { getMessages, getRooms, getUsername } from '../../selectors';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';

describe('nextUnreadMessage', () => {
  let logMessages;
  const msgId = 0;
  const roomId = 'room_id';
  const action = {
    payload: null,
  };
  const roomsState = {
    room_id: {
      oldestMsgId: 1,
    },
  };
  const messagesState = {};

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      roomId,
      msgId,
    };
  });

  it('Should call log when payload has empty room id value', () => {
    // arrange
    action.payload.roomId = '';

    // act
    const generator = nextUnreadMessage(action);

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('nextUnreadMessage saga: action roomId value is empty');
  });

  it('Should call log when payload has invalid msgId value', () => {
    // arrange
    action.payload.msgId = -1;

    // act
    const generator = nextUnreadMessage(action);

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('nextUnreadMessage saga: action msgId value lt 0');
  });

  it('Should select rooms', () => {
    // act
    const generator = nextUnreadMessage(action);

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(logMessages.length).toBe(0);
  });

  it('Should select messages state', () => {
    // act
    const generator = nextUnreadMessage(action);

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(roomsState).value).toEqual(select(getMessages));
    expect(logMessages.length).toBe(0);
  });

  it('Should call log when chatAPI.nextUnreadMessage throw exception', () => {
    // arrange
    const wantError = new Error('sendMessage error');
    // act
    const generator = nextUnreadMessage(action);

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(roomsState).value).toEqual(select(getMessages));
    expect(generator.next(messagesState).value)
      .toEqual(call(chatAPI.nextUnreadMessage, roomId, msgId));
    expect(generator.throw(new Error('sendMessage error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(wantError);
  });

  it('Should put UPDATE_ROOM_SETTINGS_STORE action', () => {
    // assert
    const updateRoomAction = {
      type: actions.UPDATE_ROOM_SETTINGS_STORE,
      payload: {
        id: roomId,
        room: {
          oldestMsgId: 0,
          unreadMsgCount: 0,
        },
      },
    };
    // act
    const generator = nextUnreadMessage(action);

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(roomsState).value).toEqual(select(getMessages));
    expect(generator.next(messagesState).value).toEqual(call(chatAPI.nextUnreadMessage, roomId, msgId));
    expect(generator.next().value).toEqual(select(getUsername));
    expect(generator.next('user').value).toEqual(put(updateRoomAction));
    expect(logMessages.length).toBe(0);
  });
});
