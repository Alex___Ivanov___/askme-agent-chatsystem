import { select, put } from 'redux-saga/effects';
import nextUnreadId from '../nextUnreadId';
import actions from '../../actions';
import { getRooms } from '../../selectors';


describe('nextUnreadId saga', () => {
  let logMessages;
  let generator;
  const messageId = 2;
  const roomId = 'room_id';
  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      messageId,
      roomId,
    };
    generator = nextUnreadId(action);
  });

  test('Should call log, when, payload has roomId field which is not valid', () => {
    // arrange
    const invalidParamError = new Error('invalid roomId parameter');
    action.payload.roomId = '';

    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toEqual(1);
    expect(logMessages[0]).toEqual(invalidParamError);
  });

  test('Should call log, when, payload has messageId field which is not valid', () => {
    // arrange
    const invalidParamError = new Error('invalid messageId parameter');
    action.payload.messageId = '';

    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toEqual(1);
    expect(logMessages[0]).toEqual(invalidParamError);
  });

  test('Should put UPDATE_ROOM_SETTINGS_STORE action with messageId from payload, when messageId isn`t zero', () => {
    // arrange
    action.payload.messageId = 2;

    const wantAction = {
      type: actions.UPDATE_ROOM_SETTINGS_STORE,
      payload: {
        id: roomId,
        room: {
          oldestMsgId: messageId,
          unreadMsgCount: 3,
        },
      },
    };

    const selectedRooms = {
      room_id: {
        unreadMsgCount: 3,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(selectedRooms).value).toEqual(put(wantAction));
    expect(logMessages.length).toEqual(0);
  });

  test('Should put UPDATE_ROOM_SETTINGS_STORE action with messageId from payload, when messageId is zero', () => {
    // arrange
    action.payload.messageId = 0;

    const wantAction = {
      type: actions.UPDATE_ROOM_SETTINGS_STORE,
      payload: {
        id: roomId,
        room: {
          oldestMsgId: 0,
          unreadMsgCount: 0,
        },
      },
    };

    const selectedRooms = {
      room_id: {
        unreadMsgCount: 0,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(selectedRooms).value).toEqual(put(wantAction));
    expect(logMessages.length).toEqual(0);
  });

  test('Should call log, when select throw error', () => {
    // arrange
    const error = new Error('throw error');

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.throw(error).done).toBeTruthy();
    expect(logMessages.length).toEqual(1);
    expect(logMessages[0]).toEqual(error);
  });
});
