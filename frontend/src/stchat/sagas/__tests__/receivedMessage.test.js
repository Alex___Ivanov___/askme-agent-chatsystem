import { put, select } from 'redux-saga/effects';
import messageReceived from '../receivedMessage';
import {
  getRooms,
  getMessages,
  getUsername,
} from '../../selectors';
import actions from '../../actions';


describe('messageReceived saga', () => {
  let logMessages;
  let generator;
  const message = {
    sender: 'user_1',
    text: 'message_text',
    roomId: 'room_id',
    id: 10,
    time: '2002-07-01T16:50:05Z',
    isModerated: 2,
  };

  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = { ...message };
    generator = messageReceived(action);
  });

  test('Should call log when payload has "sender" field which isn`t array', () => {
    // arrange
    action.payload.sender = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid "sender" parameter');
  });

  test('Should call log when payload has "text" field which isn`t valid', () => {
    // arrange
    action.payload.text = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid "text" parameter');
  });

  test('Should call log when payload has "roomId" field which isn`t valid', () => {
    // arrange
    action.payload.roomId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid "roomId" parameter');
  });

  test('Should call log when payload has "id" field which isn`t valid', () => {
    // arrange
    action.payload.id = -1;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid "id" parameter');
  });

  test('Should call log when payload has "time" field which isn`t valid', () => {
    // arrange
    action.payload.time = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid "time" parameter');
  });

  test('Should call log when payload has "isModerated" field which hasn`t valid type', () => {
    // arrange
    action.payload.isModerated = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid "isModerated" parameter');
  });

  test('Should call log when payload has "isModerated" field which less than 0', () => {
    // arrange
    action.payload.isModerated = -1;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid "isModerated" parameter');
  });

  test('Should put UPDATE_ROOM_SETTINGS_STORE, ROOM_LATEST_MESSAGE actions, when isAppend is false',
    () => {
    // arrange
      const selectedRooms = {
        room_id: {
          latestMsg: {
            id: 12,
          },
        },
      };
      const wantUpdateRoomSettingsAction = {
        type: actions.UPDATE_ROOM_SETTINGS_STORE,
        payload: {
          id: 'room_id',
          room: {
            oldestMsgId: 10,
            unreadMsgCount: 1,
          },
        },
      };

      const wantRoomLatestMessageAction = {
        type: actions.ROOM_LATEST_MESSAGE,
        payload: {
          roomId: 'room_id',
          latestMsg: { ...action.payload },
        },
      };

      // assert
      expect(generator.next().value).toEqual(select(getMessages));
      expect(generator.next({}).value).toEqual(select(getRooms));
      expect(generator.next(selectedRooms).value).toEqual(select(getUsername));
      expect(generator.next({}).value).toEqual(put(wantUpdateRoomSettingsAction));
      expect(generator.next({}).value).toEqual(put(wantRoomLatestMessageAction));
      expect(generator.next().done).toBeTruthy();
      expect(logMessages.length).toBe(0);
    });

  test('Should put UPDATE_ROOM_SETTINGS_STORE, MESSAGE_RECEIVED_STORE, '
    + 'CHAT_UPDATED, ROOM_LATEST_MESSAGE actions, when isAppend is true',
  () => {
    // arrange
    const selectedRooms = {
      room_id: {
        latestMsg: {
          id: 12,
        },
      },
    };
    const selectedMessages = {
      room_id: {
        messages: [
          { id: 12 },
        ],
      },
    };

    const wantUpdateRoomSettingsAction = {
      type: actions.UPDATE_ROOM_SETTINGS_STORE,
      payload: {
        id: 'room_id',
        room: {
          oldestMsgId: 10,
          unreadMsgCount: 1,
        },
      },
    };

    const wantMessageReceivedStore = {
      type: actions.MESSAGE_RECEIVED_STORE,
      payload: { ...action.payload },
    };

    const wantChatUpdated = {
      type: actions.CHAT_UPDATED,
      payload: {
        client: action.payload.sender,
        roomId: action.payload.roomId,
      },
    };

    const wantRoomLatestMessageAction = {
      type: actions.ROOM_LATEST_MESSAGE,
      payload: {
        roomId: 'room_id',
        latestMsg: { ...action.payload },
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(selectedMessages).value).toEqual(select(getRooms));
    expect(generator.next(selectedRooms).value).toEqual(select(getUsername));
    expect(generator.next({}).value).toEqual(put(wantUpdateRoomSettingsAction));
    expect(generator.next().value).toEqual(put(wantMessageReceivedStore));
    expect(generator.next().value).toEqual(put(wantChatUpdated));
    expect(generator.next().value).toEqual(put(wantRoomLatestMessageAction));
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });

  test('Should call catch, when one of all effects throw error', () => {
    // arrange
    const someError = new Error('some error');

    // assert
    generator.next();
    expect(generator.throw(someError).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(someError);
  });
});
