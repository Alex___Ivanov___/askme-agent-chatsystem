import { select, put } from 'redux-saga/effects';
import approvedMessage from '../approvedMessage';
import actions from '../../actions';
import { getMessages } from '../../selectors';


describe('approvedMessage', () => {
  let logMessages;
  const action = {};
  let generator;

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      message: {
        roomId: 'room_id',
        sender: 'user1',
      },
      oldId: 123444,
    };

    generator = approvedMessage(action);
  });

  test('Should call log when payload has invalid oldId value', () => {
    // arrange
    action.payload.oldId = -1;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0])
      .toEqual('approvedMessage saga: invalid oldId parameter');
  });

  test('Should call log when payload has invalid roomId value', () => {
    // arrange
    action.payload.message.roomId = '';

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0])
      .toEqual('approvedMessage saga: invalid roomId parameter');
  });

  test('Should call log when payload has invalid sender value', () => {
    // arrange
    action.payload.message.sender = '';

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0])
      .toEqual('approvedMessage saga: invalid sender parameter');
  });

  test('Should call yield select(getMessages) and return '
    + 'when store does not consist required room', () => {
    // arrange
    const messagesState = {};

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesState).done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });

  test('Should call yield select(getMessages) and return '
    + 'when room does not consist messages field', () => {
    // arrange
    const messagesState = {
      roomID: {},
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesState).done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });

  test('Should call yield put({type:"actions.SET_MESSAGES_STORE"})', () => {
    // arrange
    const messagesState = {
      room_id: {
        messages: [
          {
            id: 123443,
          },
          {
            id: 123444,
          },
          {
            id: 123445,
          },
        ],
      },
    };

    const wantMessagesState = {
      room_id: {
        messages: [
          {
            roomId: 'room_id',
            sender: 'user1',
          },
          {
            id: 123443,
          },
          {
            id: 123445,
          },
        ],
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesState).value).toEqual(put({
      type: actions.SET_MESSAGES_STORE,
      payload: wantMessagesState,
    }));
    expect(generator.next().value).toEqual(put({
      type: actions.CHAT_UPDATED,
      payload: {
        client: 'user1',
        roomId: 'room_id',
      },
    }));
    expect(generator.next().value).toEqual(put({
      type: actions.UPDATE_ROOM_SETTINGS_STORE,
      payload: {
        id: 'room_id',
        room: {
          latestMsg: {
            roomId: 'room_id',
            sender: 'user1',
          },
        },
      },
    }));
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });
});
