import { call, put } from 'redux-saga/effects';
import startPrivate from '../startPrivate';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';


describe('startPrivate chat saga', () => {
  let logs;
  const action = { payload: undefined };
  console.error = (...args) => {
    logs.push([...args].join(' '));
  };

  beforeEach(() => {
    logs = [];
  });

  test('should return and log when partner isn\'t string or is empty', () => {
    // arrange
    action.payload = { partner: undefined };
    const gen = startPrivate(action);

    // act
    const genVal = gen.next().done;
    // assert
    expect(genVal).toBeTruthy();
    expect(logs[0]).toEqual('startPrivate saga: partner param is invalid');
  });

  test('should call chatAPI.startChat correctly', () => {
    // arrange
    const partner = 'valera';
    const id = '123456';
    action.payload = { partner };
    const gen = startPrivate(action);
    const room = { id };
    const wantUpdateRoomAct = {
      type: actions.UPDATE_ROOM,
      payload: { id, name: partner },
    };
    const wantSetActiveRoomAct = {
      type: actions.SET_ACTIVE_ROOM,
      payload: id,
    };
    // assert
    expect(gen.next().value).toEqual(call(chatAPI.startChat, partner));
    expect(gen.next(room).value).toEqual(put(wantUpdateRoomAct));
    expect(gen.next().value).toEqual(put(wantSetActiveRoomAct));
    expect(logs.length).toEqual(0);
  });

  test('should log error when chatAPI.startChat fails', () => {
    // arrange
    const partner = 'valera';
    action.payload = { partner };
    const gen = startPrivate(action);
    // assert
    expect(gen.next().value).toEqual(call(chatAPI.startChat, partner));
    expect(gen.throw(new Error('chatAPI.startChat failed')).done).toBeTruthy();
    expect(logs[0]).toEqual('chatAPI.startChat failed');
  });
});
