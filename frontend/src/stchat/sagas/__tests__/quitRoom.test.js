import { call, put } from 'redux-saga/effects';
import quitRoom from '../quitRoom';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';

describe('quitRoom saga', () => {
  let logMessages;
  let generator;
  const roomId = 'room_id';
  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.log = (...args) => {
      logMessages.push(...args);
    };
    action.payload = {
      roomId,
    };
    generator = quitRoom(action);
  });

  it('Should call log when roomId isn`t valid', () => {
    // arrange
    action.payload.roomId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  it('Should call log when call returns error', () => {
    // assert
    expect(generator.next().value).toEqual(call(chatAPI.quitRoom, 'room_id'));
    expect(generator.throw(new Error('some error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('some error');
  });

  it('Should call put actions.SET_ACTIVE_ROOM and actions.DELETE_ROOM_STORE', () => {
    // assert
    expect(generator.next().value).toEqual(call(chatAPI.quitRoom, 'room_id'));
    expect(generator.next().value).toEqual(put({
      type: actions.SET_ACTIVE_ROOM,
      payload: '',
    }));
    expect(generator.next().value).toEqual(put({
      type: actions.DELETE_ROOM_STORE,
      payload: {
        roomId: 'room_id',
      },
    }));
    expect(logMessages.length).toBe(0);
  });
});
