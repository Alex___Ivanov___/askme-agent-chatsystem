import { call, select, put } from 'redux-saga/effects';
import approveModMessage from '../approveModMessage';
import actions from '../../actions';
import { getMessages } from '../../selectors';
import * as chatAPI from '../../api/ChatAPI';

describe('approveModMessage', () => {
  let logMessages;
  const action = {};
  let generator;
  const id = 1;
  const roomId = 'roomId';

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      id,
      roomId,
    };

    generator = approveModMessage(action);
  });

  it('Should call log when payload has invalid id value', () => {
    // arrange
    action.payload.id = -1;

    // assert
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0])
      .toEqual('approveModMessage saga: invalid id parameter');
  });

  it('Should call log when payload has invalid roomId value', () => {
    // arrange
    action.payload.roomId = '';

    // assert
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0])
      .toEqual('approveModMessage saga: invalid roomId parameter');
  });

  it('Should call log when chatAPI.approveModMessage throw exception', () => { // assert
    expect(generator.next().value).toEqual(call(chatAPI.approveModMessage, id, roomId));
    expect(generator.throw(new Error('some error')).done).toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0])
      .toEqual(new Error('some error'));
  });

  it('Should call select(getMessages)', () => {
    // arrange
    const messagesState = {};
    const resp = {};
    // assert
    expect(generator.next().value).toEqual(call(chatAPI.approveModMessage, id, roomId));
    expect(generator.next(resp).value).toEqual(select(getMessages));
    expect(generator.next(messagesState).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0])
      .toEqual('roomMessages isn`t existed');
  });

  it('Should call '
    + 'put({type: actions.SET_MESSAGES_STORE...})'
    + 'put({type: actions.CHAT_UPDATED...})'
    + 'put({type: actions.UPDATE_ROOM_SETTINGS_STORE...})',
  () => {
    // arrange
    const messagesState = {
      roomId: {
        messages: [
          {
            id: 1,
          },
        ],
      },
    };
    const resp = {
      oldId: 1,
      message: {
        id: 2,
        sender: 'sender',
      },
    };
    // assert
    expect(generator.next().value)
      .toEqual(call(chatAPI.approveModMessage, id, roomId));

    expect(generator.next(resp).value)
      .toEqual(select(getMessages));

    expect(generator.next(messagesState).value)
      .toEqual(put({
        type: actions.SET_MESSAGES_STORE,
        payload: {
          roomId: {
            messages: [
              resp.message,
            ],
          },
        },
      }));

    expect(generator.next().value)
      .toEqual(put({
        type: actions.CHAT_UPDATED,
        payload: {
          client: 'sender',
          roomId,
        },
      }));

    expect(generator.next().value)
      .toEqual(put({
        type: actions.UPDATE_ROOM_SETTINGS_STORE,
        payload: {
          id: roomId,
          room: {
            latestMsg: resp.message,
          },
        },
      }));

    expect(generator.next().done)
      .toBeTruthy();

    expect(logMessages.length).toBe(0);
  });
});
