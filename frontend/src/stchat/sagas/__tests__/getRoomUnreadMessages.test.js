import { call, put, select } from 'redux-saga/effects';
import * as chatAPI from '../../api/ChatAPI';
import getRoomUnreadMessages from '../getRoomUnreadMessages';
import actions from '../../actions';
import { getMessages, getRooms } from '../../selectors';

describe('getRoomUnreadMessages', () => {
  let logMessages;
  let action;
  let generator;

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action = {
      type: actions.GET_ROOM_UNREAD_MESSAGES,
      payload: {
        roomId: 'RoomID',
        msgId: 230,
        count: 10,
      },
    };
    generator = getRoomUnreadMessages(action);
  });

  test('Should call log when roomId isn`t valid', () => {
    // arrange
    action.payload.roomId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(new Error('getRoomUnreadMessages saga: invalid action argument "roomId"'));
  });

  test('Should call log when msgId isn`t valid', () => {
    // arrange
    action.payload.msgId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(new Error('getRoomUnreadMessages saga: invalid action argument "msgId"'));
  });

  test('Should call log when count isn`t valid', () => {
    // arrange
    action.payload.count = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(new Error('getRoomUnreadMessages saga: invalid action argument "count"'));
  });


  test('Should return, when messages > 0 and lastMessage = lastMessageId', () => {
    const msgStore = {
      roomId: {
        messages: [{
          id: 123,
        }],
      },
    };

    const roomsStore = {
      roomId: {
        latestMsg: 123,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(msgStore).value).toEqual(select(getRooms));
    expect(generator.next(roomsStore).done).toBeTruthy();
  });

  test('Should return, when messages = 0 and is lastMessageId = 0 ', () => {
    // arrange
    const msgStore = {
      roomId: {
        messages: [],
      },
    };

    const roomsStore = {
      roomId: {
        latestMsg: null,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(msgStore).value).toEqual(select(getRooms));
    expect(generator.next(roomsStore).done).toBeTruthy();
  });

  test('Should call chatAPI.getRoomUnreadModMessages when room is moderable', () => {
    // arrange
    const apiArgs = {
      roomId: 'RoomID',
      msgId: 230,
      count: 10,
    };

    const msgStore = {
      RoomID: {
        messages: [{
          id: 123,
        }],
      },
    };

    const roomsStore = {
      RoomID: {
        latestMsg: 234,
        moderable: true,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(msgStore).value).toEqual(select(getRooms));
    expect(generator.next(roomsStore).value)
      .toEqual(call(chatAPI.getRoomUnreadModMessages, apiArgs.roomId, apiArgs.msgId, apiArgs.count));
  });

  test('Should call chatAPI.getRoomUnreadMessages when room is not moderable', () => {
    // arrange
    const apiArgs = {
      roomId: 'RoomID',
      msgId: 230,
      count: 10,
    };

    const msgStore = {
      RoomID: {
        messages: [{
          id: 123,
        }],
      },
    };

    const roomsStore = {
      RoomID: {
        latestMsg: 234,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(msgStore).value).toEqual(select(getRooms));
    expect(generator.next(roomsStore).value)
      .toEqual(call(chatAPI.getRoomUnreadMessages, apiArgs.roomId, apiArgs.msgId, apiArgs.count));
  });

  test('Should call console.error, when call method return reject ', () => {
    // arrange
    const testError = new Error('some error description');
    const apiArgs = {
      roomId: 'RoomID',
      msgId: 230,
      count: 10,
    };

    const msgStore = {
      RoomID: {
        messages: [{
          id: 123,
        }],
      },
    };

    const roomsStore = {
      RoomID: {
        latestMsg: 234,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(msgStore).value).toEqual(select(getRooms));
    expect(generator.next(roomsStore).value)
      .toEqual(call(chatAPI.getRoomUnreadMessages, apiArgs.roomId, apiArgs.msgId, apiArgs.count));
    expect(generator.throw(testError).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(testError);
  });

  test('Should call console.error, when chatApi.getRoomUnreadMessages returns invalid roomId of response data', () => {
    // arrange
    const apiResponseData = {
      roomId: '',
      messages: [],
    };

    const apiArgs = {
      roomId: 'RoomID',
      msgId: 230,
      count: 10,
    };

    const msgStore = {
      RoomID: {
        messages: [{
          id: 123,
        }],
      },
    };

    const roomsStore = {
      RoomID: {
        latestMsg: 234,
      },
    };

    // assert
    generator.next();
    generator.next(msgStore);
    expect(generator.next(roomsStore).value)
      .toEqual(call(chatAPI.getRoomUnreadMessages, apiArgs.roomId, apiArgs.msgId, apiArgs.count));

    expect(generator.next(apiResponseData).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(new Error('getRoomUnreadMessages saga: invalid response argument "roomId"'));
  });

  test('Should call console.error, when chatApi.getRoomUnreadMessages returns invalid messages of response data',
    () => {
      // arrange
      const apiResponseData = {
        roomId: 'RoomID',
        messages: null,
      };

      const apiArgs = {
        roomId: 'RoomID',
        msgId: 230,
        count: 10,
      };

      const msgStore = {
        RoomID: {
          messages: [{
            id: 123,
          }],
        },
      };

      const roomsStore = {
        RoomID: {
          latestMsg: 234,
        },
      };

      // assert
      generator.next();
      generator.next(msgStore);
      expect(generator.next(roomsStore).value)
        .toEqual(call(chatAPI.getRoomUnreadMessages, apiArgs.roomId, apiArgs.msgId, apiArgs.count));
      expect(generator.next(apiResponseData).value).toEqual(put({
        type: actions.UNREAD_END_REACHED_STORE,
        payload: {
          roomId: apiArgs.roomId,
        },
      }));
      expect(generator.next().done).toBeTruthy();
      expect(logMessages.length).toBe(0);
    });

  test('Should put actions.GET_ROOM_UNREAD_MESSAGES_STORE, when chatApi.getRoomUnreadMessages returns count of '
    + 'messages is equal count request', () => {
    // arrange
    const apiResponseData = {
      roomId: 'RoomID',
      messages: Array(10),
    };
    const wantPutGetRoomUnreadMessagesStore = {
      type: actions.GET_ROOM_UNREAD_MESSAGES_STORE,
      payload: {
        messages: Array(10),
        roomId: 'RoomID',
      },
    };

    const apiArgs = {
      roomId: 'RoomID',
      msgId: 230,
      count: 10,
    };

    const msgStore = {
      RoomID: {
        messages: [{
          id: 123,
        }],
      },
    };

    const roomsStore = {
      RoomID: {
        latestMsg: 234,
      },
    };

    // assert
    generator.next();
    generator.next(msgStore);
    expect(generator.next(roomsStore).value)
      .toEqual(call(chatAPI.getRoomUnreadMessages, apiArgs.roomId, apiArgs.msgId, apiArgs.count));

    expect(generator.next(apiResponseData).value).toEqual(put(wantPutGetRoomUnreadMessagesStore));
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });
});
