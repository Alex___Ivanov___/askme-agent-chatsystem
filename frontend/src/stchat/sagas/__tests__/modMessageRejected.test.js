import { put, select } from 'redux-saga/effects';
import modMessageRejected from '../modMessageRejected';
import { getMessages } from '../../selectors';
import actions from '../../actions';

describe('modMessageRejected', () => {
  let logMessages;
  const action = {};
  let generator;

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      msgId: 1,
      roomId: 'roomID',
    };

    generator = modMessageRejected(action);
  });

  test('Should call log when payload has empty room id value', () => {
    // arrange
    action.payload.roomId = '';
    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0])
      .toEqual('modMessageRejected saga: invalid roomId parameter');
  });

  test('Should call log when payload has invalid msgId value', () => {
    // arrange
    action.payload.msgId = -1;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0])
      .toEqual('modMessageRejected saga: invalid msgId parameter');
  });

  test('Should call yield select(getMessages) and return '
    + 'when store does not consist required room', () => {
    // arrange
    const messagesState = {};

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesState).done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });

  test('Should call yield select(getMessages) and return '
    + 'when room does not consist messages field', () => {
    // arrange
    const messagesState = {
      roomID: {},
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesState).done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });

  test('Should return when messages state doesn`t contain required message', () => {
    // arrange
    const messagesState = {
      roomID: {
        messages: [
          {
            id: 2,
          },
          {
            id: 3,
          },
        ],
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesState).done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });

  test('Should call yield put({type:"actions.SET_MESSAGES_STORE"})', () => {
    // arrange
    const messagesState = {
      roomID: {
        messages: [
          {
            id: 1,
          },
          {
            id: 2,
          },
        ],
      },
    };

    const wantMessagesState = {
      roomID: {
        messages: [
          {
            id: 1,
            isModerated: 0,
          },
          {
            id: 2,
          },
        ],
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesState).value).toEqual(put({
      type: actions.SET_MESSAGES_STORE,
      payload: wantMessagesState,
    }));
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });
});
