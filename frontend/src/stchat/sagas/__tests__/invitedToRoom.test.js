import { put, select } from 'redux-saga/effects';
import invitedToRoom from '../invitedToRoom';
import { getUsername } from '../../selectors';
import actions from '../../actions';

describe('invitedToRoom', () => {
  let logMessages;
  let generator;
  const room = {
    id: 'uuidRoom_1',
    name: 'room_name',
    members: ['user_1', 'user_2'],
    creator: 'user_1',
    type: 'public',
    unreadMsgCount: 2,
    oldestMsgId: 4,
    latestMsg: null,
    readOnly: false,
    moderable: false,
  };
  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = { ...room };
    generator = invitedToRoom(action);
  });

  test('Should call log when payload has id field which is not valid', () => {
    // arrange
    action.payload.id = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('invitedToRoom saga: payload has "id" field which is not valid');
  });

  test('Should call log when payload has name field which is not valid', () => {
    // arrange
    action.payload.name = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('invitedToRoom saga: payload has "name" field which is not valid');
  });

  test('Should call log when payload has members field which is not valid', () => {
    // arrange
    action.payload.members = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('invitedToRoom saga: payload has "members" field which is not valid');
  });

  test('Should call log when payload has creator field which is not valid', () => {
    // arrange
    action.payload.creator = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('invitedToRoom saga: payload has "creator" field which is not valid');
  });

  test('Should call log when payload has unreadMsgCount field which is not valid', () => {
    // arrange
    action.payload.unreadMsgCount = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('invitedToRoom saga: payload has "unreadMsgCount" field which is not valid');
  });

  test('Should call log when payload has oldestMsgId field which is not valid', () => {
    // arrange
    action.payload.oldestMsgId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('invitedToRoom saga: payload has "oldestMsgId" field which is not valid');
  });

  test('Should call log when payload has readOnly field which is not valid', () => {
    // arrange
    action.payload.readOnly = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('invitedToRoom saga: payload has "readOnly" field which is not valid');
  });

  test('Should call log when payload has moderable field which is not valid', () => {
    // arrange
    action.payload.moderable = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message)
      .toEqual('invitedToRoom saga: payload has "moderable" field which is not valid');
  });

  test('Should put INVITED_TO_ROOM_STORE action with own room name, when room is public', () => {
    // arrange
    const username = 'user_1';
    const wantAction = {
      type: actions.INVITED_TO_ROOM_STORE,
      payload: { ...room },
    };
    // assert
    expect(generator.next().value).toEqual(select(getUsername));
    expect(generator.next(username).value).toEqual(put(wantAction));
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });

  test('Should put INVITED_TO_ROOM_STORE action with participant room name, when room is private', () => {
    // arrange
    action.payload.type = 'private';
    const username = 'user_1';
    const wantAction = {
      type: actions.INVITED_TO_ROOM_STORE,
      payload: {
        ...room,
        name: 'user_2',
        type: 'private',
      },
    };
    // assert
    expect(generator.next().value).toEqual(select(getUsername));
    expect(generator.next(username).value).toEqual(put(wantAction));
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });
});
