import { call, put, select } from 'redux-saga/effects';
import getHistoryMessages from '../getHistoryMessages';
import { getMessages, getRooms } from '../../selectors';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';


describe('getHistoryMessages', () => {
  const msgId = 5;
  const roomId = 'roomId';
  const count = 10;

  const action = {
    payload: null,
  };
  let logMessages = null;
  let roomsStore = null;
  let messagesStore = null;
  let generator = null;

  beforeEach(() => {
    logMessages = [];
    console.log = (...args) => {
      logMessages.push(...args);
    };
    action.payload = {
      msgId,
      roomId,
      count,
    };
    roomsStore = {
      roomId: {
        moderable: false,
      },
    };
    messagesStore = {
      roomId: {
        historyEndReached: false,
      },
    };
    generator = getHistoryMessages(action);
  });

  test('Should call log when roomId isn`t string', () => {
    // arrange
    action.payload.roomId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  test('Should call log when members has length is zero', () => {
    // arrange
    action.payload.roomId = '';

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  test('Should call log when fromTime ins`t string', () => {
    // arrange
    action.payload.msgId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid msgId param');
  });

  test('Should call log when fromTime is empty', () => {
    // arrange
    action.payload.msgId = 0;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid msgId param');
  });

  test('Should call log when count isn`t number', () => {
    // arrange
    action.payload.count = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid count param');
  });

  test('Should call log when count is equal or less than zero', () => {
    // arrange
    action.payload.count = 0;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid count param');
  });

  test('Should stop working when the end of the history was reached', () => {
    // arrange
    messagesStore.roomId.historyEndReached = true;

    // assert
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesStore).done).toBeTruthy();
  });

  test('Should call chatAPI.getRoomModMessages when room is moderable', () => {
    // arrange
    roomsStore.roomId.moderable = true;

    // assert
    generator.next();
    expect(generator.next(messagesStore).value).toEqual(select(getRooms));
    expect(generator.next(roomsStore).value).toEqual(call(chatAPI.getRoomModMessages, roomId, msgId, count));
  });

  test('Should call chatAPI.getRoomModMessages when room is not moderable', () => {
    // assert
    generator.next();
    expect(generator.next(messagesStore).value).toEqual(select(getRooms));
    expect(generator.next(roomsStore).value).toEqual(call(chatAPI.getRoomMessages, roomId, msgId, count));
  });


  test('Should call log when chatAPI.getRoomMessages throws error', () => {
    // arrange
    const wantError = new Error('something wrong');

    // assert
    generator.next();
    generator.next(messagesStore);
    expect(generator.next(roomsStore).value).toEqual(call(chatAPI.getRoomMessages, roomId, msgId, count));
    expect(generator.throw(wantError).done).toBeTruthy();
  });


  test('Should call log when roomId field of response is invalid', () => {
    // arrange
    const invalidResponse = {
      roomId: null,
    };

    // act
    generator.next();
    generator.next(messagesStore);

    // assert
    expect(generator.next(roomsStore).value).toEqual(call(chatAPI.getRoomMessages, roomId, msgId, count));
    expect(generator.next(invalidResponse).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual('invalid roomId field in response');
  });

  test('Should put HISTORY_END_REACHED_STORE action to store when the number of messages received is null or undefined',
    () => {
      // arrange
      const validResponse = {
        ...action.payload,
        roomId,
        messages: null,
      };
      const wantAction = {
        type: actions.HISTORY_END_REACHED_STORE,
        payload: {
          roomId,
        },
      };

      // act
      generator.next();
      generator.next(messagesStore);
      generator.next(roomsStore);

      // assert
      expect(generator.next(validResponse).value).toEqual(put(wantAction));
      expect(generator.next().done).toBeTruthy();
    });


  test('Should put GET_MESSAGES_STORE action to store when count and the number of received messages are the same',
    () => {
      // arrange
      const validResponse = {
        ...action.payload,
        roomId,
        messages: Array(10),
      };
      const wantAction = {
        type: actions.GET_MESSAGES_STORE,
        payload: {
          roomId,
          messages: validResponse.messages,
        },
      };

      // act
      generator.next();
      generator.next(messagesStore);
      generator.next(roomsStore);

      // assert
      expect(generator.next(validResponse).value).toEqual(put(wantAction));
      expect(generator.next().done).toBeTruthy();
    });

  test('Should put HISTORY_END_REACHED_STORE and GET_MESSAGES_STORE actions to store when count is less than'
    + 'the number of received messages, but ins`t zero', () => {
    // arrange
    const validResponse = {
      ...action.payload,
      roomId,
      messages: Array(5),
    };
    const wantHistoryEndReachedAction = {
      type: actions.HISTORY_END_REACHED_STORE,
      payload: {
        roomId,
      },
    };
    const wantGetMessagesAction = {
      type: actions.GET_MESSAGES_STORE,
      payload: {
        roomId,
        messages: validResponse.messages,
      },
    };

    // act
    generator.next();
    generator.next(messagesStore);
    generator.next(roomsStore);

    // assert
    expect(generator.next(validResponse).value).toEqual(put(wantHistoryEndReachedAction));
    expect(generator.next().value).toEqual(put(wantGetMessagesAction));
    expect(generator.next().done).toBeTruthy();
  });
});
