import { call, put } from 'redux-saga/effects';
import deleteUsersFromRoom from '../deleteUsersFromRoom';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';


describe('deleteUsersFromRoom', () => {
  let logMessages = null;
  let generator;
  const users = ['user_1', 'user_2'];
  const roomId = 'room_id';
  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.log = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      users,
      roomId,
    };
    generator = deleteUsersFromRoom(action);
  });

  test('Should call log when users isn`t array', () => {
    // arrange
    action.payload.roomId = undefined;
    action.payload.users = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid users param');
  });

  test('Should call log when users has length is zero', () => {
    // arrange
    action.payload.users = [];

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid users param');
  });

  test('Should call log when roomId ins`t string', () => {
    // arrange
    action.payload.roomId = [];

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  test('Should call log when roomId is empty', () => {
    // arrange
    action.payload.roomId = '';

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  test('Should call log when chatAPI.deleteUsersFromRoom returns Promise.reject', () => {
    // assert
    expect(generator.next().value).toEqual(call(chatAPI.deleteUsersFromRoom, users, roomId));
    expect(generator.throw(new Error('some error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('some error');
  });

  test('Should call log when chatAPI.deleteUsersFromRoom returns Promise.resolve and puts to store', () => {
    // arrange
    const wantAction = {
      type: actions.DELETE_USERS_FROM_ROOM_STORE,
      payload: {
        users,
        roomId,
      },
    };

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.deleteUsersFromRoom, users, roomId));
    expect(generator.next().value).toEqual(put(wantAction));
  });
});
