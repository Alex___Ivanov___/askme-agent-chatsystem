import { put, select } from 'redux-saga/effects';
import usersDeletedFromRoom from '../usersDeletedFromRoom';
import { getActiveRoomId, getUsername } from '../../selectors';
import actions from '../../actions';

describe('usersDeletedFromRoom', () => {
  let generator;
  let logMessages;
  const roomId = 'room_id';
  const users = ['user1', 'user2'];
  const action = {};

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      users,
      roomId,
    };
    generator = usersDeletedFromRoom(action);
  });

  test('Should call console.error when users field isn`t array', () => {
    // arrange
    action.payload.users = null;

    // assert
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0].message)
      .toEqual('invalid users param');
  });

  test('Should call console.error when users has length is zero', () => {
    // arrange
    action.payload.users = [];

    // assert
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0].message)
      .toEqual('invalid users param');
  });

  test('Should call console.error when roomId ins`t string', () => {
    // arrange
    action.payload.roomId = [];

    // assert
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0].message)
      .toEqual('invalid roomId param');
  });

  test('Should call console.error when roomId is empty', () => {
    // arrange
    action.payload.roomId = '';

    // assert
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0].message)
      .toEqual('invalid roomId param');
  });

  test('Should call put({type: actions.DELETE_USERS_FROM_ROOM_STORE...}', () => {
    // assert
    expect(generator.next().value).toEqual(select(getUsername));
    expect(generator.next('user0').value).toEqual(select(getActiveRoomId));
    expect(generator.next('room_id').value).toEqual(put({
      type: actions.DELETE_USERS_FROM_ROOM_STORE,
      payload: {
        users,
        roomId,
      },
    }));
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(0);
  });

  test('Should call put({type: actions.DELETE_ROOM_STORE...}', () => {
    // assert
    expect(generator.next().value).toEqual(select(getUsername));
    expect(generator.next('user1').value).toEqual(select(getActiveRoomId));
    expect(generator.next('room_id_2').value).toEqual(put({
      type: actions.DELETE_ROOM_STORE,
      payload: {
        roomId,
      },
    }));
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(0);
  });

  test('Should call put({type: actions.DELETE_ROOM_STORE...}) '
    + 'and call put({type: actions.SET_ACTIVE_ROOM...})', () => {
    // assert
    expect(generator.next().value).toEqual(select(getUsername));
    expect(generator.next('user1').value).toEqual(select(getActiveRoomId));
    expect(generator.next('room_id').value).toEqual(put({
      type: actions.SET_ACTIVE_ROOM,
      payload: '',
    }));
    expect(generator.next().value).toEqual(put({
      type: actions.DELETE_ROOM_STORE,
      payload: {
        roomId,
      },
    }));
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(0);
  });
});
