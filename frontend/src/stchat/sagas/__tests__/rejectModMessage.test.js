import { call, select, put } from 'redux-saga/effects';
import rejectModMessage from '../rejectModMessage';
import * as chatAPI from '../../api/ChatAPI';
import { getMessages } from '../../selectors';
import actions from '../../actions';

describe('rejectModMessage', () => {
  let logMessages;
  const action = {};
  let generator;
  const id = 1;
  const roomId = 'roomId';

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      id,
      roomId,
    };

    generator = rejectModMessage(action);
  });

  it('Should call log when payload has invalid id value', () => {
    // arrange
    action.payload.id = -1;

    // assert
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0])
      .toEqual('rejectModMessage saga: invalid id parameter');
  });

  it('Should call log when payload has invalid roomId value', () => {
    // arrange
    action.payload.roomId = '';

    // assert
    expect(generator.next().done)
      .toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0])
      .toEqual('rejectModMessage saga: invalid roomId parameter');
  });

  it('Should call log when chatAPI.rejectModMessage throw exception', () => { // assert
    expect(generator.next().value).toEqual(call(chatAPI.rejectModMessage, id, roomId));
    expect(generator.throw(new Error('some error')).done).toBeTruthy();
    expect(logMessages.length)
      .toBe(1);
    expect(logMessages[0])
      .toEqual(new Error('some error'));
  });

  it('Should call select(getMessages)', () => {
    // arrange
    const messagesState = {};

    // assert
    expect(generator.next().value).toEqual(call(chatAPI.rejectModMessage, id, roomId));
    expect(generator.next().value).toEqual(select(getMessages));
    expect(generator.next(messagesState).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0])
      .toEqual('rejectModMessage saga: roomMessages isn`t existed');
  });

  it('Should call log when messages state doesn`t contain required message',
    () => {
      // arrange
      const messagesState = {
        roomId: {
          messages: [
            {
              id: 2,
            },
            {
              id: 0,
            },
          ],
        },
      };

      // assert
      expect(generator.next().value).toEqual(call(chatAPI.rejectModMessage, id, roomId));
      expect(generator.next().value).toEqual(select(getMessages));
      expect(generator.next(messagesState).done).toBeTruthy();
      expect(logMessages.length).toBe(1);
      expect(logMessages[0])
        .toEqual('rejectModMessage saga: required message isn`t existed');
    });

  it('Should call put({type: actions.SET_MESSAGES_STORE...})',
    () => {
    // arrange
      const messagesState = {
        roomId: {
          messages: [
            {
              id: 1,
            },
            {
              id: 0,
            },
          ],
        },
      };

      // assert
      expect(generator.next().value).toEqual(call(chatAPI.rejectModMessage, id, roomId));
      expect(generator.next().value).toEqual(select(getMessages));
      expect(generator.next(messagesState).value).toEqual(put(
        {
          type: actions.SET_MESSAGES_STORE,
          payload: {
            roomId: {
              messages: [
                {
                  id: 1,
                  isModerated: 0,
                },
                {
                  id: 0,
                },
              ],
            },
          },
        },
      ));
      expect(generator.next().done).toBeTruthy();
      expect(logMessages.length).toBe(0);
    });
});
