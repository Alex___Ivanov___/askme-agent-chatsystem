import { call, put, select } from 'redux-saga/effects';
import updateRoomSettingsSaga from '../updateRoomSettings';
import { getRooms } from '../../selectors';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';


describe('updateRoomSettingsSaga', () => {
  let logMessages;
  let generator;
  const id = 'room_id';
  const name = 'room_name';
  const readOnly = false;
  const moderable = false;
  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.error = (...args) => {
      logMessages.push(...args);
    };

    action.payload = {
      id,
      name,
      readOnly,
      moderable,
    };
    generator = updateRoomSettingsSaga(action);
  });

  test('Should call log when payload has invalid room id', () => {
    // arrange
    action.payload.id = undefined;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(new Error('invalid id param'));
  });

  test('Should call log when payload has invalid field of room name', () => {
    // arrange
    action.payload.name = undefined;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(new Error('invalid roomName param'));
  });

  test('Should call log when payload has invalid field of readOnly', () => {
    // arrange
    action.payload.readOnly = undefined;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(new Error('invalid readOnly param'));
  });

  test('Should call log when payload has invalid field of moderable', () => {
    // arrange
    action.payload.moderable = undefined;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(new Error('invalid moderable param'));
  });

  test('Should call log when select with roomsSelector method throws error', () => {
    // arrange
    const simpleError = new Error('some error text');

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.throw(simpleError).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(simpleError);
  });


  test('Should return when name, readOnly, moderable fields are equal fields of payload', () => {
    // arrange
    const selectedRooms = {
      room_id: {
        name: 'room_name',
        readOnly: false,
        moderable: false,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(selectedRooms).done).toBeTruthy();
  });

  test('Should return when chatAPI.updateRoomSettings throw exception', () => {
    // arrange
    const wantError = new Error('updateRoomSettings error');
    const selectedRooms = {
      room_id: {
        name: 'room_name',
        readOnly: false,
        moderable: true,
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(selectedRooms).value).toEqual(call(chatAPI.updateRoomSettings, action.payload));
    expect(generator.throw(new Error('updateRoomSettings error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(wantError);
  });

  test('Should put UPDATE_ROOM_SETTINGS_STORE action when all are good', () => {
    // arrange
    const selectedRooms = {
      room_id: {
        name: 'room_name',
        readOnly: false,
        moderable: true,
      },
    };
    const wantSendMessageAction = {
      type: actions.UPDATE_ROOM_SETTINGS_STORE,
      payload: {
        id,
        room: {
          name,
          readOnly,
          moderable,
        },
      },
    };

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(selectedRooms).value).toEqual(call(chatAPI.updateRoomSettings, action.payload));
    expect(generator.next().value).toEqual(put(wantSendMessageAction));
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(0);
  });
});
