import { call, put, select } from 'redux-saga/effects';
import sendMessageSaga, { splitMessage } from '../sendMessage';
import { getRooms, getUserInfo } from '../../selectors';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';

describe('splitMessage', () => {
  test('Should return one message in messages', () => {
    // arrange
    const inputMessage = 'In a professional context.';

    // act
    const gotMessages = splitMessage(inputMessage, 40);

    // assert
    expect(gotMessages.length).toBe(1);
    expect(inputMessage).toEqual(gotMessages[0]);
  });

  test('Should return two message in messages when input string gt for 1 of maxLen', () => {
    // arrange
    // eslint-disable-next-line max-len
    const inputMessage = 'ZWfZh5zOJJ6SjWqMEatxFa9emtvMegGvUnVrPyO1AmCRHLzJimUWhLhgQuOT5O0dDlrYJrTlVO1GZs7aoi7k7pSMe5K';
    const wantMessages = [
      'ZWfZh5zOJJ6SjWqMEatxFa9emtvMegGvUnVrPyO1AmCRHLzJimUWhLhgQuOT5O0dDlrYJrTlVO1GZs7aoi7k7pSMe5',
      'K',
    ];
    // act
    const gotMessages = splitMessage(inputMessage, 90);

    // assert
    expect(gotMessages.length).toEqual(2);
    expect(gotMessages).toEqual(wantMessages);
  });

  test('Should return 2 message in messages with trailing space', () => {
    // arrange
    const inputMessage = 'In a professional context.adadadadadada In a professional context.adadadadadada ';

    // act
    const gotMessages = splitMessage(inputMessage, 40);

    // assert
    expect(gotMessages.length).toBe(2);
    gotMessages.forEach((v) => expect(v[v.length - 1]).toBe(' '));
  });

  test('Should return 3 message in messages', () => {
    // arrange
    const inputMessage = 'In a professional context.adadadadada data binder In a professional context.adadadadadada ';
    const wantMessages = [
      'In a professional context.adadadadada',
      'data binder In a professional',
      'context.adadadadadada ',
    ];
    // act
    const gotMessages = splitMessage(inputMessage, 40);

    // assert
    expect(gotMessages).toEqual(wantMessages);
  });
});

describe('sendMessageSaga', () => {
  let generator;
  let logMessages;
  const msgText = 'message text';
  const roomId = 'room_id';
  const action = {
    payload: null,
  };

  const sendMessageResponse = {
    sender: 'user_1',
    roomId: 'room_id_1',
    text: 'message_text',
    id: 1,
    time: 'message_time',
    isModerated: true,
  };

  console.log = (...args) => {
    logMessages.push([...args].join(' '));
  };

  let roomsState = null;

  const userInfoState = {
    username: 'user_1',
  };

  beforeEach(() => {
    logMessages = [];

    action.payload = {
      roomId,
      msgText,
    };

    roomsState = {
      room_id: {
        moderable: false,
        creator: 'user_1',
      },
    };
    generator = sendMessageSaga(action);
  });

  test('Should call log when payload has invalid room id', () => {
    // arrange
    action.payload.roomId = undefined;

    // act
    const genVal = generator.next().done;

    // assert
    expect(genVal).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual('invalid roomId param');
  });

  test('Should call log when payload has invalid msgText field', () => {
    // arrange
    action.payload.msgText = undefined;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual('invalid text param');
  });

  test('Should call sendModMessage when room is moderable and username!==creator', () => {
    // arrange
    const sendMsgArgs = {
      roomId,
      text: msgText,
    };

    roomsState.room_id.moderable = true;
    roomsState.room_id.creator = 'user_2';

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(roomsState).value).toEqual(select(getUserInfo));
    expect(generator.next(userInfoState).value).toEqual(call(chatAPI.sendModMessage, sendMsgArgs));
  });

  test('Should call sendMessage when room is moderable and username===creator', () => {
    // arrange
    const sendMsgArgs = {
      roomId,
      text: msgText,
    };

    roomsState.room_id.moderable = true;

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(roomsState).value).toEqual(select(getUserInfo));
    expect(generator.next(userInfoState).value).toEqual(call(chatAPI.sendMessage, sendMsgArgs));
  });

  test('Should call sendMessage when room is not moderable and username===creator', () => {
    // arrange
    const sendMsgArgs = {
      roomId,
      text: msgText,
    };

    roomsState.room_id.moderable = false;

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(roomsState).value).toEqual(select(getUserInfo));
    expect(generator.next(userInfoState).value).toEqual(call(chatAPI.sendMessage, sendMsgArgs));
  });

  test('Should call sendMessage when room is not moderable and username!==creator', () => {
    // arrange
    const sendMsgArgs = {
      roomId,
      text: msgText,
    };

    roomsState.room_id.moderable = false;
    roomsState.room_id.creator = 'user2';

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(roomsState).value).toEqual(select(getUserInfo));
    expect(generator.next(userInfoState).value).toEqual(call(chatAPI.sendMessage, sendMsgArgs));
  });

  test('Should call log when chatAPI.sendMessage throw exception', () => {
    // arrange
    const sendMsgArgs = {
      roomId,
      text: msgText,
    };
    const wantError = new Error('sendMessage error');

    // assert
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(roomsState).value).toEqual(select(getUserInfo));
    expect(generator.next(userInfoState).value).toEqual(call(chatAPI.sendMessage, sendMsgArgs));
    expect(generator.throw(new Error('sendMessage error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0]).toEqual(wantError.message);
  });

  test('Should call console log when roomId in query and roomId in response are not the same',
    () => {
      // assert
      generator.next();
      generator.next(roomsState);
      generator.next(userInfoState);
      expect(generator.next(sendMessageResponse).done).toBeTruthy();
      expect(logMessages.length).toBe(1);
      expect(logMessages[0]).toEqual('invalid roomId field in response');
    });

  test('Should put SEND_MESSAGE_STORE action', () => {
    // arrange
    sendMessageResponse.roomId = 'room_id';
    const wantSendMessageAction = {
      type: actions.SEND_MESSAGE_STORE,
      payload: {
        id: 1,
        sender: 'user_1',
        roomId: 'room_id',
        text: 'message_text',
        time: 'message_time',
        isModerated: true,
      },
    };

    // assert
    generator.next();
    generator.next(roomsState);
    generator.next(userInfoState);
    expect(generator.next(sendMessageResponse).value).toEqual(put(wantSendMessageAction));
    expect(logMessages.length).toBe(0);
  });

  test('Should put CHAT_UPDATED action, after SEND_MESSAGE_STORE action', () => {
    // arrange
    sendMessageResponse.roomId = 'room_id';
    const wantChatUpdatedAction = {
      type: actions.CHAT_UPDATED,
      payload: {
        client: 'user_1',
        roomId: 'room_id',
      },
    };

    // assert
    generator.next();
    generator.next(roomsState);
    generator.next(userInfoState);
    generator.next(sendMessageResponse);
    expect(generator.next().value).toEqual(put(wantChatUpdatedAction));
    expect(logMessages.length).toBe(0);
  });

  test('Should select room from store, after CHAT_UPDATED action', () => {
    // arrange
    const messageResponse = {
      id: 2,
      text: 'V',
      time: '11:11:11',
      sender: 'V',
      roomId: 'room_id',
      isModerated: true,
    };

    const selectedRooms = {
      room_id: {
        name: 'My team',
        members: ['V', 'A', 'S', 'A'],
        creator: 'v',
        type: 'group',
        readOnly: false,
        latestMsg: { time: '10:10:10', id: 1, sender: 'V' },
        oldestMsgId: null,
        unreadMsgCount: 0,
      },
    };
    const wantChatUpdatedAction = {
      type: actions.UPDATE_ROOM,
      payload: {
        name: 'My team',
        members: ['V', 'A', 'S', 'A'],
        creator: 'v',
        type: 'group',
        readOnly: false,
        latestMsg: {
          time: '11:11:11', id: 2, sender: 'V', text: 'V', isModerated: true,
        },
        oldestMsgId: null,
        unreadMsgCount: 0,
        id: 'room_id',
      },
    };

    // assert
    generator.next();
    generator.next(roomsState);
    generator.next(userInfoState);
    generator.next(messageResponse);
    generator.next();
    expect(generator.next().value).toEqual(select(getRooms));
    expect(generator.next(selectedRooms).value).toEqual(put(wantChatUpdatedAction));
    expect(logMessages.length).toBe(0);
  });
});
