import { call, put } from 'redux-saga/effects';
import clearHistory from '../clearHistory';
import * as chatAPI from '../../api/ChatAPI';
import actions from '../../actions';

describe('clearHistory', () => {
  let logMessages;
  let generator;
  const roomId = 'room_id';
  const action = {
    payload: null,
  };

  beforeEach(() => {
    logMessages = [];
    console.log = (...args) => {
      logMessages.push(...args);
    };
    action.payload = {
      roomId,
    };
    generator = clearHistory(action);
  });

  it('Should call log when roomId isn`t valid', () => {
    // arrange
    action.payload.roomId = null;

    // assert
    expect(generator.next().done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('invalid roomId param');
  });

  it('Should call log when call returns error', () => {
    // assert
    expect(generator.next().value).toEqual(call(chatAPI.clearHistory, 'room_id'));
    expect(generator.throw(new Error('some error')).done).toBeTruthy();
    expect(logMessages.length).toBe(1);
    expect(logMessages[0].message).toEqual('some error');
  });

  it('Should call put actions.UPDATE_ROOM_SETTINGS_STORE and actions.CLEAR_HISTORY_STORE', () => {
    // assert
    expect(generator.next().value).toEqual(call(chatAPI.clearHistory, 'room_id'));
    expect(generator.next().value).toEqual(put({
      type: actions.UPDATE_ROOM_SETTINGS_STORE,
      payload: {
        id: 'room_id',
        room: {
          latestMsg: null,
          oldestMsgId: 0,
          unreadMsgCount: 0,
        },
      },
    }));
    expect(generator.next().value).toEqual(put({
      type: actions.CLEAR_HISTORY_STORE,
      payload: {
        roomId: 'room_id',
      },
    }));
    expect(logMessages.length).toBe(0);
  });
});
