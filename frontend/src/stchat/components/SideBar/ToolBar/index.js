import {
  MdFilterList,
} from 'react-icons/md';
import React, { useState } from 'react';
import './ToolBar.css';
import PropTypes from 'prop-types';
import FilterSettings from './FilterSettings';

const ToolBar = (props) => {
  const {
    setSearchString,
    searchString,
  } = props;

  const [filer, setFilter] = useState(false);

  return (
    <>
      <div className="sidebar__header">
        <div className="sidebar__search">
          <input
            className="sidebar__search-input"
            value={searchString}
            onChange={(e) => {
              setSearchString(e.target.value);
            }}
            placeholder="Search by name"
          />
        </div>
        <div
          className="filter"
          title="Filter"
          onClick={() => { setFilter(!filer); }}
        >
          <MdFilterList />
        </div>
        <div className="toolbar" />
      </div>
      {filer && <FilterSettings />}
    </>
  );
};

ToolBar.propTypes = {
  setSearchString: PropTypes.func.isRequired,
  searchString: PropTypes.string.isRequired,
};

export default ToolBar;
