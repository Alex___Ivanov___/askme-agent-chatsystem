import React, { useState } from 'react';
import CheckBox from '../../../Common/CheckBox';
import './filterSettings.css';

const FilterSettings = () => {
  const [pending, setPending] = useState(true);
  const [progress, setProgress] = useState(true);
  const [closed, setClosed] = useState(false);
  return (
    <div className="room_status">
      <span>Statuses:</span>
      <CheckBox
        name="pending"
        checked={pending}
        onChange={(e) => {
          console.log(e);
          setPending(!pending);
        }}
      />
      <CheckBox
        name="in progress"
        checked={progress}
        onChange={(e) => {
          console.log(e);
          setProgress(!progress);
        }}
      />
      <CheckBox
        name="closed"
        checked={closed}
        onChange={(e) => {
          console.log(e);
          setClosed(!closed);
        }}
      />
    </div>
  );
};

export default FilterSettings;
