import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ToolBar from './ToolBar';
import ErrorBoundary from '../Common/ErrorBoundary';
import UserRoomsList from './UserRoomsList';

const SideBar = (props) => {
  const {
    setRoomListVisibility,
  } = props;
  const [searchString, setSearchString] = useState('');

  return (
    <ErrorBoundary>
      <ToolBar
        setSearchString={setSearchString}
        searchString={searchString}
      />
      <UserRoomsList
        setRoomListVisibility={setRoomListVisibility}
        searchString={searchString}
        setSearchString={setSearchString}
      />
    </ErrorBoundary>
  );
};

SideBar.propTypes = {
  setRoomListVisibility: PropTypes.func.isRequired,
};

export default SideBar;
