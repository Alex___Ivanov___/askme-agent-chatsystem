import React from 'react';
import './checkbox.css';
import PropTypes from 'prop-types';

const CheckBox = ({ name, checked, onChange }) => (
  // eslint-disable-next-line jsx-a11y/label-has-associated-control
  <label className="container">
    <span>{name}</span>
    <input type="checkbox" checked={checked} onChange={onChange} />
    <span className="checkmark" />
  </label>
);

CheckBox.propTypes = {
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default CheckBox;
