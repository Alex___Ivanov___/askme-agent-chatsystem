import React from 'react';
import './MenuList.css';

const MenuList = ({ list }) => {
  const listItems = list.map((i) => (
    <ul className="menu__item" key={i.desc}><div onClick={i.cb}>{i.desc}</div></ul>
  ));
  return <li className="menu__list">{listItems}</li>;
};

export default MenuList;
