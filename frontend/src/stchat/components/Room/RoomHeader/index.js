import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  MdClose,
  MdArrowBack,
  MdSettings,
} from 'react-icons/md';
import PropTypes from 'prop-types';
import RoomSettings from '../../Settings';
import actions from '../../../actions';
import './RoomHeader.css';
import {
  unsetActiveRoom,
  setPrevActiveRoom,
} from '../../../actionCreators';
import { getActiveRoomId } from '../../../selectors';
import { sendCloseChat } from '../../../api/ParentAPI';

const RoomHeader = (props) => {
  const {
    roomName,
    setRoomListVisibility,
    botActivity,
  } = props;
  const dispatch = useDispatch();
  const activeRoomID = useSelector(getActiveRoomId);
  const [isVisibleSettings, setSettingsVisibility] = useState(false);
  const toggleRoomSettings = () => setSettingsVisibility((prevState) => !prevState);

  const handleCloseBtn = () => {
    dispatch(setPrevActiveRoom(activeRoomID));
    dispatch(unsetActiveRoom());
    sendCloseChat();
    setRoomListVisibility(false);
  };

  const dispatchSetActiveRoom = () => {
    dispatch({
      type: actions.SET_ACTIVE_ROOM,
      payload: '',
    });
  };

  const handleBackBtn = () => {
    dispatchSetActiveRoom();
    setRoomListVisibility(true);
  };

  return (
    <div className="room__header">
      {isVisibleSettings && (
        <RoomSettings
          closeCb={toggleRoomSettings}
          botActivity={botActivity}
        />
      )}

      <div className="room__back-btn" onClick={handleBackBtn}><MdArrowBack /></div>
      <div className="room__info" title={roomName}>
        <div className="room__info-body">
          <div className="room__name">{roomName}</div>
          <span className={`room__bot-activity ${botActivity ? 'working' : 'paused'}`}>
            {botActivity ? 'working' : 'paused'}
          </span>
        </div>
      </div>
      <div className="active_buttons">
        <div
          className="room__settings-btn"
          onClick={toggleRoomSettings}
        >
          <MdSettings title="settings" />
        </div>
        <div className="room__close-chat-btn" onClick={handleCloseBtn} title="close chat"><MdClose /></div>
      </div>
    </div>
  );
};

RoomHeader.propTypes = {
  roomName: PropTypes.string.isRequired,
  setRoomListVisibility: PropTypes.func.isRequired,
};

export default RoomHeader;
