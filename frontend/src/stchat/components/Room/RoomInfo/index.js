import React from 'react';
import PropTypes from 'prop-types';
import './RoomInfo.css';


const RoomInfo = (props) => {
  const {
    creator, members, username, showParticipantsCb,
  } = props;

  const participantsCount = members.length;
  const membersInfo = `Participants (${participantsCount})`;
  const participantCountClass = creator === username
    ? 'room__participant-count' : 'room__participant-count participant-hidden';

  return (
    <div className="room__info">
      <div className="room__creator-label">
        <span> Room creator: </span>
        <b>{creator}</b>
      </div>
      <div
        className={participantCountClass}
        onClick={() => { showParticipantsCb(); }}
      >
        {membersInfo}
      </div>
    </div>
  );
};

RoomInfo.propTypes = {
  members: PropTypes.instanceOf(Array).isRequired,
  creator: PropTypes.string.isRequired,
  showParticipantsCb: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
};

export default RoomInfo;
