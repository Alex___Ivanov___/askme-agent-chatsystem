import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { MdArrowBack } from 'react-icons/md';
import PropTypes from 'prop-types';
import actions from '../../actions';
import './Settings.css';
import Switch from '../Common/Switch';

const RoomSettings = (props) => {
  const {
    closeCb,
    botActivity,
  } = props;
  const { activeRoomID } = useSelector((state) => state.roomsReducer);
  const dispatch = useDispatch();

  const changeBotActivity = () => {
    dispatch({
      type: actions.CHANGE_BOT_ACTIVITY,
      payload: {
        roomId: activeRoomID,
      },
    });
  };

  const closeRoomAction = () => dispatch({
    type: actions.CLOSE_ROOM,
    payload: {
      roomId: activeRoomID,
    },
  });

  const showConfirmModalDeleteRoom = () => {
    dispatch({
      type: actions.OPEN_CONFIRM_MODAL,
      payload: {
        msg: 'Are you sure you want to close room?',
        resultCb: (result) => {
          if (result) closeRoomAction();
        },
      },
    });
  };

  return (
    <div className="room-settings">
      <div className="room-settings__header">
        <div onClick={closeCb} className="room-settings-header__btn">
          <MdArrowBack title="Back" />
        </div>
        <div className="room-settings_header-text" title="Create room">Room settings</div>
      </div>
      <div className="room-settings__body">
        <div className="room-settings__body__read_only">
          <div className="rsi-header">Bot activity</div>
          <Switch switchCb={changeBotActivity} startValue={botActivity} title="change bot activity" />
        </div>
      </div>
      <div className="room-settings__body__read_only">
        <div className="rsi-header">Close room</div>
        <div className="rs-body__btn rsi-header" onClick={showConfirmModalDeleteRoom}>Close</div>
      </div>
    </div>
  );
};

RoomSettings.propTypes = {
  closeCb: PropTypes.func.isRequired,
  botActivity: PropTypes.bool.isRequired,
};

export default RoomSettings;
