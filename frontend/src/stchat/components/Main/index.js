import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import './Main.css';
import Room from '../Room';
import * as chatAPI from '../../api/ChatAPI';
import Loading from '../Loading';
import actions from '../../actions';
import config from '../../config';
import ConfirmModalWindow from '../Common/ConfirmModalWindow';
import SideBar from '../SideBar';
import * as actionCreators from '../../actionCreators';
import sendNewEvent, * as parentAPI from '../../api/ParentAPI';

export default () => {
  const [isRoomListVisible, setRoomListVisibility] = useState(true);
  const [isLoadingVisible, setLoadingVisibility] = useState(true);
  const dispatch = useDispatch();

  const onNewMessage = (data) => {
    const { args } = data;

    if (!args) {
      console.error(new Error('invalid args'));
      return;
    }

    dispatch(
      {
        type: actions.MESSAGE_RECEIVED,
        payload: args,
      },
    );

    sendNewEvent(data.cmd);
  };

  const onRoomSettingsUpdated = (data) => {
    if (!data.args) {
      console.error(new Error('invalid args'));
      return;
    }
    const { id } = data.args;
    dispatch({
      type: actions.ROOM_SETTINGS_UPDATED,
      payload: {
        id,
        room: data.args,
      },
    });
    sendNewEvent(data.cmd);
  };

  const onRoomClosed = (data) => {
    if (!data.args) {
      console.error(new Error('invalid args'));
      return;
    }
    const { roomId } = data.args;
    dispatch({
      type: actions.DELETE_ROOM_STORE,
      payload: {
        roomId,
      },
    });
  };

  const onNextUnreadId = (data) => {
    const { args } = data;

    if (!args) {
      console.error(new Error('invalid args'));
      return;
    }

    dispatch({
      type: actions.NEXT_UNREAD_ID,
      payload: {
        ...args,
      },
    });
  };

  const onOpenChat = () => dispatch(actionCreators.openChat());

  const setup = () => {
    chatAPI.onConnected(() => {
      parentAPI.onOpenChat(onOpenChat);
      parentAPI.sendConnected();
      const urlParams = new URLSearchParams(window.location.search);
      const userID = urlParams.get('userID');
      const tagsStr = urlParams.get('tags');
      const tags = tagsStr.split(',');
      dispatch({ type: actions.SET_USERINFO, payload: { username: userID, isGuest: false, tags } });
      chatAPI.authConnection({
        username: userID,
      }).then(() => {
        setLoadingVisibility(false);
      }).then(() => dispatch({ type: actions.APPEND_TO_ALL_PENDING_ROOMS }))
        .then(() => {
          dispatch({ type: actions.GET_USER_ROOMS });
        });
    });
    chatAPI.onDisconnected(() => {
      console.log('there will be your callback');
    });
    chatAPI.onMsg(onNewMessage);
    chatAPI.onNextUnreadId(onNextUnreadId);
    chatAPI.onRoomSettingsUpdated(onRoomSettingsUpdated);
    chatAPI.onRoomClosed(onRoomClosed);
    chatAPI.start({
      url: config.backendWebSocket,
      pingMsg: config.wsPingMsg,
      pingWait: config.wsPingWait,
      pingInterval: config.wsPingInterval,
      reconnectDelay: config.wsReconnectDelay,
    });
  };

  useEffect(() => setup(), []);

  return (
    <>
      {isLoadingVisible && <Loading />}
      <ConfirmModalWindow />
      <div className={`sidebar ${isRoomListVisible ? 'sidebar_visible' : ''}`}>
        <SideBar
          setRoomListVisibility={setRoomListVisibility}
        />
      </div>
      <div className="content">
        <Room setRoomListVisibility={setRoomListVisibility} />
      </div>
    </>
  );
};
