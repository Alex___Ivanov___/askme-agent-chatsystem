import actions from '../actions';

export const openChat = () => ({
  type: actions.OPEN_CHAT,
});

export const setActiveRoom = (roomId) => ({
  type: actions.SET_ACTIVE_ROOM,
  payload: roomId,
});

export const unsetActiveRoom = () => ({
  type: actions.SET_ACTIVE_ROOM,
  payload: '',
});

export const setPrevActiveRoom = (roomId) => ({
  type: actions.SET_PREV_ACTIVE_ROOM,
  payload: roomId,
});

export const unsetPrevActiveRoom = () => ({
  type: actions.SET_PREV_ACTIVE_ROOM,
  payload: '',
});

export const getUserRooms = () => ({
  type: actions.GET_USER_ROOMS,
});

export const messageReceived = (args) => ({
  type: actions.MESSAGE_RECEIVED,
  payload: args,
});

export const nextUnreadId = (args) => ({
  type: actions.NEXT_UNREAD_ID,
  payload: {
    ...args,
  },
});

export const updateRoom = (args) => ({
  type: actions.UPDATE_ROOM,
  payload: args,
});

export const clearHistory = (roomId) => ({
  type: actions.CLEAR_HISTORY_STORE,
  payload: {
    roomId,
  },
});

export const updateRoomSettingStore = (args) => ({
  type: actions.UPDATE_ROOM_SETTINGS_STORE,
  payload: args,
});

export const roomSettingUpdated = (id, room) => ({
  type: actions.ROOM_SETTINGS_UPDATED,
  payload: {
    id,
    room,
  },
});
