export const getRooms = (state) => state.roomsReducer.rooms;
export const getMessages = (state) => state.messagesReducer;
export const getUserInfo = (state) => state.userInfoReducer;
export const getUsername = (state) => state.userInfoReducer.username;
export const getActiveRoomId = (state) => state.roomsReducer.activeRoomID;
export const getPrevActiveRoomId = (state) => state.roomsReducer.prevActiveRoomID;
