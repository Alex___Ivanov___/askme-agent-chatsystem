import './index.css';

let iframe = null;
let currentUser = 'monterey';
let prevUser = '';
const iframeURL = 'app.html';
/* eslint-disable */
const iframeOrigin = IFRAME_DOMAIN;
const authData = {
  rururu: {
    username: 'rururu',
    blockNumber: 1,
    transactionId: '1',
  },
};

const sendMsgToIframe = (msg) => {
  if (iframe) {
    iframe.contentWindow.postMessage(msg, iframeOrigin);
  }
};

const openChat = () => {
  const container = document.getElementById('chat');
  if (prevUser !== currentUser) {
    console.log('TestPage: user changed');
    if (iframe) {
      container.removeChild(iframe);
      iframe = null;
    }
  }
  if (!iframe) {
    iframe = document.createElement('iframe');
    iframe.classList.add('chat-iframe');
    iframe.id = 'chat-iframe';
    iframe.sandbox = 'allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-top-navigation';
    iframe.src = `${iframeURL}?userID=${currentUser}&iframe=true&tags=#fun`;
    container.append(iframe);
    prevUser = currentUser;
  } else {
    iframe.style.display = iframe.style.display === 'none' ? 'flex' : 'none';
    if (iframe.style.display==="flex") {
      sendMsgToIframe({ cmd: 'open_chat'});
    }
  }
};

const onConnected = () => {
  sendMsgToIframe({ cmd: 'register_origin'});
};

const userSelect = (e) => {
  prevUser = currentUser;
  currentUser = e.target.value;
};

window.onload = () => {
  currentUser = document.getElementById('username').value;
  document.getElementById('username').addEventListener('change', userSelect);
  document.getElementById('btn-run-chat').addEventListener('click', openChat);
  // Receive message from IFRAME
  window.addEventListener('message', (MessageEvent) => {
    if (MessageEvent.origin !== iframeOrigin) {
      return;
    }
    const { cmd, args } = MessageEvent.data;
    if (typeof cmd === 'undefined') {
      console.log('TestPage: post message data has no "cmd" property');
      return;
    }
    switch (cmd) {
      case 'connected':
        onConnected();
        break;
      case 'close_chat':
        document.getElementById('chat-iframe').style.display = 'none';
        break;
      case 'new_event':
        console.log(`new event with name: ${args.name} has been received`);
        break;
      default:
    }
    console.log(`TestPage: "${cmd}" post message has been received from iframe \n with args ${JSON.stringify(args)}`);
  });
};
