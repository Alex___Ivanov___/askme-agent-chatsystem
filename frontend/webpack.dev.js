const merge = require('webpack-merge');
const { DefinePlugin } = require('webpack');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    watchOptions: {
      poll: true,
    },
    hot: true,
    open: true,
  },
  plugins: [
    new DefinePlugin({
      DEBUG_LOG: JSON.stringify(true),
      BACKEND_WEBSOCKET: JSON.stringify('ws://localhost:8090'),
      ORIGIN_DOMAIN_WHITELIST: JSON.stringify(['http://localhost:8080']),
      IFRAME_DOMAIN: JSON.stringify('http://localhost:8080'),
    }),
  ],
});
