const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const rules = [
  { test: /\.js$/, exclude: /node_modules/, use: ['babel-loader', 'eslint-loader'] },
  { test: /\.css$/, use: ['style-loader', 'css-loader'] },
  { test: /\.(png|svg|jpg|gif)$/, use: ['file-loader'] },
  {
    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'fonts/',
        },
      },
    ],
  },
];

module.exports = {
  entry: {
    index: path.join(__dirname, '/src/testpage/test.js'),
    stchat: path.join(__dirname, '/src/stchat/index.js'),
  },
  output: {
    path: path.join(__dirname, '/dist'),
    filename: '[name].js',
  },
  module: {
    rules,
  },
  plugins: [

    new HtmlWebpackPlugin({
      template: path.join(__dirname, '/src/testpage/index.html'),
      filename: 'index.html',
      chunks: ['index'],

    }),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '/src/stchat/index.html'),
      filename: 'app.html',
      chunks: ['stchat'],
    }),
  ],
};
