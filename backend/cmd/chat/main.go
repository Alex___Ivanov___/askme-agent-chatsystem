package main

import (
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/go_bindata"
	"github.com/gorilla/websocket"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"social-trading-chat/backend/config"
	"social-trading-chat/backend/connpool"
	"social-trading-chat/backend/handlers"
	"social-trading-chat/backend/migrations"
	"social-trading-chat/backend/msgproc"
	"social-trading-chat/backend/services"
	"social-trading-chat/backend/storage"
)

const (
	logFormat     = "[${level}]\t ${time_rfc3339} ${short_file}:${line} |"
	logHTTPFormat = "[${method}]\t ${time_rfc3339} ${host}${uri} | ${status} ${remote_ip} ${user_agent}\n"
)

func main() {
	var conf config.ChatConfig
	log.SetHeader(logFormat)

	err := config.GetConfig(&conf)
	if err != nil {
		log.Fatal("Failed to load config: ", err)
	}
	log.SetLevel(log.Lvl(conf.LogLevel))

	db, err := sqlx.Open("postgres", conf.DbInfo.DataSourceName)
	if err != nil {
		log.Fatal("Failed to open db connection: ", err)
	}

	wsUpgrader := &websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	runMigrate(conf.DbInfo.DataSourceName)

	userConns := services.NewUserConns()
	connPool := connpool.NewConnPool()

	roomStorage, err := storage.NewRoomStorage(db)
	if err != nil {
		log.Fatal("Failed to create RoomStorage: ", err)
	}
	messageStorage, err := storage.NewMessageStorage(db)
	if err != nil {
		log.Fatal("Failed to create MessageStorage: ", err)
	}

	userManager, err := services.NewUserManager(connPool, userConns)
	if err != nil {
		log.Fatal("Failed to create UserManager: ", err)
	}

	messageComposer := msgproc.NewMessageComposer()

	permissionManager := services.NewPermissionManager()

	err = permissionManager.AllowedCommands(map[string]struct{}{
		"get_session_id":  {},
		"auth_connection": {},
	})
	if err != nil {
		log.Fatal("Failed to set AllowedCommands in permissionManager: ", err)
	}

	validate := validator.New()

	parserCreator := func() msgproc.IMessageParser {
		return &msgproc.MessageParser{}
	}

	composerCreator := msgproc.NewMessageComposer

	contextCreator := msgproc.WsContextCreator(parserCreator, composerCreator, validate, userManager)

	_ = connPool.AddCloseCb(func(connID string) {
		userID, _ := userConns.GetUserByConnID(connID)
		_ = userConns.DeleteUserConn(userID, connID)
	})

	e := echo.New()
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: logHTTPFormat,
	}))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
	}))

	e.GET("/", handlers.WsHandler(wsUpgrader, connPool))
	e.GET("/user/:user_id/unread_count", handlers.UnreadMessageCount(messageStorage))

	clientMsgRouter, err := msgproc.NewClientDataRouter(contextCreator)
	if err != nil {
		log.Fatal("Failed to create ClientDataRouter: ", err)
	}

	connPool.ReceiveCb(clientMsgRouter.Process)

	err = clientMsgRouter.SetPermissionManager(permissionManager)
	if err != nil {
		log.Fatal("Failed to set permissionManager in msgRouter: ", err)
	}

	botSessionVerifier := services.NewBotSessionVerifier(
		conf.BotSessionVirificatorURL, conf.StabBotSessionDataVerificator)
	userSessionVerifier := services.NewUserSessionVerifier(
		conf.UserSessionVirificatorURL, conf.StabUserSessionDataVerificator)

	authProc := msgproc.Auth(userManager, userSessionVerifier, botSessionVerifier, permissionManager, messageComposer)
	registerOrFatal(clientMsgRouter, "auth_connection", authProc)

	getSessionByIdProc := msgproc.GetSessionByID(connPool, messageComposer)
	registerOrFatal(clientMsgRouter, "get_session_id", getSessionByIdProc)

	clientMsgProc := msgproc.ClientMsg(userManager, roomStorage, messageStorage)
	registerOrFatal(clientMsgRouter, "new_message", clientMsgProc)

	getUserRoomsProc := msgproc.GetUserRooms(userManager, roomStorage, storage.NewRoomGetOpts)
	registerOrFatal(clientMsgRouter, "get_user_rooms", getUserRoomsProc)

	getRoomMsg := msgproc.GetRoomMsg(userManager, roomStorage, messageStorage)
	registerOrFatal(clientMsgRouter, "get_room_messages", getRoomMsg)

	getRoomUnreadMsg := msgproc.GetRoomUnreadMsg(messageStorage)
	registerOrFatal(clientMsgRouter, "get_room_unread_messages", getRoomUnreadMsg)

	nextUnreadMsg := msgproc.NextUnreadMsg(userManager, roomStorage)
	registerOrFatal(clientMsgRouter, "next_unread_message", nextUnreadMsg)

	searchRooms := msgproc.SearchRooms(roomStorage)
	registerOrFatal(clientMsgRouter, "search_rooms", searchRooms)

	changeBotActivity := msgproc.ChangeBotActivity(roomStorage)
	registerOrFatal(clientMsgRouter, "change_bot_activity", changeBotActivity)

	closeRoom := msgproc.CloseRoom(roomStorage)
	registerOrFatal(clientMsgRouter, "close_room", closeRoom)

	newRating := msgproc.NewRating(roomStorage, userManager)
	registerOrFatal(clientMsgRouter, "new_rating", newRating)

	if noSslCertsInConfig(conf) {
		log.Fatal(e.Start(conf.Addr))
	}
	log.Fatal(e.StartTLS(conf.Addr, conf.Cert.Crt, conf.Cert.Key))
}

func noSslCertsInConfig(conf config.ChatConfig) bool {
	return conf.Cert.Crt == "" && conf.Cert.Key == ""
}

func registerOrFatal(router *msgproc.ClientDataRouter, cmd string, procFunc msgproc.InboundDataProcessor) {
	if err := router.Register(cmd, procFunc); err != nil {
		log.Fatal("Failed to register processor: ", err)
	}
}

func runMigrate(dsn string) {
	s := bindata.Resource(migrations.AssetNames(), migrations.Asset)
	d, err := bindata.WithInstance(s)
	if err != nil {
		log.Fatal(err)
	}
	m, err := migrate.NewWithSourceInstance("go-bindata", d, dsn)
	if err != nil {
		log.Fatal(err)
	}
	if err = m.Up(); err != nil {
		if err == migrate.ErrNoChange {
			log.Warn(err)
		} else {
			log.Fatal(err)
		}
	}
}
