package storage

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // A pure Go postgres driver for Go's database/sql package
	"reflect"
	"time"

	"gopkg.in/go-playground/validator.v9"
	"social-trading-chat/backend/msgproc/types"
)

type getArgs struct {
	RoomID string `validate:"required"`
	MsgID  int64  `validate:"required"`
	Count  int    `validate:"required"`
}

const getUnreadMsgCountQuery = `
with moderatedMessage as (
    with oldest as (
        select msg_id, room_id, msg_time from messages
        where msg_id in (select unread_message from room_setting where user_room in (
            select id from user_rooms where user_id = $1 )))
    select count(messages.msg_id) from messages inner join oldest on oldest.room_id = messages.room_id
    where oldest.msg_id is not null and oldest.msg_id <= messages.msg_id and is_moderated = 2 group by oldest.room_id)
select sum(moderatedMessage.count) from moderatedMessage`

const setNewMsgAndUpdateUnreadMsg = `
	with message_id as (
	    insert into messages (sender, text, msg_time, room_id) values ($1, $2, $3, $4) returning msg_id),
    updated as (
        update room_setting set unread_message = message_id.msg_id from message_id
            where user_room in (
                select id from user_rooms where room_id = $4 and user_id != $1) 
                      and unread_message is null)
	select * from message_id`

const getUnreadMsgByCountQuery = `
	with res as (
	    select sender, text, msg_time, msg_id, is_moderated from messages 
	    where room_id = $1 and  msg_id >= $2 and is_moderated = 2 limit $3)
	select * from res order by msg_id desc`

// MessageStorage is user for storing information about messages
type MessageStorage struct {
	validate *validator.Validate
	db       *sqlx.DB
}

// NewMessageStorage is initializer of MessageStorage
// Should return error when db param is nil
func NewMessageStorage(db *sqlx.DB) (*MessageStorage, error) {
	if db == nil || reflect.ValueOf(db).IsNil() {
		return nil, ErrInvalidParam
	}
	ms := &MessageStorage{
		db:       db,
		validate: validator.New(),
	}
	return ms, nil
}

// Add validates message struct and inserts message to storage, returns message id
// Returns error when message field aren't valid or DB returns error
func (ms *MessageStorage) Add(msg types.Message) (msgID int64, err error) {
	err = ms.validate.Struct(msg)
	_, parseErr := time.Parse(time.RFC3339, msg.Time)
	if err != nil || parseErr != nil {
		return msgID, ErrInvalidParam
	}

	tx, err := ms.db.Begin()
	if err != nil {
		return msgID, err
	}
	defer func() {
		var e error
		if err != nil {
			e = tx.Rollback()
			if e != nil {
				err = fmt.Errorf("%s: %s", err, e)
			}
		} else {
			err = tx.Commit()
			if err != nil {
				msgID = 0
			}
		}
	}()
	err = tx.QueryRow(setNewMsgAndUpdateUnreadMsg,
		msg.Sender, msg.Text, msg.Time, msg.RoomID).Scan(&msgID)
	return
}

// Get validates input param ant gets messages by room id, count and message id
// Returns error when input param aren't valid or DB returns error
func (ms *MessageStorage) Get(roomID string, msgID int64, count int) (msgs []types.Message, err error) {
	args := getArgs{
		RoomID: roomID,
		Count:  count,
		MsgID:  msgID,
	}
	err = ms.validate.Struct(args)
	if err != nil {
		return nil, ErrInvalidParam
	}
	query := `select sender, text, msg_time, msg_id, room_id, is_moderated from messages 
			where room_id = $1 and msg_id <= $2 and is_moderated = 2 order by msg_id desc limit $3`
	rows, err := ms.db.Query(query, args.RoomID, args.MsgID, args.Count)
	if err != nil {
		return
	}

	defer func() {
		var e error
		e = rows.Close()
		if err != nil {
			if e != nil {
				err = fmt.Errorf("%s: %s", err, e)
			}
		} else {
			err = e
		}
	}()

	for rows.Next() {
		var msg types.Message
		err = rows.Scan(&msg.Sender, &msg.Text, &msg.Time, &msg.Id, &msg.RoomID, &msg.IsModerated)
		if err != nil {
			msgs = nil
			return
		}
		msgs = append(msgs, msg)
	}
	return
}

// GetUnreadCount validates input user's ID and gets total count of unread messages(in all rooms) by user ID
// Returns error when user's ID isn't valid or DB returns error
func (ms *MessageStorage) GetUnreadCount(userID string) (int, error) {
	if userID == "" {
		return 0, ErrInvalidParam
	}
	var count sql.NullInt64
	if err := ms.db.QueryRow(getUnreadMsgCountQuery, userID).Scan(&count); err != nil {
		return 0, err
	}
	return int(count.Int64), nil
}

// GetUnreadMsgByCount validates input param ant gets unread messages by room id, count
// Returns error when input param aren't valid or DB returns error
func (ms *MessageStorage) GetUnreadMsgByCount(roomID string, msgID int64, count int) ([]types.Message, error) {
	if roomID == "" || msgID <= 0 || count <= 0 {
		return nil, ErrInvalidParam
	}
	rows, err := ms.db.Query(getUnreadMsgByCountQuery, roomID, msgID, count)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = rows.Close()
	}()

	var messages []types.Message
	for rows.Next() {
		var msg types.Message
		err = rows.Scan(&msg.Sender, &msg.Text, &msg.Time, &msg.Id, &msg.IsModerated)
		if err != nil {
			return nil, err
		}
		msg.RoomID = roomID
		messages = append(messages, msg)
	}
	return messages, err
}
