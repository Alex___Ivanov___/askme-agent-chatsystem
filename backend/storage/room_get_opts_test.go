package storage

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRoomGetOpts_BuildQuery(t *testing.T) {
	// arrange
	rgo, _ := NewRoomGetOpts("user")
	_ = rgo.SetTags([]string{"tag"})
	wantQuery := "select user_rooms.room_id from user_rooms inner join rooms r on user_rooms.room_id = r.room_id" +
		" where user_id = 'user' and r.status in ('in progress', 'pending')" +
		"and r.tag in ('tag')"
	// assert
	assert.Equal(t, wantQuery, rgo.BuildQuery())
}
