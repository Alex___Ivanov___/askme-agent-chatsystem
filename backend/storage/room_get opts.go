package storage

import (
	"errors"
	"social-trading-chat/backend/msgproc"
	"social-trading-chat/backend/msgproc/types"
	"strings"
)

type RoomGetOpts struct {
	userID   string
	statuses []types.RoomStatus
	tags     []string
}

func NewRoomGetOpts(userID string) (msgproc.RoomGetOpts, error) {
	if userID == "" {
		return nil, errors.New("userID is empty")
	}
	return &RoomGetOpts{
		userID:   userID,
		statuses: []types.RoomStatus{types.RoomInProgress, types.RoomPending},
	}, nil
}

func (rgo *RoomGetOpts) SetStatuses(statuses []types.RoomStatus) error {
	if statuses == nil {
		return errors.New("invalid statuses")
	}
	rgo.statuses = statuses
	return nil
}

func (rgo *RoomGetOpts) SetTags(tags []string) error {
	if tags == nil {
		return errors.New("invalid tags")
	}
	rgo.tags = tags
	return nil
}

func (rgo *RoomGetOpts) BuildQuery() string {
	var b strings.Builder
	b.WriteString("select user_rooms.room_id from user_rooms inner join rooms r on user_rooms.room_id = r.room_id")
	b.WriteString(" where user_id = '" + rgo.userID + "' ")
	if len(rgo.statuses) > 0 {
		b.WriteString("and r.status in (")
		for i := range rgo.statuses {
			b.WriteString("'")
			b.WriteString(string(rgo.statuses[i]))
			b.WriteString("'")
			if i == len(rgo.statuses)-1 {
				b.WriteString(")")
			} else {
				b.WriteString(", ")
			}
		}
	}
	if len(rgo.tags) > 0 {
		b.WriteString(" and r.tag in (")
		for i := range rgo.tags {
			b.WriteString("'")
			b.WriteString(rgo.tags[i])
			b.WriteString("'")
			if i == len(rgo.tags)-1 {
				b.WriteString(")")
			} else {
				b.WriteString(", ")
			}
		}
	}
	return b.String()
}

func (rgo *RoomGetOpts) UserID() string {
	return rgo.userID
}
