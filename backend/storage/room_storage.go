package storage

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/gommon/log"
	"github.com/lib/pq"
	_ "github.com/lib/pq" // A pure Go postgres driver for Go's database/sql package
	"gopkg.in/go-playground/validator.v9"
	"reflect"
	"social-trading-chat/backend/errtypes"
	"social-trading-chat/backend/msgproc"
	"social-trading-chat/backend/msgproc/types"
)

// ErrInvalidParam is error which is returned when input params are invalid
var ErrInvalidParam = errors.New("input param is invalid")

// ErrDatabaseConnection is error which is returned when queries to DB without args, like db.Begin()
// or tx.Rollback(), tx.Commit() returns error
var ErrDatabaseConnection = errors.New("there is something wrong with database connection")

type usersProcessingArgs struct {
	RoomID  string   `validate:"required"`
	UserIds []string `validate:"gt=0,dive,required"`
}

// RoomStorage is user for storing information about rooms and room members
type RoomStorage struct {
	validate *validator.Validate
	db       *sqlx.DB
}

// NewRoomStorage is initializer of RoomStorage
// Should return error when db param is nil
// or init query returns error
func NewRoomStorage(db *sqlx.DB) (*RoomStorage, error) {
	if db == nil || reflect.ValueOf(db).IsNil() {
		return nil, ErrInvalidParam
	}

	rs := &RoomStorage{
		db:       db,
		validate: validator.New(),
	}
	return rs, nil
}

const insertUserRoomQuery = `
	with rows as (
    insert into user_rooms (user_id, room_id) values ($1, $2)
        on conflict (user_id, room_id) do nothing
        returning id)
insert into room_setting (user_room) select id from rows`

const updateOldestUnreadMsgQuery = `
	update room_setting set unread_message = case
		when ($1::bigint is not null)
		then case when (unread_message is null or unread_message < $1) then $1 else unread_message end
		else null end
		where user_room in (select id from user_rooms where room_id = $2 and user_id = any($3))`

const getRoomInfoQuery = `
	with oldest_unread_msg_id as (
	    select unread_message from room_setting
			where user_room in (
			    select id from user_rooms where user_id = $2 and room_id = $1)),
	unread_msg_count as (
	    select count(msg_time) from messages
     		where (select unread_message from oldest_unread_msg_id) is not null 
       		and msg_id>=(select unread_message from oldest_unread_msg_id) and room_id = $1),
	last_msg as (
	    select msg_id,sender,text,msg_time from messages
			where room_id=$1 order by msg_id desc limit 1)
	select * from last_msg, oldest_unread_msg_id, unread_msg_count`

// Create creates row with room_id, room_name, creator_id and room_type params in room table
// and creates rows with user_id and room_id params in user_rooms table
// Returns created room id and error
// Should return error when input room fields aren't valid or DB returns error
// For correct work you should add creator to members field, and members field should consist 2 users
// If DB returns error process Rollback will be called
func (rs *RoomStorage) Create(room types.Room) (roomID string, err error) {
	err = rs.validate.Struct(room)
	if err != nil {
		log.Error(err)
		err = ErrInvalidParam
		return
	}

	tx, err := rs.db.Begin()
	if err != nil {
		log.Error(err)
		err = ErrDatabaseConnection
		return
	}

	defer func() {
		var cmdError error
		if err != nil {
			roomID = ""
			cmdError = tx.Rollback()
		} else {
			cmdError = tx.Commit()
		}

		if cmdError != nil {
			roomID = ""
			err = cmdError
		}
	}()

	query := `insert into rooms (room_id, manager_id, bot_activity, status, tag, client_id) 
	values ($1, $2, $3, $4, $5, $6) returning room_id`
	err = tx.QueryRow(query, room.ID, room.ManagerId, room.BotActivity, room.Status, room.Tag, room.ClientID).Scan(&roomID)
	if err != nil {
		return
	}

	for _, member := range room.Members {
		_, err = tx.Exec(insertUserRoomQuery, member, roomID)
		if err != nil {
			return
		}
	}
	return
}

// AddUsers inserts row with user_id and room_id params in user_rooms table
// Should return error when roomId is empty or userIds length is 0 or
// DB returns error on queries
func (rs *RoomStorage) AddUsers(roomId string, userIds ...string) (err error) {
	err = rs.validate.Struct(usersProcessingArgs{
		RoomID:  roomId,
		UserIds: userIds,
	})
	if err != nil {
		log.Error(err)
		err = ErrInvalidParam
		return
	}

	tx, err := rs.db.Begin()
	if err != nil {
		log.Error(err)
		err = ErrDatabaseConnection
		return
	}

	defer func() {
		var cmdError error
		if err != nil {
			cmdError = tx.Rollback()
		} else {
			cmdError = tx.Commit()
		}

		if cmdError != nil {
			err = cmdError
		}
	}()

	for _, userId := range userIds {
		_, err = tx.Exec(insertUserRoomQuery, userId, roomId)
		if err != nil {
			return
		}
	}

	return
}

// GetByID gets room info by room id
// Should return error when roomID is empty or
// DB returns error on queries
func (rs *RoomStorage) GetByID(roomID string) (types.Room, error) {
	room, err := rs.getByID(roomID)
	if err == sql.ErrNoRows {
		err = types.ErrRoomNotExisted
	}
	return room, err
}

func (rs *RoomStorage) GetActiveClientRoom(clientID string) (types.Room, error) {
	query := `select room_id, manager_id, bot_activity, status, tag from rooms
where status != 'closed' and client_id = $1`
	room := types.Room{}
	err := rs.db.QueryRow(query, clientID).
		Scan(&room.ID, &room.ManagerId, &room.BotActivity, &room.Status, &room.Tag)
	switch err {
	case nil:
		rows, err := rs.db.Query("select user_id from user_rooms where room_id = $1", room.ID)
		if err != nil {
			return types.Room{}, err
		}
		defer func() {
			closeErr := rows.Close()
			if closeErr != nil {
				log.Error()
			}
		}()
		for rows.Next() {
			var userID string
			err = rows.Scan(&userID)
			if err != nil {
				return types.Room{}, err
			}
			room.Members = append(room.Members, userID)
		}
		return room, nil
	case sql.ErrNoRows:
		err = types.ErrRoomNotExisted
		return room, err
	}
	return types.Room{}, err
}

func (rs *RoomStorage) GetByRoomAndUser(roomID string, userID string) (room types.Room, err error) {
	room, err = rs.getByID(roomID)
	if err != nil {
		return
	}
	room.LatestMsg = &types.Message{}
	oldestMsgId := sql.NullInt64{}
	queryErr := rs.db.QueryRow(getRoomInfoQuery, roomID, userID).Scan(
		&room.LatestMsg.Id, &room.LatestMsg.Sender, &room.LatestMsg.Text, &room.LatestMsg.Time, &oldestMsgId, &room.UnreadMsgCount)
	if queryErr != nil && queryErr != sql.ErrNoRows {
		err = queryErr
		return
	}
	room.LatestMsg.RoomID = roomID
	if room.LatestMsg.Id == 0 {
		room.LatestMsg = nil
	}
	room.OldestMsgId = oldestMsgId.Int64
	room.ID = roomID
	return
}

func (rs *RoomStorage) getByID(roomID string) (types.Room, error) {
	if roomID == "" {
		return types.Room{}, ErrInvalidParam
	}
	room := types.Room{
		ID: roomID,
	}

	query := `select manager_id, bot_activity, status, tag, closed_at, client_id from rooms where room_id = $1`
	err := rs.db.QueryRow(query, roomID).
		Scan(&room.ManagerId, &room.BotActivity, &room.Status, &room.Tag, &room.ClosedAt, &room.ClientID)
	if err != nil {
		return types.Room{}, err
	}

	rows, err := rs.db.Query("select user_id from user_rooms where room_id = $1", roomID)
	if err != nil {
		return types.Room{}, err
	}
	defer func() {
		closeErr := rows.Close()
		if closeErr != nil {
			log.Error()
		}
	}()
	for rows.Next() {
		var userID string
		err = rows.Scan(&userID)
		if err != nil {
			return types.Room{}, err
		}
		room.Members = append(room.Members, userID)
	}
	return room, nil
}

// GetByOpts gets information about all user's rooms by user id
// Should return error when userID is empty or
// DB returns error on queries
func (rs *RoomStorage) GetByOpts(opts msgproc.RoomGetOpts) (rooms []types.Room, err error) {
	if opts == nil {
		err = ErrInvalidParam
		return
	}
	rows, err := rs.db.Query(opts.BuildQuery())
	if err != nil {
		return
	}
	defer func() {
		closeErr := rows.Close()
		if closeErr != nil {
			log.Error()
		}
	}()
	for rows.Next() {
		var roomID string
		err = rows.Scan(&roomID)
		if err != nil {
			return
		}
		var room types.Room
		room, err = rs.getByID(roomID)
		if err != nil {
			return
		}
		room.LatestMsg = &types.Message{}
		oldestMsgId := sql.NullInt64{}
		queryErr := rs.db.QueryRow(getRoomInfoQuery, roomID, opts.UserID()).
			Scan(&room.LatestMsg.Id,
				&room.LatestMsg.Sender,
				&room.LatestMsg.Text,
				&room.LatestMsg.Time,
				&oldestMsgId,
				&room.UnreadMsgCount,
			)
		if queryErr != nil && queryErr != sql.ErrNoRows {
			err = queryErr
			return
		}
		room.LatestMsg.RoomID = roomID
		if room.LatestMsg.Id == 0 {
			room.LatestMsg = nil
		}
		room.OldestMsgId = oldestMsgId.Int64
		rooms = append(rooms, room)
	}
	return
}

// DeleteUsers deletes row with user_id and room_id params from user_rooms table
// Should return error when roomId is empty or userIds length is 0 or
// DB returns error on queries
func (rs *RoomStorage) DeleteUsers(roomId string, userIds ...string) (err error) {
	err = rs.validate.Struct(usersProcessingArgs{
		RoomID:  roomId,
		UserIds: userIds,
	})
	if err != nil {
		log.Error(err)
		err = ErrInvalidParam
		return
	}

	tx, err := rs.db.Begin()
	if err != nil {
		log.Error(err)
		err = ErrDatabaseConnection
		return
	}

	defer func() {
		var cmdError error
		if err != nil {
			cmdError = tx.Rollback()
		} else {
			cmdError = tx.Commit()
		}

		if cmdError != nil {
			roomId = ""
			err = cmdError
		}
	}()

	for _, userId := range userIds {
		_, err = tx.Exec("delete from user_rooms where user_id = $1 and room_id = $2", userId, roomId)
		if err != nil {
			return
		}
	}
	return
}

// Update should updates fields - room_name, creator_id and room_type when input room are valid
// returns updated room and should return error when room isn’t existed
func (rs *RoomStorage) Update(room types.Room) (updatedRoom types.Room, err error) {
	err = rs.validate.Struct(room)
	if err != nil {
		log.Error(err)
		err = ErrInvalidParam
		return
	}

	tx, err := rs.db.Begin()
	if err != nil {
		log.Error(err)
		err = ErrDatabaseConnection
		return
	}

	defer func() {
		var cmdError error
		if err != nil {
			cmdError = tx.Rollback()
		} else {
			cmdError = tx.Commit()
		}

		if cmdError != nil {
			err = cmdError
		}

		if err == nil {
			updatedRoom = room
		}
	}()

	query := `
		update rooms set manager_id = $1, bot_activity = $3, status = $4, tag = $5, closed_at = $6, rating = $7
		where room_id = $2`
	_, err = tx.Exec(
		query,
		room.ManagerId,
		room.ID,
		room.BotActivity,
		room.Status,
		room.Tag,
		room.ClosedAt,
		room.Rating,
	)
	return
}

// UpdateOldestUnreadMsg changes users' oldest unread message in Postgres by roomID
// It can receive nil msgID
// Returns error when roomID is empty, users length is zero or DB returns error
func (rs *RoomStorage) UpdateOldestUnreadMsg(roomID string, users []string, msgID int64) (err error) {
	if roomID == "" {
		return ErrInvalidParam
	}
	if len(users) == 0 {
		return ErrInvalidParam
	}

	msgIdNullable := sql.NullInt64{}
	if msgID > 0 {
		msgIdNullable.Valid = true
		msgIdNullable.Int64 = msgID
	}

	tx, err := rs.db.Begin()
	if err != nil {
		log.Error(err)
		err = ErrDatabaseConnection
		return
	}

	defer func() {
		var cmdError error
		if err != nil {
			cmdError = tx.Rollback()
		} else {
			cmdError = tx.Commit()
		}

		if cmdError != nil {
			err = cmdError
		}
	}()

	_, err = tx.Exec(updateOldestUnreadMsgQuery, msgIdNullable, roomID, pq.Array(users))
	return
}

// DeleteRoom validates roomID and deletes room from DB. Returns error
// if roomID isn't valid or DB return error
func (rs *RoomStorage) DeleteRoom(roomID string) (err error) {
	if roomID == "" {
		err = errtypes.ErrFuncArg{}.Invalidate("roomID")
		return
	}

	tx, err := rs.db.Begin()
	if err != nil {
		log.Error(err)
		err = ErrDatabaseConnection
		return
	}

	defer func() {
		var cmdError error
		if err != nil {
			cmdError = tx.Rollback()
		} else {
			cmdError = tx.Commit()
		}

		if cmdError != nil {
			err = cmdError
		}
	}()

	_, err = tx.Exec(`delete from rooms where room_id = $1`, roomID)
	return
}

// Search finds all rooms which contain searchStr and doesn't contain user
// Returns error when searchStr is empty or DB returns error
func (rs *RoomStorage) Search(searchStr string, user string, count int) (rooms []types.Room, err error) {
	if searchStr == "" {
		err = errtypes.ErrFuncArg{}.Invalidate("searchStr")
		return
	}
	if user == "" {
		err = errtypes.ErrFuncArg{}.Invalidate("user")
		return
	}
	if count < 1 {
		err = errtypes.ErrFuncArg{}.Invalidate("count")
		return
	}

	searchStr = fmt.Sprintf("%%%s%%", searchStr)
	query := `select rooms.room_id, manager_id from rooms
				inner join user_rooms ur on rooms.room_id = ur.room_id
				where lower(rooms.room_id) like lower($1) and ur.user_id != $2
				limit $3`

	rows, err := rs.db.Query(query, searchStr, user, count)
	if err != nil {
		return
	}

	defer func() {
		closeErr := rows.Close()
		if err != nil || closeErr != nil {
			rooms = nil
			err = fmt.Errorf("func error: %v;\nclose error: %v", err, closeErr)
		}
	}()

	rooms = []types.Room{}
	for rows.Next() {
		room := types.Room{}
		err = rows.Scan(&room.ID, &room.ManagerId)
		if err != nil {
			return
		}
		rooms = append(rooms, room)
	}
	return
}

func (rs *RoomStorage) GetManagedRooms(userID string) (rooms []types.Room, err error) {
	if userID == "" {
		err = ErrInvalidParam
		return
	}
	rows, err := rs.db.Query("select room_id from rooms where manager_id = $1", userID)
	if err != nil {
		return
	}
	defer func() {
		closeErr := rows.Close()
		if closeErr != nil {
			log.Error()
		}
	}()
	for rows.Next() {
		var roomID string
		err = rows.Scan(&roomID)
		if err != nil {
			return
		}
		var room types.Room
		room, err = rs.getByID(roomID)
		if err != nil {
			return
		}
		room.LatestMsg = &types.Message{}
		oldestMsgId := sql.NullInt64{}
		queryErr := rs.db.QueryRow(getRoomInfoQuery, roomID, userID).
			Scan(&room.LatestMsg.Id,
				&room.LatestMsg.Sender,
				&room.LatestMsg.Text,
				&room.LatestMsg.Time,
				&oldestMsgId,
				&room.UnreadMsgCount,
			)
		if queryErr != nil && queryErr != sql.ErrNoRows {
			err = queryErr
			return
		}
		room.LatestMsg.RoomID = roomID
		if room.LatestMsg.Id == 0 {
			room.LatestMsg = nil
		}
		room.OldestMsgId = oldestMsgId.Int64
		rooms = append(rooms, room)
	}
	return
}

func (rs *RoomStorage) GetLatestClosedRoomByClient(clientID string) (room types.Room, err error) {
	query := `select room_id, manager_id, bot_activity, status, tag, closed_at from rooms
	where client_id = $1 order by closed_at limit 1`

	err = rs.db.QueryRow(query, clientID).
		Scan(&room.ID, &room.ManagerId, &room.BotActivity, &room.Status, &room.Tag, &room.ClosedAt)
	if err != nil {
		return types.Room{}, err
	}

	rows, err := rs.db.Query("select user_id from user_rooms where room_id = $1", room.ID)
	if err != nil {
		return types.Room{}, err
	}
	defer func() {
		closeErr := rows.Close()
		if closeErr != nil {
			log.Error()
		}
	}()
	for rows.Next() {
		var userID string
		err = rows.Scan(&userID)
		if err != nil {
			return types.Room{}, err
		}
		room.Members = append(room.Members, userID)
	}
	return room, nil
}
