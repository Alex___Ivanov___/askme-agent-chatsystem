package services

import (
	"github.com/labstack/gommon/log"
	"reflect"
	"social-trading-chat/backend/errtypes"
)

// UserManager combines ConnPool and UserConns functionality
type UserManager struct {
	IConnPool
	IUserConns
}

// NewUserManager is initializer for UserManager
func NewUserManager(connPool IConnPool, userConns IUserConns) (*UserManager, error) {
	if connPool == nil || reflect.ValueOf(connPool).IsNil() {
		return nil, errtypes.ErrFuncArg{}.Invalidate("connPool")
	}
	if userConns == nil || reflect.ValueOf(userConns).IsNil() {
		return nil, errtypes.ErrFuncArg{}.Invalidate("userConns")
	}
	return &UserManager{
		IConnPool:  connPool,
		IUserConns: userConns,
	}, nil
}

// Broadcast broadcasts message to all users' connections
// Does not broadcasts those who were passed in the parameter 'excludeConns'
func (um *UserManager) Broadcast(msg []byte, users []string, excludeConns ...string) {
	if len(msg) == 0 {
		log.Error("msg length is 0")
		return
	}
	if len(users) == 0 {
		log.Error("users length is 0")
		return
	}
	for _, user := range users {
		userConns, err := um.GetUserConns(user)
		if err != nil {
			continue
		}
		for _, excludeConn := range excludeConns {
			delete(userConns, excludeConn)
		}
		for userConn := range userConns {
			err := um.Send(msg, userConn)
			if err != nil {
				log.Error(err)
			}
		}
	}
}
