package services

import (
	"github.com/labstack/gommon/log"
	"net/http"
	"social-trading-chat/backend/errtypes"
	"social-trading-chat/backend/msgproc"
)

// BotSessionVerifier implements IVerifier
type UserSessionVerifier struct {
	reqURL string
	test   bool
}

func NewUserSessionVerifier(reqURL string, test bool) *UserSessionVerifier {
	if reqURL == "" {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("NewUserSessionVerifier"))
	}
	return &UserSessionVerifier{
		reqURL: reqURL,
		test:   test,
	}
}

func (sdv UserSessionVerifier) Verify(args msgproc.SessionDataArgs) error {
	if sdv.test {
		return nil
	}
	// TODO ADD ValidRequest

	resp, err := http.Get(sdv.reqURL + "/")
	if err != nil {
		return err
	}
	resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return ErrSessionNotAllowed
	}
	return nil
}
