package services

import (
	"errors"
	"sync"
)

var InvalidCommandsError = errors.New("invalid commands")
var InvalidConnError = errors.New("invalid conn")

// PermissionManager is needed for checking
// if the input connection can send input command
type PermissionManager struct {
	conns           map[string]struct{}
	allowedCommands map[string]struct{}
	m               sync.Mutex
}

// Initializer
func NewPermissionManager() *PermissionManager {
	return &PermissionManager{
		conns:           map[string]struct{}{},
		allowedCommands: map[string]struct{}{},
	}
}

// AddConn adds connection to conns map
func (pm *PermissionManager) AddConn(conn string) error {
	if conn == "" {
		return InvalidConnError
	}
	pm.m.Lock()
	pm.conns[conn] = struct{}{}
	pm.m.Unlock()
	return nil
}

// DeleteConn deletes connection from conns map
func (pm *PermissionManager) DeleteConn(conn string) error {
	if conn == "" {
		return InvalidConnError
	}
	pm.m.Lock()
	delete(pm.conns, conn)
	pm.m.Unlock()
	return nil
}

// AllowedCommands is setter for allowedCommands field
func (pm *PermissionManager) AllowedCommands(allowedCommands map[string]struct{}) error {
	if allowedCommands == nil {
		return InvalidCommandsError
	}
	pm.m.Lock()
	pm.allowedCommands = allowedCommands
	pm.m.Unlock()
	return nil
}

// CheckPermission checks if the input connection can send input command
func (pm *PermissionManager) CheckPermission(cmd string, conn string) bool {
	pm.m.Lock()
	defer pm.m.Unlock()
	_, ok := pm.allowedCommands[cmd]
	if ok {
		return true
	}
	_, ok = pm.conns[conn]
	return ok
}
