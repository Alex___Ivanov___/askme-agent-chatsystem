package services

import (
	"errors"
	"github.com/labstack/gommon/log"
	"net/http"
	"social-trading-chat/backend/errtypes"
	"social-trading-chat/backend/msgproc"
)

var ErrSessionNotAllowed = errors.New("session is not allowed")

// BotSessionVerifier implements IVerifier
type BotSessionVerifier struct {
	reqURL string
	test   bool
}

func NewBotSessionVerifier(reqURL string, test bool) *BotSessionVerifier {
	if reqURL == "" {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("NewBotSessionVerifier"))
	}
	return &BotSessionVerifier{
		reqURL: reqURL,
		test:   test,
	}
}

func (sdv BotSessionVerifier) Verify(args msgproc.SessionDataArgs) error {
	if sdv.test {
		return nil
	}
	resp, err := http.Get(sdv.reqURL + "/" + args.ConnID)
	if err != nil {
		return err
	}
	resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return ErrSessionNotAllowed
	}
	return nil
}
