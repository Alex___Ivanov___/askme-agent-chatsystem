package services

import (
	"errors"
	"social-trading-chat/backend/msgproc/types"
	"sync"
)

// InvalidConnID is error, which could be returned when conn_id is empty
var InvalidConnID = errors.New("connID is invalid")

// InvalidUserID is error, which could be returned when user_id is empty
var InvalidUserID = errors.New("userID is invalid")

// UserConns is instance for storing information about user and his connection
type UserConns struct {
	userConns map[string]map[string]types.ConnInfo
	connUser  map[string]string
	mu        sync.Mutex
}

// NewUserConns initializes UserConns instance with userConns and connsUser fields
func NewUserConns() *UserConns {
	return &UserConns{
		userConns: map[string]map[string]types.ConnInfo{},
		connUser:  map[string]string{},
	}
}

// DeleteUserConn deletes connections id by user id
func (uc *UserConns) DeleteUserConn(userID string, connID string) error {
	if userID == "" {
		return InvalidUserID
	}
	if connID == "" {
		return InvalidUserID
	}
	uc.mu.Lock()
	defer uc.mu.Unlock()
	conns, ok := uc.userConns[userID]
	if !ok {
		return InvalidUserID
	}
	delete(uc.connUser, connID)
	delete(conns, connID)
	if len(conns) == 0 {
		delete(uc.userConns, userID)
	}
	return nil
}

func (uc *UserConns) GetAllUsers() []string {
	uc.mu.Lock()
	var users []string
	for user := range uc.userConns {
		users = append(users, user)
	}
	uc.mu.Unlock()
	return users
}

// GetUserConns returns map of connections by user id
// Returns nil error, when map contains connections by user id, otherwise - error
func (uc *UserConns) GetUserConns(userID string) (userConnsCopy map[string]types.ConnInfo, err error) {
	uc.mu.Lock()
	userConns, ok := uc.userConns[userID]
	if !ok {
		err = InvalidUserID
	}
	userConnsCopy = map[string]types.ConnInfo{}
	for conn, info := range userConns {
		userConnsCopy[conn] = info
	}
	uc.mu.Unlock()
	return
}

func (uc *UserConns) IsBot(userID string, connID string) (bool, error) {
	uc.mu.Lock()
	defer uc.mu.Unlock()
	info, ok := uc.userConns[userID][connID]
	if !ok {
		return false, errors.New("invslid paramms")
	}
	return info.IsBot, nil
}

// SetUserConn creates or updates userID with map connIds
// Returns error when userID are connID is empty
func (uc *UserConns) SetUserConn(userID string, connID string, isBot bool) error {
	uc.mu.Lock()
	defer uc.mu.Unlock()
	if userID == "" {
		return InvalidUserID
	}
	if connID == "" {
		return InvalidConnID
	}
	_, ok := uc.userConns[userID]
	if !ok {
		uc.userConns[userID] = map[string]types.ConnInfo{}
	}
	uc.userConns[userID][connID] = types.ConnInfo{
		IsBot: isBot,
	}
	uc.connUser[connID] = userID
	return nil
}

// GetUserByConnID gets user id by conn id
func (uc *UserConns) GetUserByConnID(connID string) (string, error) {
	uc.mu.Lock()
	defer uc.mu.Unlock()
	userID, ok := uc.connUser[connID]
	if !ok {
		return "", InvalidConnID
	}
	return userID, nil
}
