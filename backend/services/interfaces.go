package services

import (
	"social-trading-chat/backend/connpool"
	"social-trading-chat/backend/msgproc/types"
)

//go:generate mockery -name IConnPool -case underscore -inpkg -testonly
type IConnPool interface {
	AddCloseCb(closeCb func(connID string)) error
	CallCloseCbs(connID string)
	Register(conn connpool.IConn, connUUID string) error
	ReceiveCb(cb func(msg []byte, connUUID string))
	Send(msg []byte, connUUID string) error
}

//go:generate mockery -name IUserConns -case underscore -inpkg -testonly
type IUserConns interface {
	GetAllUsers() []string
	// SetUserConn creates or updates user connections
	SetUserConn(userID string, connID string, isBot bool) error
	// GetUserConns gets connections of user by user id
	GetUserConns(userID string) (userConnsCopy map[string]types.ConnInfo, err error)
	// DeleteUserConn deletes connection of user by user id
	DeleteUserConn(userID string, connID string) error
	// GetUserByConnID gets user by connection id
	GetUserByConnID(connID string) (string, error)
	IsBot(userID string, connID string) (bool, error)
}
