package errtypes

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func mockFunc(testArg string) error {
	if testArg == "" {
		return ErrFuncArg{}.Invalidate("testArg")
	}
	return nil
}

func TestErrFuncArg_StaticFunc(t *testing.T) {

	gotErr := mockFunc("")
	wantErrStr := "mockFunc has invalid testArg argument"

	assert.Equal(t, wantErrStr, gotErr.Error())
}
