package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_configPath_configPath_has_passed_through_cli(t *testing.T) {
	// arrange
	wantConfigPath := "/path/to/my_config.json"
	flagStringVar = func(p *string, name, value, usage string) {
		*p = wantConfigPath
	}
	// act
	gotConfigPath := configPath()
	//assert
	assert.Equal(t, wantConfigPath, gotConfigPath)
}

func Test_configPath_configPath_has_not_passed_through_cli(t *testing.T) {
	// arrange
	wantConfigPath := "chat.json"
	flagStringVar = func(p *string, name, value, usage string) {
		*p = value
	}
	// act
	gotConfigPath := configPath()
	//assert
	assert.Equal(t, wantConfigPath, gotConfigPath)
}

func TestGetConfig_error(t *testing.T) {
	// arrange
	defaultPath := "./config.json"
	flagStringVar = func(p *string, name, value, usage string) {
		*p = defaultPath
	}
	// act
	err := GetConfig(struct{}{})
	// assert
	assert.NotNil(t, err)
}
