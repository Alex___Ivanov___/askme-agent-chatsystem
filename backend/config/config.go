package config

import (
	"flag"
	"github.com/tkanos/gonfig"
)

type dbConf struct {
	DataSourceName string
}

type Cert struct {
	Crt string
	Key string
}

// config for chat BE
type ChatConfig struct {
	Addr                           string
	DbInfo                         dbConf
	LogLevel                       int
	UserSessionVirificatorURL      string
	StabUserSessionDataVerificator bool
	BotSessionVirificatorURL       string
	StabBotSessionDataVerificator  bool
	SystemUsername                 string
	Cert                           Cert
}

var flagStringVar func(p *string, name, value, usage string)

func init() {
	flagStringVar = flag.StringVar
}

// configPath returns path to config
// path is set in -c flag
// if -c flag is empty, "chat.json" will be used
func configPath() (confPath string) {
	flagStringVar(&confPath, "c", "chat.json", "path to a configuration file")
	flag.Parse()
	return
}

// GetConfig parse config information in config file to config struct
// Receives pointer to config struct
// Calls configPath func and gonfig.GetConf
func GetConfig(configType interface{}) error {
	return gonfig.GetConf(configPath(), configType)
}
