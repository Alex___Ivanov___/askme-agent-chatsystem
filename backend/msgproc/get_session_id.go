package msgproc

import (
	"github.com/labstack/gommon/log"
	"reflect"
	"social-trading-chat/backend/errtypes"
)

// GetSessionByID sends to clients their connection id
func GetSessionByID(
	connPool IConnPool,
	msgComposer IMessageComposer,
) func(Context, string) {
	if connPool == nil || reflect.ValueOf(connPool).IsNil() {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("connPool"))
	}
	if msgComposer == nil || reflect.ValueOf(msgComposer).IsNil() {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("msgComposer"))
	}

	return func(ctx Context, connID string) {
		err := ctx.MakeResponse(ResponseMsg{
			Cmd: "get_session_id_response",
			Args: SessionIdArgs{
				SessionID: connID},
		})
		if err != nil {
			log.Error(err)
		}
	}
}
