package msgproc

import (
	"social-trading-chat/backend/msgproc/types"
)

type ResponseMsg struct {
	Cmd   string      `json:"cmd" validate:"required"`
	Error string      `json:"error,omitempty" validate:"-"`
	Args  interface{} `json:"args,omitempty" validate:"-"`
}

type MsgArgs struct {
	Sender string `json:"sender" mapstructure:"sender"`
	RoomID string `json:"roomId" mapstructure:"roomId" validate:"required"`
	Text   string `json:"text" mapstructure:"text" validate:"required"`
	Tag    string `json:"tag" mapstructure:"tag"`
}

type ModMsgArgs struct {
	Sender string `json:"sender" mapstructure:"sender"`
	RoomID string `json:"roomId" mapstructure:"roomId" validate:"required"`
	Text   string `json:"text" mapstructure:"text" validate:"required"`
}

type SessionIdArgs struct {
	SessionID string `json:"sessionId" mapstructure:"sessionId" validate:"required"`
}

type SessionDataArgs struct {
	ConnID      string                 `validate:"required"`
	IsBot       bool                   `json:"bot" mapstructure:"bot"`
	Username    string                 `json:"username" mapstructure:"username" validate:"required"`
	SessionData map[string]interface{} `json:"sessionData" mapstructure:"sessionData"`
}

type ChainSession struct {
	TransactionID string `json:"transactionId" mapstructure:"transactionId" validate:"required"`
	BlockNumber   int    `json:"blockNumber" mapstructure:"blockNumber" validate:"gt=0"`
}

type TokenSession struct {
	Token string `json:"token" mapstructure:"token" validate:"required"`
}

type AuthResponseArgs struct {
	Status string `json:"status" validate:"required"`
}

// UserRoomsResponseArgs contains slice of room
type UserRoomsResponseArgs struct {
	Rooms []types.Room `json:"rooms"`
}

// AddUsersArgs contains  slice of users and room ID where to add them
type AddUsersArgs struct {
	RoomID string   `json:"roomId" mapstructure:"roomId" validate:"required"`
	Users  []string `json:"users" mapstructure:"users" validate:"gt=0,dive,required"`
}

// DeleteUsersArgs contains  slice of users and room ID where to delete them
type DeleteUsersArgs struct {
	RoomID string   `json:"roomId" mapstructure:"roomId" validate:"required"`
	Users  []string `json:"users" mapstructure:"users" validate:"gt=0,dive,required"`
}

type GetRoomMsgArgs struct {
	RoomID string `json:"roomId" mapstructure:"roomId" validate:"required"`
	MsgID  int64  `json:"msgId" mapstructure:"msgId" validate:"required"`
	Count  int    `json:"count" mapstructure:"count" validate:"gt=0"`
}

type NextUnreadMessageArgs struct {
	RoomID    string `json:"roomId" mapstructure:"roomId" validate:"required"`
	MessageID int64  `json:"messageId" mapstructure:"messageId" validate:"gte=0"`
}

type GetRoomMsgResponseArgs struct {
	RoomID   string          `json:"roomId" mapstructure:"roomId"`
	Messages []types.Message `json:"messages" mapstructure:"messages"`
}

type StartChatArgs struct {
	Partner string `json:"partner" mapstructure:"partner" validate:"required"`
}

type StartChatResponseArgs struct {
	Room types.Room `json:"room" mapstructure:"room"`
}

type UpdateRoomSettingsArgs struct {
	RoomID    string `json:"id" mapstructure:"id" validate:"required"`
	Name      string `json:"name" mapstructure:"name" validate:"required"`
	ReadOnly  bool   `json:"readOnly" mapstructure:"readOnly"`
	Moderable bool   `json:"moderable" mapstructure:"moderable"`
}

type GetRoomUnreadMsgArgs struct {
	RoomId        string `json:"roomId" mapstructure:"roomId" validate:"required"`
	FromMessageId int64  `json:"fromMessageId" mapstructure:"fromMessageId" validate:"required"`
	Count         int    `json:"count" mapstructure:"count" validate:"gt=0"`
}

type ApproveModMsgArgs struct {
	RoomId    string `json:"roomId" mapstructure:"roomId" validate:"required"`
	MessageID int64  `json:"messageId" mapstructure:"messageId" validate:"required"`
}

type ApprovedMsgResponseArgs struct {
	Message types.Message `json:"message" mapstructure:"message"`
	OldID   int64         `json:"oldId" mapstructure:"oldId"`
}

// RejectModMessageArgs is used in RejectModMessage processor for received params parsing
type RejectModMessageArgs struct {
	MsgID  int64  `json:"msgId" mapstructure:"msgId" validate:"required"`
	RoomID string `json:"roomId" mapstructure:"roomId" validate:"required"`
}

// DeleteModBroadcastArgs is used in RejectModMessage processor for using in broadcast
type DeleteModBroadcastArgs struct {
	MsgID  int64  `json:"msgId" mapstructure:"msgId" validate:"required"`
	RoomID string `json:"roomId" mapstructure:"roomId" validate:"required"`
}

// DeleteRoomArgs is used in DeleteRoom processor for received params parsing
type DeleteRoomArgs struct {
	RoomId string `json:"roomId" mapstructure:"roomId" validate:"required"`
}

// SearchRoomsArgs is used in SearchRooms processor for received params parsing
type SearchRoomsArgs struct {
	SearchStr string `json:"searchStr" mapstructure:"searchStr" validate:"required"`
	Count     int    `json:"count" mapstructure:"count" validate:"gt=0"`
}

type ChangeBotActivityArgs struct {
	RoomId      string `json:"roomId" mapstructure:"roomId" validate:"required"`
	BotActivity bool   `json:"botActivity" mapstructure:"botActivity"`
}
type CloseRoomArgs struct {
	RoomId string `json:"roomId" mapstructure:"roomId" validate:"required"`
}

type RatingRequestArgs struct {
	RoomId string `json:"roomId" mapstructure:"roomId" validate:"required"`
}

type NewRatingArgs struct {
	RoomId string `json:"roomId" mapstructure:"roomId" validate:"required"`
	Rating string `json:"rating" mapstructure:"rating" `
}
