package msgproc

import (
	"errors"
	"github.com/labstack/gommon/log"
	v "gopkg.in/go-playground/validator.v9"
	"reflect"
)

func NewRating(
	roomStorage iRoomStorage,
	userManager IUserManager,
) func(Context, string) {
	if roomStorage == nil || reflect.ValueOf(roomStorage).IsNil() {
		log.Panic("Invalid param: roomStorage")
	}
	if userManager == nil || reflect.ValueOf(userManager).IsNil() {
		log.Panic("Invalid param: userManager")
	}
	validator := v.New()
	return func(ctx Context, connID string) {
		var err error
		responseMsg := ResponseMsg{
			Cmd: "new_rating_response",
		}
		defer func() {
			if err != nil {
				responseMsg.Error = err.Error()
			}

			err = ctx.MakeResponse(responseMsg)
			if err != nil {
				log.Error(err)
			}
		}()

		if ctx == nil || reflect.ValueOf(ctx).IsNil() {
			log.Error("Invalid param: context")
			err = errors.New("internal error")
			return
		}
		var args NewRatingArgs
		err = ctx.ParseArgs(&args)
		if err != nil {
			log.Error("context.ParseArgs: ", err)
			return
		}
		err = validator.Struct(args)
		if err != nil {
			log.Error("validator.Struct: ", err)
			err = errors.New("params is not valid")
			return
		}
		user, err := userManager.GetUserByConnID(connID)
		if err != nil {
			log.Error("userManager.GetUserByConnID: ", err)
			err = errors.New("internal error")
			return
		}
		bot, err := userManager.IsBot(user, connID)
		if err != nil {
			log.Error("userManager.IsBot: ", err)
			err = errors.New("internal error")
			return
		}
		if !bot {
			err = errors.New("user is not bot")
			log.Error(err)
			return
		}

		room, err := roomStorage.GetLatestClosedRoomByClient(args.RoomId)
		if err != nil {
			log.Error("roomStorage.GetLatestClosedRoomByClient: ", err)
			err = errors.New("internal error")
			return
		}
		room.Rating = args.Rating
		_, err = roomStorage.Update(room)
		if err != nil {
			log.Error("roomStorage.GetLatestClosedRoomByClient: ", err)
			err = errors.New("internal error")
			return
		}
	}
}
