package msgproc

import (
	"github.com/labstack/gommon/log"
	"gopkg.in/go-playground/validator.v9"
	"reflect"
	"social-trading-chat/backend/msgproc/types"
)

// GetRoomMsg checks GetRoomMsgArgs, if user is member of a room - it
// should send him messages.
// Sends to the sender an error message when an error occurs.
func GetRoomMsg(
	userManager IUserManager,
	roomStorage iRoomStorage,
	msgStorage iMessageStorage,
) func(Context, string) {
	if userManager == nil || reflect.ValueOf(userManager).IsNil() {
		log.Panic("Invalid param: UserManager")
	}
	if roomStorage == nil || reflect.ValueOf(roomStorage).IsNil() {
		log.Panic("Invalid param: RoomStorage")
	}
	if msgStorage == nil || reflect.ValueOf(msgStorage).IsNil() {
		log.Panic("Invalid param: MessageStorage")
	}
	validate := validator.New()
	return func(ctx Context, connID string) {
		var errValue string
		var msgs []types.Message
		args := GetRoomMsgArgs{}
		defer func() {
			resp := ResponseMsg{
				Cmd: "get_room_messages_response",
			}
			if errValue != "" {
				resp.Error = errValue
			} else {
				resp.Args = GetRoomMsgResponseArgs{
					RoomID:   args.RoomID,
					Messages: msgs,
				}
			}
			err := ctx.MakeResponse(resp)
			if err != nil {
				log.Error("composer.Compose: ", err)
			}
		}()
		if ctx == nil || reflect.ValueOf(ctx).IsNil() {
			errValue = "internal error"
			log.Error("Invalid param: messageParser")
			return
		}
		err := ctx.ParseArgs(&args)
		if err != nil {
			errValue = "invalid arguments format"
			log.Error("context.ParseArgs: ", err)
			return
		}
		err = validate.Struct(args)
		if err != nil {
			errValue = "invalid arguments"
			log.Error("validate.Struct: ", err)
			return
		}
		userID, err := userManager.GetUserByConnID(connID)
		if err != nil {
			errValue = "invalid connection ID"
			log.Error("userManager.GetUserByConnID: ", err)
			return
		}
		room, err := roomStorage.GetByID(args.RoomID)
		if err != nil {
			errValue = "invalid room ID"
			log.Error("roomStorage.GetByID: ", err)
			return
		}
		var isMember bool
		for _, member := range room.Members {
			if member == userID {
				isMember = true
				break
			}
		}
		if !isMember {
			errValue = "permission denied"
			return
		}
		msgs, err = msgStorage.Get(args.RoomID, args.MsgID, args.Count)
		if err != nil {
			errValue = "internal error"
			log.Error("msgStorage.Get: ", err)
		}
		return
	}
}
