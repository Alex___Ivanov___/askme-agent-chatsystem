package msgproc

import (
	"github.com/labstack/gommon/log"
	"reflect"
	"social-trading-chat/backend/msgproc/types"
)

func ChangeBotActivity(roomStorage iRoomStorage) func(Context, string) {
	if roomStorage == nil || reflect.ValueOf(roomStorage).IsNil() {
		log.Panic("Invalid param: UserManager")
	}
	return func(ctx Context, connID string) {
		var err error
		var responseErr string
		var user string
		var room types.Room

		responseMsg := ResponseMsg{
			Cmd:   "change_bot_activity_response",
			Error: "",
			Args:  nil,
		}

		argsMsg := types.Message{
			Id:          0,
			Time:        GetCurrentTime(),
			Sender:      user,
			IsModerated: 2,
		}

		defer func() {
			responseMsg.Error = responseErr
			respErr := ctx.MakeResponse(responseMsg)
			if respErr != nil {
				log.Error(respErr)
			}
			if responseErr == "" {
				broadcastErr := ctx.MakeBroadcastExceptSenderConnAndBot(ResponseMsg{
					Cmd:  "new_message",
					Args: argsMsg,
				}, room.Members)
				if respErr != nil {
					log.Error(broadcastErr)
				}
			}
		}()

		args := ChangeBotActivityArgs{}
		err = ctx.ParseArgs(&args)
		if err != nil {
			responseErr = "invalid arguments"
			log.Error(err)
			return
		}

		user, err = ctx.GetUserByConnID(connID)
		if err != nil {
			responseErr = "internal error"
			log.Error(err)
			return
		}

		room, err = roomStorage.GetByID(args.RoomId)
		if err != nil {
			responseErr = "internal error"
			log.Error(err)
			return
		}

		if room.ManagerId != "" && user != room.ManagerId {
			responseErr = "permission denied"
			log.Error(responseErr)
			return
		}

		argsMsg.RoomID = room.ClientID
		argsMsg.Sender = user

		if args.BotActivity {
			argsMsg.Text = "turn on"
			argsMsg.Type = 2
			err = ctx.SendToBot(ResponseMsg{
				Cmd:  "new_message",
				Args: argsMsg,
			}, room.Members)
			if err != nil {
				responseErr = "internal error"
				log.Error(err)
				return
			}

		} else {
			argsMsg.Text = "turn off"
			argsMsg.Type = 1
			err = ctx.SendToBot(ResponseMsg{
				Cmd:  "new_message",
				Args: argsMsg,
			}, room.Members)
			if err != nil {
				responseErr = "internal error"
				log.Error(err)
				return
			}
		}
		room.BotActivity = !room.BotActivity
		_, err = roomStorage.Update(room)
		if err != nil {
			log.Error(err)
		}
	}
}
