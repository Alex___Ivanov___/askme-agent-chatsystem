package msgproc

import (
	"github.com/labstack/gommon/log"
	"reflect"
	"social-trading-chat/backend/msgproc/types"
)

func CloseRoom(roomStorage iRoomStorage) func(Context, string) {
	if roomStorage == nil || reflect.ValueOf(roomStorage).IsNil() {
		log.Panic("Invalid param: UserManager")
	}
	return func(ctx Context, connID string) {
		var err error
		var responseErr string
		var user string
		var room types.Room

		responseMsg := ResponseMsg{
			Cmd:   "close_room_response",
			Error: "",
			Args:  nil,
		}

		args := CloseRoomArgs{}
		err = ctx.ParseArgs(&args)
		if err != nil {
			responseErr = "invalid arguments"
			log.Error(err)
			return
		}
		defer func() {
			responseMsg.Error = responseErr
			respErr := ctx.MakeResponse(responseMsg)
			if respErr != nil {
				log.Error(respErr)
			}
			if responseErr == "" {
				broadcastErr := ctx.MakeBroadcastExceptSenderConnAndBot(ResponseMsg{
					Cmd:  "close_room",
					Args: args,
				}, room.Members)
				if respErr != nil {
					log.Error(broadcastErr)
				}
			}
		}()

		user, err = ctx.GetUserByConnID(connID)
		if err != nil {
			responseErr = "internal error"
			log.Error(err)
			return
		}

		room, err = roomStorage.GetByID(args.RoomId)
		if err != nil {
			responseErr = "internal error"
			log.Error(err)
			return
		}

		if room.ManagerId != "" && user != room.ManagerId {
			responseErr = "permission denied"
			log.Error(responseErr)
			return
		}

		if room.ClosedAt != nil {
			responseErr = "room already closed"
			log.Error(responseErr)
			return
		}

		currentTime := GetCurrentTime()
		room.Status = types.RoomClosed
		room.ClosedAt = &currentTime

		err = ctx.SendToBot(ResponseMsg{
			Cmd: "rating_request",
			Args: RatingRequestArgs{
				RoomId: room.ClientID,
			},
		}, room.Members)
		if err != nil {
			responseErr = "internal error"
			log.Error(err)
			return
		}

		_, err = roomStorage.Update(room)
		if err != nil {
			log.Error(err)
		}
	}
}
