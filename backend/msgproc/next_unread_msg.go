package msgproc

import (
	"reflect"

	"github.com/labstack/gommon/log"
	"gopkg.in/go-playground/validator.v9"
	"social-trading-chat/backend/errtypes"
)

// NextUnreadMsg processes the "next_unread_msg" command which implies setting of
// "unread_message" field id by "user_id" and "room_id" in a room storage
func NextUnreadMsg(
	userManager IUserManager,
	roomStorage iRoomStorage,
) func(Context, string) {
	if userManager == nil || reflect.ValueOf(userManager).IsNil() {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("UserManager"))
	}
	if roomStorage == nil || reflect.ValueOf(roomStorage).IsNil() {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("RoomStorage"))
	}
	validate := validator.New()
	return func(ctx Context, connID string) {
		var args NextUnreadMessageArgs
		var userID string
		resp := ResponseMsg{Cmd: "next_unread_message_response"}
		defer func() {
			err := ctx.MakeResponse(resp)
			if err != nil {
				log.Error(err)
			}
		}()
		if ctx == nil || reflect.ValueOf(ctx).IsNil() {
			resp.Error = "internal error"
			log.Error("Invalid param: messageParser")
			return
		}
		if err := ctx.ParseArgs(&args); err != nil {
			resp.Error = "invalid arguments structure"
			log.Error("ctx.ParseArgs: ", err)
			return
		}
		if err := validate.Struct(args); err != nil {
			resp.Error = "invalid arguments"
			log.Error("validate.Struct: ", err)
			return
		}
		userID, err := userManager.GetUserByConnID(connID)
		if err != nil {
			resp.Error = "invalid connection ID"
			log.Error("userManager.GetUserByConnID: ", err)
			return
		}
		if err := roomStorage.UpdateOldestUnreadMsg(args.RoomID, []string{userID}, args.MessageID); err != nil {
			resp.Error = "internal error"
			log.Error("roomStorage.UpdateOldestUnreadMsg: ", err)
		}
	}
}
