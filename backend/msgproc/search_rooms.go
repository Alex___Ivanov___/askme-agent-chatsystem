package msgproc

import (
	"github.com/labstack/gommon/log"
	"reflect"
	"social-trading-chat/backend/errtypes"
	"social-trading-chat/backend/msgproc/types"
)

// SearchRooms calls roomStorage.Search and sends to user rooms which has been found by search string
func SearchRooms(
	roomStorage iRoomStorage,
) func(Context, string) {
	if roomStorage == nil || reflect.ValueOf(roomStorage).IsNil() {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("roomStorage"))
	}
	return func(context Context, connID string) {
		if context == nil || reflect.ValueOf(context).IsNil() {
			log.Panic(errtypes.ErrFuncArg{}.Invalidate("context"))
		}

		var rooms []types.Room
		var err error
		var args SearchRoomsArgs
		respMsg := ResponseMsg{
			Cmd:   "search_room_response",
			Error: "",
			Args:  nil,
		}

		defer func() {
			respErr := context.MakeResponse(respMsg)
			if respErr != nil {
				log.Error(respErr)
			}
		}()

		err = context.ParseArgs(&args)
		if err != nil {
			log.Error(err)
			respMsg.Error = "input args is invalid"
			return
		}

		err = context.ValidateStruct(args)
		if err != nil {
			log.Error(err)
			respMsg.Error = "input args is invalid"
			return
		}

		user, err := context.GetUserByConnID(connID)
		if err != nil {
			log.Error(err)
			respMsg.Error = "internal server error"
			return
		}

		rooms, err = roomStorage.Search(args.SearchStr, user, args.Count)
		if err != nil {
			log.Error(err)
			respMsg.Error = "internal server error"
			return
		}
		respMsg.Args = rooms
	}
}
