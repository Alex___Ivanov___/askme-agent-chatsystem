package types

import "errors"

type RoomType string

const (
	Group   RoomType = "group"
	Private RoomType = "private"
)

const (
	MsgRejected = iota
	MsgWaiting
	MsgApproved
)

type RoomStatus string

const (
	RoomPending    RoomStatus = "pending"
	RoomInProgress RoomStatus = "in progress"
	RoomClosed     RoomStatus = "closed"
)

// Room is DTO for information about chat room and room members
// Room is used in msgproc and storage packages
type Room struct {
	ID             string     `json:"id" mapstructure:"id"`
	ClientID       string     `json:"clientId" mapstructure:"clientId"`
	Tag            string     `json:"tag" mapstructure:"tag"`
	ManagerId      string     `json:"managerId" mapstructure:"managerId"`
	Status         RoomStatus `json:"status" mapstructure:"status"`
	BotActivity    bool       `json:"botActivity" mapstructure:"botActivity"`
	Members        []string   `json:"members" mapstructure:"members" validate:"gt=0,dive,required"`
	UnreadMsgCount int        `json:"unreadMsgCount" mapstructure:"unreadMsgCount"`
	OldestMsgId    int64      `json:"oldestMsgId" mapstructure:"oldestMsgId"`
	LatestMsg      *Message   `json:"latestMsg" mapstructure:"latestMsg" validate:"-"`
	ClosedAt       *string    `json:"closedAt" mapstructure:"closedAt"`
	Rating         string     `json:"rating" mapstructure:"rating"`
}

type CreateRoomArgs struct {
	Name      string   `json:"name" mapstructure:"name" validate:"required"`
	Members   []string `json:"members" mapstructure:"members" validate:"required,gt=0,dive,required"`
	ReadOnly  bool     `json:"readOnly" mapstructure:"readOnly"`
	Moderable bool     `json:"moderable" mapstructure:"moderable"`
}

// Message contains all information about message
type Message struct {
	Id          int64  `json:"id" mapstructure:"id"`
	Tag         string `json:"tag" mapstructure:"id"`
	Text        string `json:"text" mapstructure:"text" validate:"required"`
	Time        string `json:"time" mapstructure:"time" validate:"required"`
	Sender      string `json:"sender" mapstructure:"sender" validate:"required"`
	RoomID      string `json:"roomId" mapstructure:"roomId" validate:"required"`
	IsModerated int    `json:"isModerated" mapstructure:"isModerated" validate:"gte=0"`
	Type        int    `json:"type" mapstructure:"type"`
}

type ConnInfo struct {
	IsBot bool
}

var (
	ErrRoomNotExisted = errors.New("room is not existed")
)
