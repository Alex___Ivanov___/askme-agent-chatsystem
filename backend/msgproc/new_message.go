package msgproc

import (
	"github.com/labstack/gommon/log"
	uuid "github.com/satori/go.uuid"
	"gopkg.in/go-playground/validator.v9"
	"reflect"
	"social-trading-chat/backend/msgproc/types"
	"time"
)

var GetCurrentTime = func() string {
	return time.Now().UTC().Format(time.RFC3339)
}

var GenerateID = func() string {
	return uuid.NewV4().String()
}

// ClientMsg checks msgArqs, generates current time, saves MSG in storage and
// sends new messages to all room members. If an error occurs - sends to the sender
// answer with an error
func ClientMsg(
	userManager IUserManager,
	roomStorage iRoomStorage,
	msgStorage iMessageStorage,
) func(msgParser Context, connID string) {
	if userManager == nil || reflect.ValueOf(userManager).IsNil() {
		log.Panic("Invalid param: UserManager")
	}
	if roomStorage == nil || reflect.ValueOf(roomStorage).IsNil() {
		log.Panic("Invalid param: RoomStorage")
	}
	if msgStorage == nil || reflect.ValueOf(msgStorage).IsNil() {
		log.Panic("Invalid param: MessageStorage")
	}
	validate := validator.New()
	return func(ctx Context, connID string) {
		var userID string
		msgArgs := MsgArgs{}
		if ctx == nil || reflect.ValueOf(ctx).IsNil() {
			log.Error("Invalid param: MessageParser")
			return
		}
		err := ctx.ParseArgs(&msgArgs)
		if err != nil {
			log.Error("context.ParseArgs: ", err)
			return
		}
		err = validate.Struct(msgArgs)
		if err != nil {
			log.Error("validator.Struct: ", err)
			return
		}
		userID, err = userManager.GetUserByConnID(connID)
		if err != nil {
			log.Error("userManager.GetUserByConnID: ", err)
			return
		}

		isBot, err := userManager.IsBot(userID, connID)
		if err != nil {
			log.Error(err)
			return
		}

		if isBot {
			processBotMsg(userManager, roomStorage, msgStorage, ctx, msgArgs)
		} else {
			processUserMsg(roomStorage, msgStorage, ctx, userID, msgArgs)
		}
	}
}

func processBotMsg(
	userManager IUserManager,
	roomStorage iRoomStorage,
	msgStorage iMessageStorage,
	ctx Context,
	args MsgArgs,
) {
	msgBody := &types.Message{
		Text:        args.Text,
		Time:        GetCurrentTime(),
		Sender:      args.Sender,
		IsModerated: 2,
		Tag:         args.Tag,
	}

	msgToClient := &ResponseMsg{
		Cmd:  "new_message",
		Args: msgBody,
	}
	responseToClient := &ResponseMsg{
		Cmd:  "new_message_response",
		Args: msgBody,
	}

	tag := args.Tag
	if tag == "" {
		tag = "untagged"
		msgBody.Tag = tag
	}

	room, err := roomStorage.GetActiveClientRoom(args.RoomID)
	if err == types.ErrRoomNotExisted {
		onlineUsers := userManager.GetAllUsers()
		room = types.Room{
			ID:          GenerateID(),
			ClientID:    args.RoomID,
			Members:     onlineUsers,
			BotActivity: true,
			Status:      types.RoomPending,
			Tag:         tag,
		}
		room.ID, err = roomStorage.Create(room)
		if err != nil {
			responseToClient.Error = "internal error"
			log.Error("userManager.Create: ", err)
		}
		msgBody.RoomID = room.ID

		msgBody.Id, err = msgStorage.Add(*msgBody)
		if err != nil {
			responseToClient.Error = "internal error"
			log.Error("msgStorage.Add: ", err)
		}
		msgBody.Type = 2
		err = ctx.MakeResponse(responseToClient)
		if err != nil {
			log.Error("ctx.MakeResponse: ", err)
		}
		if responseToClient.Error == "" {
			err = ctx.MakeBroadcastExceptSenderConn(msgToClient, onlineUsers)
			if err != nil {
				log.Error("ctx.MakeBroadcastExceptSenderConn: ", err)
			}
		}
		return
	}
	if err == nil {
		if room.ClosedAt != nil {
			log.Error("room is closed")
			return
		}
		msgBody.RoomID = room.ID
		if room.ManagerId == "" {
			onlineUsers := userManager.GetAllUsers()
			err = roomStorage.AddUsers(room.ID, onlineUsers...)
			if err != nil {
				log.Error("roomStorage.AddUsers: ", err)
				return
			}
			room.BotActivity = true
			if args.Tag != "" {
				room.Tag = args.Tag
			}

			room, err = roomStorage.Update(room)
			if err != nil {
				log.Error("roomStorage.Update: ", err)
				return
			}
			msgBody.Id, err = msgStorage.Add(*msgBody)
			if err != nil {
				responseToClient.Error = "internal error"
				log.Error("msgStorage.Add: ", err)
			}

			err = ctx.MakeResponse(responseToClient)

			if responseToClient.Error == "" {
				room, err := roomStorage.GetByID(room.ID)
				if err != nil {
					log.Error("roomStorage.GetByID: ", err)
					return
				}
				err = ctx.MakeBroadcastExceptSenderConn(msgToClient, room.Members)
				if err != nil {
					log.Error("ctx.MakeBroadcastExceptSenderConn: ", err)
				}
			}

		} else {
			room.BotActivity = true
			room.ClientID = args.RoomID
			room, err = roomStorage.Update(room)
			if err != nil {
				log.Error("roomStorage.Update: ", err)
				return
			}

			msgBody.Id, err = msgStorage.Add(*msgBody)
			if err != nil {
				responseToClient.Error = "internal error"
				log.Error("msgStorage.Add: ", err)
			}

			err = ctx.MakeResponse(responseToClient)
			if err != nil {
				log.Error("ctx.MakeResponse: ", err)
			}

			if responseToClient.Error == "" {
				err = ctx.MakeBroadcast(msgToClient, []string{room.ManagerId})
				if err != nil {
					log.Error("ctx.MakeBroadcast: ", err)
				}
			}
		}
	}
}

func processUserMsg(
	roomStorage iRoomStorage,
	msgStorage iMessageStorage,
	ctx Context,
	userID string,
	args MsgArgs,
) {
	msgBody := &types.Message{
		RoomID:      args.RoomID,
		Text:        args.Text,
		Time:        GetCurrentTime(),
		Sender:      userID,
		IsModerated: 2,
		Tag:         args.Tag,
	}

	msgToClient := &ResponseMsg{
		Cmd:  "new_message",
		Args: msgBody,
	}
	responseToClient := &ResponseMsg{
		Cmd:  "new_message_response",
		Args: msgBody,
	}

	room, err := roomStorage.GetByID(args.RoomID)
	if err != nil {
		return
	}
	if room.ClosedAt != nil {
		log.Error("room is closed")
		return
	}
	msgBody.Tag = room.Tag
	if room.ManagerId == "" {
		msgBody.Sender = userID
		room.ManagerId = userID
		room.BotActivity = false
		room.Status = types.RoomInProgress
		msgBody.Type = 1

		room, err = roomStorage.Update(room)
		if err != nil {
			log.Error("roomStorage.Update: ", err)
			return
		}

		msgBody.Id, err = msgStorage.Add(*msgBody)
		if err != nil {
			responseToClient.Error = "internal error"
			log.Error("msgStorage.Add: ", err)
		}

		msgToBot := ResponseMsg{Cmd: "new_message"}
		msgToBotBody := *msgBody
		msgToBotBody.RoomID = room.ClientID
		msgToBot.Args = msgToBotBody

		err = ctx.SendToBot(msgToBot, room.Members)
		if err != nil {
			log.Error("ctx.SendToBot: ", err)
			return
		}

		err = ctx.MakeResponse(responseToClient)

		if responseToClient.Error == "" {
			room, err := roomStorage.GetByID(args.RoomID)
			if err != nil {
				log.Error("roomStorage.GetByID: ", err)
				return
			}
			err = ctx.MakeBroadcastExceptSenderConnAndBot(msgToClient, room.Members)
			if err != nil {
				log.Error("ctx.MakeBroadcastExceptSenderConn: ", err)
			}
			return
		}

	} else {
		if room.ManagerId != userID {
			responseToClient.Error = "invalid user"
		} else {
			msgBody.Type = 1
			msgBody.Sender = userID
			room.BotActivity = false
		}

		room, err = roomStorage.Update(room)
		if err != nil {
			log.Error("roomStorage.Update: ", err)
			return
		}

		msgBody.Id, err = msgStorage.Add(*msgBody)
		if err != nil {
			responseToClient.Error = "internal error"
			log.Error("msgStorage.Add: ", err)
		}
	}

	err = ctx.MakeResponse(responseToClient)
	if err != nil {
		log.Error("ctx.MakeResponse: ", err)
	}

	if responseToClient.Error == "" {
		msgToBot := ResponseMsg{Cmd: "new_message"}
		msgToBotBody := *msgBody
		msgToBotBody.RoomID = room.ClientID
		msgToBot.Args = msgToBotBody
		err = ctx.SendToBot(msgToBot, room.Members)
		if err != nil {
			log.Error("ctx.SendToBot: ", err)
		}
		err = ctx.MakeBroadcastExceptSenderConnAndBot(msgToClient, []string{room.ManagerId})
		if err != nil {
			log.Error("ctx.MakeBroadcast: ", err)
		}
	}
}
