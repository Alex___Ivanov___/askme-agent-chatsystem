package msgproc

import (
	"github.com/labstack/gommon/log"
	"reflect"
)

// GetRoomUnreadMsg returns unread slice of messages
func GetRoomUnreadMsg(
	messageStorage iMessageStorage,
) func(Context, string) {
	if messageStorage == nil || reflect.ValueOf(messageStorage).IsNil() {
		log.Panic("Invalid param: messageStorage")
	}

	return func(ctx Context, connID string) {
		var args GetRoomUnreadMsgArgs
		responseMsg := ResponseMsg{
			Cmd: "get_room_unread_messages_response",
		}
		defer func() {
			err := ctx.MakeResponse(responseMsg)
			if err != nil {
				log.Error("GetRoomUnreadMsg: failed to ctx.MakeResponse: ", err)
			}
		}()
		if err := ctx.ParseArgs(&args); err != nil {
			responseMsg.Error = "invalid arguments format"
			log.Error("GetRoomUnreadMsg: failed to ctx.ParseArgs: ", err)
			return
		}
		if err := ctx.ValidateStruct(args); err != nil {
			responseMsg.Error = "invalid arguments"
			log.Error("GetRoomUnreadMsg: failed to ctx.ValidateStruct: ", err)
			return
		}
		messages, err := messageStorage.GetUnreadMsgByCount(args.RoomId, args.FromMessageId, args.Count)
		if err != nil {
			responseMsg.Error = "internal error"
			log.Error("GetRoomUnreadMsg: failed to messageStorage.GetUnreadMsgByCount: ", err)
			return
		}
		responseMsg.Args = map[string]interface{}{"messages": messages, "roomId": args.RoomId}
	}
}
