package msgproc

import (
	"errors"
	"github.com/labstack/gommon/log"
)

// WsContext is a websocket implementation of the Context interface
type WsContext struct {
	parser    IMessageParser
	composer  IMessageComposer
	manager   IUserManager
	validator iValidator
	connID    string
}

// ContextCreator is a factory of a Context like instance
type ContextCreator func(connID string) Context

// WsContextCreator closures dependencies of a ContextCreator
func WsContextCreator(
	parserCreator func() IMessageParser,
	composerCreator func() IMessageComposer,
	validator iValidator,
	manager IUserManager,
) ContextCreator {
	return func(connID string) Context {
		return &WsContext{
			parser:    parserCreator(),
			composer:  composerCreator(),
			validator: validator,
			manager:   manager,
			connID:    connID,
		}
	}
}

func (c WsContext) ValidateStruct(argsStruct interface{}) error {
	return c.validator.Struct(argsStruct)
}

func (c *WsContext) ParseMsg(msg []byte) error {
	return c.parser.ParseMsg(msg)
}

func (c *WsContext) ParseArgs(argsStruct interface{}) error {
	return c.parser.ParseArgs(argsStruct)
}

func (c *WsContext) Cmd() string {
	return c.parser.Cmd()
}

func (c *WsContext) SetUserConn(userID, connID string, isBot bool) error {
	return c.manager.SetUserConn(userID, connID, isBot)
}

func (c *WsContext) GetUserByConnID(connID string) (string, error) {
	return c.manager.GetUserByConnID(connID)
}

func (c *WsContext) MakeResponse(dataStruct interface{}) error {
	bytes, err := c.composer.Compose(dataStruct)
	if err != nil {
		return err
	}
	return c.manager.Send(bytes, c.connID)
}

func (c *WsContext) MakeBroadcast(dataStruct interface{}, users []string) error {
	bytes, err := c.composer.Compose(dataStruct)
	if err != nil {
		return err
	}
	c.manager.Broadcast(bytes, users)
	return nil
}

func (c *WsContext) MakeBroadcastExceptSenderConn(dataStruct interface{}, users []string) error {
	body, err := c.composer.Compose(dataStruct)
	if err != nil {
		return err
	}
	c.manager.Broadcast(body, users, c.connID)
	return nil
}

func (c WsContext) MakeBroadcastExceptSenderConnAndBot(dataStruct interface{}, users []string) error {
	filteredUsers := c.removeBotConn(users)
	return c.MakeBroadcastExceptSenderConn(dataStruct, filteredUsers)
}

func (c *WsContext) SendToBot(dataStruct interface{}, users []string) error {
	bot, err := c.findBotUser(users)
	if err != nil {
		return err
	}
	return c.MakeBroadcast(dataStruct, []string{bot})
}

func (c *WsContext) removeBotConn(users []string) []string {
	for i, user := range users {
		conns, err := c.manager.GetUserConns(user)
		if err != nil {
			log.Error("manager.GetUserConns", err)
			continue
		}
		for k := range conns {
			if conns[k].IsBot {
				filteredUsers := append(users[:i], users[i+1:]...)
				return filteredUsers
			}
		}
	}
	return users
}

func (c *WsContext) findBotUser(users []string) (string, error) {
	for _, user := range users {
		conns, err := c.manager.GetUserConns(user)
		if err != nil {
			log.Error("manager.GetUserConns", err)
			continue
		}
		for k := range conns {
			if conns[k].IsBot {
				return user, nil
			}
		}
	}
	return "", errors.New("there is no bot")
}
