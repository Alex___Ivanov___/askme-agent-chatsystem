package msgproc

import (
	"fmt"
	"reflect"

	"github.com/labstack/gommon/log"
	"social-trading-chat/backend/errtypes"
)

type InboundDataProcessor func(context Context, connID string)

type ClientDataRouter struct {
	contextCreator    ContextCreator
	processors        map[string]InboundDataProcessor
	permissionManager IPermissionManager
}

var ErrProcExists = func(procName string) error {
	return fmt.Errorf("proc %s already registered", procName)
}

// NewClientDataRouter initializes ClientDataRouter
// Sets callback in ConnPool
// Should return error if ConnPool or processors map isn't valid
func NewClientDataRouter(contextCreator ContextCreator) (cr *ClientDataRouter, err error) {
	if contextCreator == nil {
		err = errtypes.ErrFuncArg{}.Invalidate("contextCreator")
		return
	}
	cr = &ClientDataRouter{
		processors:        map[string]InboundDataProcessor{},
		contextCreator:    contextCreator,
		permissionManager: nil,
	}
	return
}

// Register appends processor into internal processor map
func (cr *ClientDataRouter) Register(procName string, procFunc InboundDataProcessor) error {
	if procName == "" {
		return errtypes.ErrFuncArg{}.Invalidate("procName")
	}
	if procFunc == nil {
		return errtypes.ErrFuncArg{}.Invalidate("procFunc")
	}
	if _, ok := cr.processors[procName]; ok {
		return ErrProcExists(procName)
	}
	cr.processors[procName] = procFunc
	return nil
}

// Process receives input message and sender's connID
// Calls needed processor by inMsg cmd field
func (cr *ClientDataRouter) Process(inMsg []byte, connID string) {
	log.Debugf("ClientDataRouter: inMsg=`%s`", inMsg)
	context := cr.contextCreator(connID)
	if err := context.ParseMsg(inMsg); err != nil {
		log.Error(err)
		return
	}

	cmd := context.Cmd()

	if !reflect.ValueOf(cr.permissionManager).IsNil() && !cr.permissionManager.CheckPermission(cmd, connID) {
		return
	}

	handler, ok := cr.processors[cmd]
	if !ok {
		log.Error("DataRouter.Process has been received unregistered cmd: ", cmd)
		return
	}
	log.Infof("ClientDataRouter: cmd=`%s`", cmd)
	handler(context, connID)
}

func (cr *ClientDataRouter) SetPermissionManager(pm IPermissionManager) error {
	if pm == nil || reflect.ValueOf(pm).IsNil() {
		return errtypes.ErrFuncArg{}.Invalidate("IPermissionManager")
	}
	cr.permissionManager = pm
	return nil
}
