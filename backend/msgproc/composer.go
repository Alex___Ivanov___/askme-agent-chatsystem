package msgproc

import (
	"encoding/json"
	"gopkg.in/go-playground/validator.v9"
)

// MessageComposer is used for input data serializing
type MessageComposer struct {
	validate *validator.Validate
}

// NewMessageComposer is initializer for MessageComposer
func NewMessageComposer() IMessageComposer {
	return &MessageComposer{
		validate: validator.New(),
	}
}

// Compose receives input interface and  returns serialized object
func (c MessageComposer) Compose(v interface{}) (data []byte, err error) {
	err = c.validate.Struct(v)
	if err != nil {
		return
	}
	return json.Marshal(v)
}
