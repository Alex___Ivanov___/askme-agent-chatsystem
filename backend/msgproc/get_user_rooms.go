package msgproc

import (
	"github.com/labstack/gommon/log"
	"reflect"
	"social-trading-chat/backend/msgproc/types"
)

// GetUserRooms gets information about rooms by user
func GetUserRooms(
	manager IUserManager,
	storage iRoomStorage,
	newRoomGetOpts func(userId string) (RoomGetOpts, error),
) func(Context, string) {
	if manager == nil || reflect.ValueOf(manager).IsNil() {
		log.Panic("invalid param: manager")
	}
	if storage == nil || reflect.ValueOf(storage).IsNil() {
		log.Panic("invalid param: storage")
	}
	if newRoomGetOpts == nil || reflect.ValueOf(newRoomGetOpts).IsNil() {
		log.Panic("invalid param: newRoomGetOpts")
	}
	return func(ctx Context, connID string) {
		var rooms []types.Room
		var err error
		respMsg := ResponseMsg{
			Cmd:   "get_user_rooms_response",
			Error: "",
			Args:  nil,
		}
		defer func() {
			err = ctx.MakeResponse(respMsg)
			if err != nil {
				log.Error("context.MakeResponse: ", err)
			}
		}()

		userId, err := manager.GetUserByConnID(connID)
		if err != nil {
			log.Error("IUserManager.GetUserByConnID: ", err)
			return
		}
		rgs, err := newRoomGetOpts(userId)
		if err != nil {
			log.Error("newRoomGetOpts: ", err)
			return
		}
		// TODO добавить таги и статусы
		rooms, err = storage.GetByOpts(rgs)
		if err != nil {
			log.Error("iRoomStorage.GetByOpts: ", err)
			return
		}
		respMsg.Args = UserRoomsResponseArgs{
			Rooms: rooms,
		}
	}
}
