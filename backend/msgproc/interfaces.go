package msgproc

import (
	"social-trading-chat/backend/connpool"
	"social-trading-chat/backend/msgproc/types"
)

//go:generate mockery -name IUserConns -case underscore -inpkg -testonly
type IUserConns interface {
	GetAllUsers() []string
	// SetUserConn creates or updates user connections
	SetUserConn(userID, connID string, isBot bool) error
	// GetUserConns gets connections of user by user id
	GetUserConns(userID string) (userConns map[string]types.ConnInfo, err error)
	// DeleteUserConn deletes connection of user by user id
	DeleteUserConn(userID string, connID string) error
	// GetUserByConnID gets user by connection id
	GetUserByConnID(connID string) (string, error)
	IsBot(userID string, connID string) (bool, error)
}

//go:generate mockery -name IMessageComposer -case underscore -inpkg -testonly
type IMessageComposer interface {
	// Compose returns serialized object
	Compose(v interface{}) ([]byte, error)
}

//go:generate mockery -name Context -case underscore -inpkg -testonly
type IMessageParser interface {
	// ParseMsg deserializes input data to message field
	ParseMsg(data []byte) error
	// ParseArgs converts message args field to input interface
	ParseArgs(argsStruct interface{}) error
	// Cmd returns message cmd field
	Cmd() string
}

//go:generate mockery -name Context -case underscore -inpkg -testonly
// Ctx represents a facade of MessageParser, Message Composer and UserManager
// which is a dependency of InboundDataProcessors
type Context interface {
	IMessageParser
	// ValidateStruct validates fields of struct accordingly to the struct's args
	ValidateStruct(dataStruct interface{}) error
	// SetUserConn binds user to an established connection
	SetUserConn(userID string, connID string, isBot bool) error
	// GetUserByConnID acquire a connection ID bounded to a specified user
	GetUserByConnID(connID string) (string, error)
	// MakeResponse serializes data and sends it back to requester
	MakeResponse(dataStruct interface{}) error
	// MakeResponse serializes data and make broadcast to a specified users
	MakeBroadcast(dataStruct interface{}, users []string) error
	// MakeResponse serializes data and make broadcast to a specified users
	// except requester connection
	MakeBroadcastExceptSenderConn(dataStruct interface{}, users []string) error
	MakeBroadcastExceptSenderConnAndBot(dataStruct interface{}, users []string) error
	SendToBot(dataStruct interface{}, users []string) error
}

//go:generate mockery -name IConnPool -case underscore -inpkg -testonly
type IConnPool interface {
	AddCloseCb(closeCb func(connID string)) error
	CallCloseCbs(connID string)
	Register(conn connpool.IConn, connID string) error
	ReceiveCb(cb func(msg []byte, connID string))
	Send(msg []byte, connUUID string) error
}

//go:generate mockery -name IPermissionManager -case underscore -inpkg -testonly
type IPermissionManager interface {
	AddConn(conn string) error
	DeleteConn(conn string) error
	CheckPermission(cmd string, conn string) bool
}

//go:generate mockery -name IUserManager -case underscore -inpkg -testonly
type IUserManager interface {
	IConnPool
	IUserConns
	Broadcast(msg []byte, users []string, excludeConns ...string)
}

//go:generate mockery -name ISessionVerifier -case underscore -inpkg -testonly
type ISessionVerifier interface {
	Verify(args SessionDataArgs) error
}

//go:generate mockery -name ISessionVerifierFactory -case underscore -inpkg -testonly
type ISessionVerifierFactory interface {
	Create(isGuest bool) ISessionVerifier
}

//go:generate mockery -name iValidator -case underscore -inpkg -testonly
type iValidator interface {
	Struct(s interface{}) error
}

//go:generate mockery -name iRoomStorage -case underscore -inpkg -testonly
type iRoomStorage interface {
	Create(types.Room) (roomId string, err error)
	Search(searchStr string, user string, count int) (rooms []types.Room, err error)
	Update(room types.Room) (updatedRoom types.Room, err error)
	GetActiveClientRoom(clientID string) (room types.Room, err error)
	GetByID(roomId string) (room types.Room, err error)
	AddUsers(roomId string, usersId ...string) error
	GetByOpts(opts RoomGetOpts) (rooms []types.Room, err error)
	DeleteUsers(roomId string, usersId ...string) error
	GetByRoomAndUser(roomID string, userID string) (room types.Room, err error)
	UpdateOldestUnreadMsg(roomID string, users []string, msgID int64) (err error)
	GetLatestClosedRoomByClient(clientID string) (room types.Room, err error)
	DeleteRoom(roomID string) (err error)
}

//go:generate mockery -name iMessageStorage -case underscore -inpkg -testonly
type iMessageStorage interface {
	Add(types.Message) (msgID int64, err error)
	Get(roomID string, msgID int64, count int) ([]types.Message, error)
	GetUnreadMsgByCount(roomID string, msgID int64, count int) ([]types.Message, error)
}

type RoomGetOpts interface {
	SetStatuses(statuses []types.RoomStatus) error
	SetTags(tags []string) error
	BuildQuery() string
	UserID() string
}
