package msgproc

import (
	"errors"
	"github.com/labstack/gommon/log"
	v "gopkg.in/go-playground/validator.v9"
	"reflect"
)

// Auth authenticates the connect, after which it has more options
func Auth(
	userManager IUserManager,
	userSessionVerivier,
	botSessionVerifier ISessionVerifier,
	permissionManager IPermissionManager,
	composer IMessageComposer,
) func(Context, string) {
	if userManager == nil || reflect.ValueOf(userManager).IsNil() {
		log.Panic("Invalid param: UserManager")
	}
	if botSessionVerifier == nil || reflect.ValueOf(botSessionVerifier).IsNil() {
		log.Panic("Invalid param: botSessionVerifier")
	}
	if permissionManager == nil || reflect.ValueOf(permissionManager).IsNil() {
		log.Panic("Invalid param: permissionManager")
	}
	if composer == nil || reflect.ValueOf(composer).IsNil() {
		log.Panic("Invalid param: composer")
	}
	validator := v.New()
	return func(ctx Context, connID string) {
		var err error
		responseMsg := ResponseMsg{
			Cmd:   "auth_connection_response",
			Error: "",
			Args:  nil,
		}
		defer func() {
			if err != nil {
				responseMsg.Error = err.Error()
			}

			err = ctx.MakeResponse(responseMsg)
			if err != nil {
				log.Error(err)
			}
		}()

		if ctx == nil || reflect.ValueOf(ctx).IsNil() {
			log.Error("Invalid param: context")
			err = errors.New("internal error")
			return
		}
		var args SessionDataArgs
		err = ctx.ParseArgs(&args)
		if err != nil {
			log.Error("context.ParseArgs: ", err)
			return
		}
		args.ConnID = connID
		err = validator.Struct(args)
		if err != nil {
			log.Error("validator.Struct: ", err)
			err = errors.New("params is not valid")
			return
		}

		if args.IsBot {
			err = botSessionVerifier.Verify(args)
		} else {
			if args.Username == "bot" || args.Username == "client" {
				log.Error("Invalid username: ", args.Username)
				err = errors.New("invalid username")
				return
			}
			err = userSessionVerivier.Verify(args)
		}
		if err != nil {
			log.Error("botSessionVerifier.Verify: ", err)
			return
		}

		err = userManager.SetUserConn(args.Username, connID, args.IsBot)
		if err != nil {
			log.Error("userManager.SetUserConn: ", err)
			return
		}
		err = permissionManager.AddConn(connID)
		if err != nil {
			log.Error("permissionManager.AddConn: ", err)
			return
		}
	}
}
