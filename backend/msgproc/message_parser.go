package msgproc

import (
	"encoding/json"
	"github.com/mitchellh/mapstructure"
)

// stores message info
type inputMessage struct {
	Cmd  string                 `json:"cmd"`
	Args map[string]interface{} `json:"args"`
}

// MessageParser is needed to parse and store message info
type MessageParser struct {
	inputMessage
}

// ParseMsg deserializes input data to message field
func (mp *MessageParser) ParseMsg(data []byte) error {
	return json.Unmarshal(data, &mp.inputMessage)
}

// ParseArgs converts message args field to input interface
func (mp MessageParser) ParseArgs(argsStruct interface{}) error {
	dec, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		WeaklyTypedInput: true,
		Result:           argsStruct,
	})
	if err != nil {
		return err
	}
	return dec.Decode(mp.inputMessage.Args)
}

// Cmd returns message cmd field
func (mp MessageParser) Cmd() string {
	return mp.inputMessage.Cmd
}
