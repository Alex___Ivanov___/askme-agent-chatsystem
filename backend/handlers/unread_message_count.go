package handlers

import (
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"net/http"

	"reflect"
)

type unreadMsg struct {
	Count int `json:"count"`
}

// UnreadMessageCount returns number of unread messages
func UnreadMessageCount(messageStorage iMessageStorage) func(echo.Context) error {
	if messageStorage == nil || reflect.ValueOf(messageStorage).IsNil() {
		log.Panic("Invalid param: iMessageStorage")
	}
	return func(ctx echo.Context) error {
		userId := ctx.Param("user_id")
		if userId == "" {
			log.Warn("user_id param is empty")
			return echo.ErrBadRequest
		}
		unreadCount, err := messageStorage.GetUnreadCount(userId)
		if err != nil {
			log.Error("Can't get unreadCount from messageStorage: ", err)
			return echo.ErrInternalServerError
		}
		respData := unreadMsg{Count: unreadCount}
		return ctx.JSON(http.StatusOK, respData)
	}
}
