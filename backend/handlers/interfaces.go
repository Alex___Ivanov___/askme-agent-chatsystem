package handlers

import (
	"github.com/gorilla/websocket"
	"net/http"
	"social-trading-chat/backend/connpool"
)

//go:generate mockery -name IUserConns -case underscore -inpkg -testonly
type IUserConns interface {
	// SetUserConn creates or updates user connections
	SetUserConn(userID string, connID string) error
	// GetUserConns gets connections of user by user id
	GetUserConns(userID string) (userConns map[string]struct{}, err error)
	// DeleteUserConn deletes connection of user by user id
	DeleteUserConn(userID string, connID string) error
}

//go:generate mockery -name IConnPool -case underscore -inpkg -testonly
type IConnPool interface {
	Send(msg []byte, connUUID string) error
	Register(conn connpool.IConn, connUUID string) error
	ReceiveCb(cb func(msg []byte, connUUID string))
	PingWaitForConn(timeout int, connId string) error
	PingMessageForConn(msg string, connId string) error
}

//go:generate mockery -name IUpgrader -case underscore -inpkg -testonly
// IUpgrader used for testing purpose
type IUpgrader interface {
	Upgrade(w http.ResponseWriter, r *http.Request, responseHeader http.Header) (*websocket.Conn, error)
}

//go:generate mockery -name iMessageStorage -case underscore -inpkg -testonly
type iMessageStorage interface {
	GetUnreadCount(string) (int, error)
}
