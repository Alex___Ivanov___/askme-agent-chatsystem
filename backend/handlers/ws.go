package handlers

import (
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	uuid "github.com/satori/go.uuid"
	"reflect"
	"social-trading-chat/backend/connpool"
	"social-trading-chat/backend/errtypes"
	"strconv"
)

var uuidString = func() string { return uuid.NewV4().String() }
var initAndRunWsConn = connpool.InitAndRunWsConn

// WsHandler used to upgrade connection to a WebSocket server connection
func WsHandler(upgrader IUpgrader, pool IConnPool) func(ctx echo.Context) error {
	if upgrader == nil || reflect.ValueOf(upgrader).IsNil() {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("upgrader"))
	}
	if pool == nil || reflect.ValueOf(pool).IsNil() {
		log.Panic(errtypes.ErrFuncArg{}.Invalidate("pool"))
	}
	return func(ctx echo.Context) error {
		incomingPingMsg := ctx.QueryParam("pingMsg")
		incomingPingWait := ctx.QueryParam("pingWait")
		connUUID := uuidString()
		ws, err := upgrader.Upgrade(ctx.Response(), ctx.Request(), nil)
		if err != nil {
			log.Error("Can't upgrade http to ws: ", err)
			return echo.ErrBadRequest
		}

		c, err := initAndRunWsConn(ws, connUUID)
		if err != nil {
			log.Error("Can't create NewWsConn: ", err)
			return echo.ErrInternalServerError
		}
		if err = pool.Register(c, connUUID); err != nil {
			log.Error("Can't register NewWsConn in pool: ", err)
			return echo.ErrInternalServerError
		}

		if incomingPingMsg != "" {
			if err = pool.PingMessageForConn(incomingPingMsg, connUUID); err != nil {
				log.Error("Can't set ping message in pool: ", err)
				return echo.ErrInternalServerError
			}
		}
		if incomingPingWait != "" {
			wait, err := strconv.Atoi(incomingPingWait)
			if err != nil {
				log.Error("Can't parse incoming ping wait: ", err)
				return echo.ErrBadRequest
			}
			if err = pool.PingWaitForConn(wait, connUUID); err != nil {
				log.Error("Can't set ping wait in pool: ", err)
				return echo.ErrInternalServerError
			}
		}
		return nil
	}
}
