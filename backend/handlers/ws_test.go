package handlers

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"social-trading-chat/backend/connpool"
	"strings"
	"testing"

	"github.com/gorilla/websocket"
	"github.com/labstack/gommon/log"
	"github.com/stretchr/testify/mock"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func init() {
	uuidString = func() string { return "a7f75ed2-c688-408f-aa02-2571a5017bf7" }
}

func TestWsHandler_params(t *testing.T) {
	// arrange
	cases := []struct {
		desc         string
		mockPool     *MockIConnPool
		mockUpgrader *MockIUpgrader
	}{
		{
			desc:         "Should panic when upgrader param is nil",
			mockPool:     &MockIConnPool{},
			mockUpgrader: nil,
		},
		{
			desc:         "Should panic when pool param is nil",
			mockPool:     nil,
			mockUpgrader: &MockIUpgrader{},
		},
	}
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			initAndRunWsConn = func(ws connpool.IWs, connUUID string) (conn *connpool.WsConn, e error) {
				return &connpool.WsConn{}, nil
			}
			// act
			assert.Panics(t, func() { WsHandler(c.mockUpgrader, c.mockPool) })
		})
	}
}

func TestWsHandler_closure(t *testing.T) {
	ws := &websocket.Conn{}
	// assert
	cases := []struct {
		desc                string
		pingMsg             string
		pingWait            string
		wantError           error
		mockUpgrader        func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader
		mockPool            *MockIConnPool
		remoteSrv           string
		initAndRunWsConnErr bool
		wantLog             string
	}{
		{
			desc:      "Should return ErrBadRequest and write to log, when Upgrade returned error",
			wantError: echo.ErrBadRequest,
			wantLog:   "Can't upgrade http to ws: upgrader error",
			remoteSrv: "server.com",
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(nil, errors.New("upgrader error")).
					Once()
				return up
			},
			mockPool: func() *MockIConnPool { return &MockIConnPool{} }(),
		},
		{
			desc:      "Should return InternalServerError when initAndRunWsConn returned error",
			wantError: echo.ErrInternalServerError,
			wantLog:   "Can't create NewWsConn: initAndRunWsConnErr",
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(ws, nil).
					Once()
				return up
			},
			mockPool:            func() *MockIConnPool { return &MockIConnPool{} }(),
			remoteSrv:           "server.com",
			initAndRunWsConnErr: true,
		},
		{
			desc:      "Should return InternalServerError when pool.Register returned error",
			wantError: echo.ErrInternalServerError,
			wantLog:   "Can't register NewWsConn in pool: register error",
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(ws, nil).
					Once()
				return up
			},
			mockPool: func() *MockIConnPool {
				pool := &MockIConnPool{}
				pool.On("Register", mock.Anything, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(errors.New("register error")).
					Once()
				return pool
			}(),
			remoteSrv: "server.com",
		},
		{
			desc:                "Should return error nil, when all steps is ok",
			wantLog:             "",
			wantError:           nil,
			pingMsg:             "",
			remoteSrv:           "server.com",
			initAndRunWsConnErr: false,
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(ws, nil).
					Once()
				return up
			},
			mockPool: func() *MockIConnPool {
				pool := &MockIConnPool{}
				pool.On("Register", mock.Anything, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(nil).
					Once()
				return pool
			}(),
		},
		{
			desc:                "Should return error, when ping msg setting returns error",
			wantLog:             "Can't set ping message in pool: error",
			wantError:           echo.ErrInternalServerError,
			pingMsg:             "*",
			remoteSrv:           "server.com",
			initAndRunWsConnErr: false,
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(ws, nil).
					Once()
				return up
			},
			mockPool: func() *MockIConnPool {
				pool := &MockIConnPool{}
				pool.On("Register", mock.Anything, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(nil).
					Once()
				pool.On("PingMessageForConn", "*", "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(errors.New("error")).
					Once()
				return pool
			}(),
		},
		{
			desc:                "Should return nil error, when ping msg setting returns nil error",
			wantLog:             "",
			wantError:           nil,
			pingMsg:             "*",
			remoteSrv:           "server.com",
			initAndRunWsConnErr: false,
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(ws, nil).
					Once()
				return up
			},
			mockPool: func() *MockIConnPool {
				pool := &MockIConnPool{}
				pool.On("Register", mock.Anything, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(nil).
					Once()
				pool.On("PingMessageForConn", "*", "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(nil).
					Once()
				return pool
			}(),
		},
		{
			desc:                "Should return error, when ping wait is invalid",
			wantLog:             "",
			wantError:           echo.ErrBadRequest,
			pingMsg:             "",
			pingWait:            "NaN",
			remoteSrv:           "server.com",
			initAndRunWsConnErr: false,
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(ws, nil).
					Once()
				return up
			},
			mockPool: func() *MockIConnPool {
				pool := &MockIConnPool{}
				pool.On("Register", mock.Anything, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(nil).
					Once()
				return pool
			}(),
		},
		{
			desc:                "Should return error, when ping wait setting returns error",
			wantLog:             "",
			wantError:           echo.ErrInternalServerError,
			pingMsg:             "",
			pingWait:            "1",
			remoteSrv:           "server.com",
			initAndRunWsConnErr: false,
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(ws, nil).
					Once()
				return up
			},
			mockPool: func() *MockIConnPool {
				pool := &MockIConnPool{}
				pool.On("Register", mock.Anything, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(nil).
					Once()
				pool.On("PingWaitForConn", 1, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(errors.New("error")).
					Once()
				return pool
			}(),
		},
		{
			desc:                "Should return error, when ping wait setting returns nil error",
			wantLog:             "",
			wantError:           nil,
			pingMsg:             "",
			pingWait:            "1",
			remoteSrv:           "server.com",
			initAndRunWsConnErr: false,
			mockUpgrader: func(ctx echo.Context, up *MockIUpgrader) *MockIUpgrader {
				up.On("Upgrade", ctx.Response(), ctx.Request(), http.Header(nil)).
					Return(ws, nil).
					Once()
				return up
			},
			mockPool: func() *MockIConnPool {
				pool := &MockIConnPool{}
				pool.On("Register", mock.Anything, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(nil).
					Once()
				pool.On("PingWaitForConn", 1, "a7f75ed2-c688-408f-aa02-2571a5017bf7").
					Return(nil).
					Once()
				return pool
			}(),
		},
	}
	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			// before
			Upgrader := &MockIUpgrader{}
			Pool := c.mockPool
			initAndRunWsConn = func(ws connpool.IWs, connUUID string) (conn *connpool.WsConn, e error) {
				if c.initAndRunWsConnErr {
					return nil, errors.New("initAndRunWsConnErr")
				}
				return &connpool.WsConn{}, nil
			}

			var b bytes.Buffer
			log.SetOutput(&b)

			e := echo.New()
			url := fmt.Sprintf("/?pingMsg=%s&pingWait=%s", c.pingMsg, c.pingWait)
			req := httptest.NewRequest(http.MethodGet, url, nil)
			rec := httptest.NewRecorder()
			ctx := e.NewContext(req, rec)
			ctx.SetPath("/")
			ctx.Request().RemoteAddr = c.remoteSrv + ":1232"

			// act
			gotErr := WsHandler(c.mockUpgrader(ctx, Upgrader), Pool)(ctx)
			gotLog, _ := ioutil.ReadAll(&b)

			// assert
			assert.Equal(t, c.wantError, gotErr)
			if len(gotLog) != 0 {
				assert.Equal(t, true, strings.Contains(string(gotLog), c.wantLog))
			}
			Upgrader.AssertExpectations(t)
			Pool.AssertExpectations(t)
		})
	}
}
