alter table rooms alter column bot_activity set default false;

update rooms set bot_activity = false where bot_activity is null;
