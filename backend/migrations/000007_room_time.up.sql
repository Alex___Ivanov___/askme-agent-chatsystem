begin;

alter table rooms
    add created_at timestamp not null default timenow();

alter table rooms
    add closed_at timestamp;

commit;