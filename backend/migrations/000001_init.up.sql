begin;

create extension if not exists "uuid-ossp";

create table if not exists rooms (
    room_id varchar(255) primary key,
    manager_id varchar(255)
);

create table if not exists user_rooms (
    id      uuid primary key default uuid_generate_v1(),
    user_id varchar(255) not null,
    room_id varchar(255) references rooms(room_id) on delete cascade,
    unique (user_id, room_id)
);

create table if not exists messages (
    msg_id       bigserial primary key,
    sender       varchar(255) not null,
    text         varchar      not null,
    room_id      varchar(255) references rooms (room_id) on delete cascade,
    msg_time     timestamp    not null,
    is_moderated int default 2
);

create table if not exists room_setting (
    id             uuid primary key default uuid_generate_v1(),
    user_room      uuid references user_rooms (id) on delete cascade,
    unread_message bigint references messages (msg_id) on delete set null
);


commit;
