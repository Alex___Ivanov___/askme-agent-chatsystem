# HelpChat

## Iframe integration
http://url.example.com/?userID=userID&token=yourAccessToken&iframe=true  
System has reserved names "bot" and "client", so you can not use them.

## WS Commands fot backend

### 1. Get connection session UUID 

```json5
{
  "cmd": "get_session_id"
}
```

Response

```json5
{
  "cmd": "get_session_id_response",
  "error": "error description or empty",
  "args": {
    "sessionId": "sessionID"
  }
}
```

### 2. Auth

```json5
{
  "cmd": "auth_connection",
  "args": {
    "username": "user",
    "bot": true
  }
}
```

Response

```json5
{
  "cmd": "auth_connection_response",
  "error": "error description or empty",
}
```

### 3. New message

```json5
{
  "cmd": "new_message",
  "args": {
    "roomId": "roomID",
    "text": "msg text",
    "sender": "client/bot (for bots)",
    "tag": "tag of message"
  }
}
```

Response

```json5
{
  "cmd": "new_message_response",
  "error": "error description or empty",
}
```

### 4. Get user rooms

```json5
{
  "cmd": "get_user_rooms"
}
```

Response

```json5
{
  "cmd": "get_user_rooms_response",
  "error": "error description or empty",
  "args": {
    "rooms": [
      {
        "id": "roomID_1",
        "members": [
          "user1",
          "user2",
          "user3"
        ],
        "manager_id": "managerID",
        "unreadMsgCount": 10,
        "oldestMsgId": "messageID",
        "latestMsg": {
          "id": "messageID",
          "sender": "who wrote",
          "roomId": "roomID",
          "text": "message text",
          "time": "RFC3339 format"
        },
      }
    ]
  }
}
```

### 5. Get room messsages

```json5
{
  "cmd": "get_room_messages",
  "args": {
    "roomId": "roomID",
    "msgId": 123456,
    "count": 10
  }
}
```

Response

```json5
{
  "cmd": "get_room_messages_response",
  "error": "error description or empty",
  "args": {
    "roomId": "roomID",
    "messages": [
      {
        "id": 123,
        "sender": "who wrote",
        "roomId": "roomID",
        "text": "msg text",
        "time": "RFC3339 format",
		"isModerated": 2,
		"type": 0
      },
      {
        "id": 124,
        "sender": "who wrote",
        "roomId": "roomID",
        "text": "msg text",
        "time": "RFC3339 format",
		"isModerated": 2,
		"type": 0
      }
    ]
  }
}
```

### 6. Get room unread messsages

```json5
{
  "cmd": "get_room_unread_messages",
  "args": {
    "roomId": "roomID",
    "fromMessageId": 123456,
    "count": 10
  }
}
```

Response

```json5
{
  "cmd": "get_room_unread_messages_response",
  "error": "error description or empty",
  "args": {
    "roomId": "roomID",
    "messages": [
      {
        "id": 123,
        "sender": "who wrote",
        "roomId": "roomID",
        "text": "msg text",
        "time": "RFC3339 format",
		"isModerated": 2,
		"type": 0
      },
      {
        "id": 124,
        "sender": "who wrote",
        "roomId": "roomID",
        "text": "msg text",
        "time": "RFC3339 format",
		"isModerated": 2,
		"type": 0
      }
    ]
  }
}
```

### 7. Next unread message

```json5
{
  "cmd": "next_unread_message",
  "args": {
    "roomId": "roomID",
	"messageId": 123456
  }
}
```

Response

```json5
{
  "cmd": "next_unread_message_response",
  "error": "description error"
}
```

### Events

### 1. New message

```json5
{
  "cmd": "new_message",
  "args": {
    "sender": "who wrote",
    "roomId": "roomID",
    "text": "msg text",
    "id": "messageID",
    "time": "RFC3339 format",
    "isModerated": 2,
	"type": 0,
    "tag": "tag of message"
  }
}
```

Message types

|Number|Value|
|----|-----|
|0|text message|
|1|turn off bot|
|2|turn on bot|
